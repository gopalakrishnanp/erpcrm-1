/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.rightnow.crm.taskwebapp.utils;

import com.myntra.client.tools.response.ApplicationPropertiesEntry;
import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import com.myntra.commons.excel.poiImpl.MyntraSheetReader;
import com.myntra.commons.exception.FileIOException;
import com.myntra.commons.utils.LoadApplicationProperties;
import com.myntra.commons.utils.LoadApplicationPropertiesFile;
import com.myntra.rightnow.crm.taskwebapp.controller.TaskwebappController;
import com.myntra.rightnow.crm.taskwebapp.fileupload.exceptions.FileUploadExceptionMessage;
import com.myntra.rightnow.crm.taskwebapp.fileupload.exceptions.FileUploadSheetMessages;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author preetam
 */
public class FileUploadUtils {
    
    
    public static Map<String, String> propertiesMap;    

    public static boolean isEmptyRow(MyntraSheetReader sheet, int columnIndex) {
        boolean isEmpty = false;
        try {
            sheet.setColumn(columnIndex);
            if (!sheet.hasCell() || sheet.isCellEmpty()) {
                return true;
            }
            Object cellValue = sheet.readCellValue();
            if (cellValue == null) {
                isEmpty = true;
            } else {
                String cellStringValue = cellValue.toString().trim();
                if (cellStringValue.isEmpty()) {
                    isEmpty = true;
                }
            }

        } catch (ExcelPOIWrapperException ex) {
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
            isEmpty = true;
        }
        return isEmpty;
    }

    public static void showUploadStatusMessages(List<FileUploadSheetMessages> errorMessages, String errorDisplayID, String fileName) {

        if (errorMessages.isEmpty()) {
            //upload successful
            FacesMessage uploadSuccess = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Upload is successful");
            FacesContext.getCurrentInstance().addMessage(errorDisplayID, uploadSuccess);
        } else {
            FacesMessage fileMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Error in the excel file " + fileName);
            FacesContext.getCurrentInstance().addMessage(errorDisplayID, fileMessage);

            //show error messages for each sheet
            Iterator<FileUploadSheetMessages> errorItr = errorMessages.iterator();
            FileUploadSheetMessages sheetErrorMessages = null;
            while (errorItr.hasNext()) {
                sheetErrorMessages = errorItr.next();

                //add some blank lines
                FacesMessage msgBlank = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "");
                FacesContext.getCurrentInstance().addMessage(errorDisplayID, msgBlank);

                //display heading for the sheet
                FacesMessage msgSheet = new FacesMessage(FacesMessage.SEVERITY_ERROR, "",
                        "FOR SHEET WITH NAME \"" + sheetErrorMessages.getSheetName() + "\"");
                FacesContext.getCurrentInstance().addMessage(errorDisplayID, msgSheet);

                //iterate the error messages for each sheet
                Iterator<FileUploadExceptionMessage> itr = sheetErrorMessages.getErrorMessages().iterator();
                FileUploadExceptionMessage expMessage = null;
                while (itr.hasNext()) {
                    expMessage = itr.next();
                    FacesMessage msg = null;
                    if (expMessage.getType().equals(FileUploadExceptionMessage.ERROR)) {
                        msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", expMessage.getMessage());
                    } else {
                        msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", expMessage.getMessage());
                    }

                    FacesContext.getCurrentInstance().addMessage(errorDisplayID, msg);
                }
            }
        }
    }
    
     public static String getPropertyValue(String name) {
         String value = null;
         if(propertiesMap == null) {
             
             propertiesMap = new HashMap<String, String>();
             LoadApplicationProperties properties = new LoadApplicationPropertiesFile(new String[]{"serviceurls.properties"});
             try {
                 properties.loadProperties();
             } catch (FileIOException ex) {
                 ex.printStackTrace();
             }
             Map<String, ApplicationPropertiesEntry> hm = properties.getProperties();
             for (String s : hm.keySet()) {             
                propertiesMap.put(s, hm.get(s).getValue());
            }             
         }
         value = propertiesMap.get(name);
         return value;
         
    }
}
