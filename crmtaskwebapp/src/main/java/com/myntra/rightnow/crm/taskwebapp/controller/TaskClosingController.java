/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.myntra.rightnow.crm.taskwebapp.controller;

import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import com.myntra.commons.excel.poiImpl.MyntraSheetReader;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.rightnow.crm.client.RightnowTaskClient;
import com.myntra.rightnow.crm.client.code.RightnowErrorCodes;
import com.myntra.rightnow.crm.client.entry.RightnowTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskIdEntry;
import com.myntra.rightnow.crm.client.response.RightnowTaskResponse;
import com.myntra.rightnow.crm.taskwebapp.fileupload.BaseFileUploader;
import static com.myntra.rightnow.crm.taskwebapp.fileupload.BaseFileUploader.MAX_NO_OF_EMPTY_ROWS_TO_STOP_PRCESSING;
import com.myntra.rightnow.crm.taskwebapp.fileupload.exceptions.FileUploadSheetMessages;
import com.myntra.rightnow.crm.taskwebapp.utils.FileUploadUtils;
import com.myntra.rightnow.crm.taskwebapp.utils.TaskwebappUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Preetam
 */
@ManagedBean(name = "closeTasks")
@ViewScoped
public class TaskClosingController extends BaseFileUploader implements Serializable {
    
    private static final String TASK_ID_COLUMN_NAME = "Task Id";
    
    public static final int MAX_TASKS_ALLOWED_TO_CLOSE = 1000;
    
    public int getMaxTasksAllowedToClose() {
        return MAX_TASKS_ALLOWED_TO_CLOSE;
    }
        
        
    @Override
    public void processEachExcelSheet(MyntraSheetReader sheet) {
        List<String> taskIds = new ArrayList<String>();
        //in ubuntu, it takes all the rows (65535) of the excel sheet even if rows are blank.
        //so we will have to see if consecutively 3 rows do not have task id (first column) treat them as blank and halt the processing of any further row
        try {
            //validate columns
            if(!validateColumnHeaders(sheet))
                return;

            //we are here, it means the excel columns are perfectly alright and 
            //we should proceed to reading of the actual data as eneterd by user
            int emptyRowsCount = 0; //initialize to zero before starting to read the rows
            while (sheet.hasNextRow()) {
                sheet.nextRow();
                //check for empty row, done by checking whether the Main Category value exists or not
                if (FileUploadUtils.isEmptyRow(sheet, 0)) {
                    emptyRowsCount++;
                } else {
                    //create a task entry object for the current row
                    sheet.setColumn(0);
                    Object orderObj = sheet.readCellValue();
                    Long taskId = 0L;
                    if(orderObj instanceof Double) {
                        taskId = ((Double) orderObj).longValue();
                    } else if(orderObj instanceof Long) {
                        taskId = ((Long) orderObj).longValue();
                    } else {
                        taskId = Long.parseLong((String) orderObj);
                    }
                    if (taskId > 0L) {
                        taskIds.add(taskId.longValue()+"");
                    }
                    emptyRowsCount = 0;
                }

                if (emptyRowsCount == MAX_NO_OF_EMPTY_ROWS_TO_STOP_PRCESSING) {
                    break;
                }
            }

            if (taskIds.isEmpty()) {
                addErrorMessage("There are no valid entries for any of the rows.");
            } else if(taskIds.size() > MAX_TASKS_ALLOWED_TO_CLOSE) {
                addErrorMessage("You have exceeded the maximum allowed limit of " + MAX_TASKS_ALLOWED_TO_CLOSE + 
                        " for bulk creation of Detractor Calling Tasks.");
            }

            if (errorMessagesList.size() > 0) {
                return;
            }

            //close the tasks
            RightnowTaskResponse check = RightnowTaskClient.closeTasks(null, new RightnowTaskIdEntry(taskIds));
            
            if (TaskwebappUtils.isSuccessResponse(check)) {
                if(check.getStatus().getStatusCode() == RightnowErrorCodes.NO_TASK_FETCHED.getStatusCode()) {                    
                    addErrorMessage("Couldn't fetch Incident information for the Taks. Please try again later. If problem persists, contact Tasky support.");
                } else if(check.getStatus().getStatusCode() == RightnowErrorCodes.TASKS_CLOSED_WITH_EXCEPTIONS.getStatusCode()){
                    String message = "All the Tasks are successfully closed. But the following incidents cannot be closed as they have one more unclosed Tasks<br/>";
                    for(RightnowTaskEntry taskEntry : check.getRightnowTaskEntryList()) {
                        message += taskEntry.getIncidentId()+ "     ";
                    }
                    addErrorMessage(message);
                } else if(check.getStatus().getStatusCode() == RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW.getStatusCode()){
                    addErrorMessage("Error while closing tasks! Please contact administrator.");
                }
            } 

        } catch (ExcelPOIWrapperException ex) {
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ERPServiceException ex) {
            ex.printStackTrace();
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
            addErrorMessage("Error while creating tasks");
        }        
    }
    
    @Override
    public boolean validateColumnHeaders(MyntraSheetReader sheet) {
        try {
            
            String firstColumnName = null;
            if (sheet.hasNextRow()) {
                sheet.nextRow();
                if (sheet.hasCell()) {
                    firstColumnName = sheet.readStringCellValue();
                }
            }else {
                addErrorMessage("The sheet doesn't have a colum header as first row.");
                return false;
            }
            
            //if first column is not Task ID, show an error message to the user
            //else set the Task ID column index
            if (!TASK_ID_COLUMN_NAME.equalsIgnoreCase(firstColumnName)) {
                addErrorMessage("'Task ID' column is mandatory and should be the first column.");  
                return false;
            }
                        
        } catch (ExcelPOIWrapperException ex) {
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    @Override
    public void validateData(MyntraSheetReader sheet) {
        
    }
    
    @Override
    public void showUploadStatusMessages(List<FileUploadSheetMessages> errorMessages, String fileName) {
        FileUploadUtils.showUploadStatusMessages(errorMessages, "closeTaskMessage", fileName);
    } 
}
