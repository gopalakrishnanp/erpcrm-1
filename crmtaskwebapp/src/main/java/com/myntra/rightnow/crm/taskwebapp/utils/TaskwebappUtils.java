/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.rightnow.crm.taskwebapp.utils;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.rightnow.crm.client.entry.RightnowNoteEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportFilterEntry;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pravin
 */
public class TaskwebappUtils {
    
    public static final String[] RIGHTNOW_REPORT_FILTERS = {"Courier Name", "Warehouse"};
    
    public static boolean isSuccessResponse(AbstractResponse response) {
        if(response==null)
            return false;

        if(response.getStatus().getStatusType().equals(StatusResponse.Type.SUCCESS))
            return true;

        if(response.getStatus().getStatusType().equals("SUCCESS"))
            return true;
        
        return false;
    }
    
    public static String getReportName(String reportName) {
        if(reportName.equalsIgnoreCase("BlueDart North") || 
                reportName.equalsIgnoreCase("BlueDart South") || 
                reportName.equalsIgnoreCase("FirstFlight North") || 
                reportName.equalsIgnoreCase("FirstFlight South") ||
                reportName.equalsIgnoreCase("Overnight Express North") ||
                reportName.equalsIgnoreCase("Overnight Express South") ||
                reportName.equalsIgnoreCase("Aramex") ||
                reportName.equalsIgnoreCase("DTDC") ||
                reportName.equalsIgnoreCase("Delhivery") ||
                reportName.equalsIgnoreCase("Ecomm Express") ||
                reportName.equalsIgnoreCase("Fedex") ||
                reportName.equalsIgnoreCase("India On Time") ||
                reportName.equalsIgnoreCase("Quantium") ||
                reportName.equalsIgnoreCase("VRL Logistics")) {
            reportName = "3PL All";
        } else if(reportName.equalsIgnoreCase("Feedback All") || 
                reportName.equalsIgnoreCase("Feedback 3PL") || 
                reportName.equalsIgnoreCase("Feedback ML")) {
            reportName = "Feedback_tasky";
        }
        return reportName;
    }
    
    public static List<RightnowReportFilterEntry> getReportFilters(String reportName) {
        List<RightnowReportFilterEntry> filters = new ArrayList<RightnowReportFilterEntry>();
        String value1 = null;
        String value2 = null;
        String field1 = RIGHTNOW_REPORT_FILTERS[0];
        String field2 = RIGHTNOW_REPORT_FILTERS[1];
        RightnowReportFilterEntry filter1 = null;
        RightnowReportFilterEntry filter2 = null;
        
        if(reportName.equalsIgnoreCase("BlueDart North")) {
            value1 = "Blue Dart";
            value2 = "Delhi-Gurgaon";            
        } else if(reportName.equalsIgnoreCase("BlueDart South")) {
            value1 = "Blue Dart";
            value2 = "Bangalore";
            //reportNameWithFilters = "3PL All###Blue Dart#Bangalore";
        } else if(reportName.equalsIgnoreCase("FirstFlight North")) {
            value1 = "FIRST FLIGHT COURIERS LTD";
            value2 = "Delhi-Gurgaon";
            //reportNameWithFilters = "3PL All###FIRST FLIGHT COURIERS LTD#Delhi-Gurgaon";
        } else if(reportName.equalsIgnoreCase("FirstFlight South")) {
            value1 = "FIRST FLIGHT COURIERS LTD";
            value2 = "Bangalore";
            //reportNameWithFilters = "3PL All###FIRST FLIGHT COURIERS LTD#Bangalore";
        } else if(reportName.equalsIgnoreCase("Overnight Express North")) {
            value1 = "Overnite";
            value2 = "Delhi-Gurgaon";
            //reportNameWithFilters = "3PL All###Overnite#Delhi-Gurgaon";
        } else if(reportName.equalsIgnoreCase("Overnight Express South")) {
            value1 = "Overnite";
            value2 = "Bangalore";
            //reportNameWithFilters = "3PL All###Overnite#Bangalore";
        } else if(reportName.equalsIgnoreCase("Aramex")) {
            value1 = "Aramex";
            //reportNameWithFilters = "3PL All###Aramex";
        } else if(reportName.equalsIgnoreCase("DTDC")) {
            value1 = "DTDC";
            //reportNameWithFilters = "3PL All###DTDC";
        } else if(reportName.equalsIgnoreCase("Delhivery")) {
            value1 = "Delhivery";
            //reportNameWithFilters = "3PL All###Delhivery";
        } else if(reportName.equalsIgnoreCase("Ecomm Express")) {
            value1 = "Ecom Express";
            //reportNameWithFilters = "3PL All###Ecomm Express";
        } else if(reportName.equalsIgnoreCase("Fedex")) {
            value1 = "Fedex";
            //reportNameWithFilters = "3PL All###Fedex";
        } else if(reportName.equalsIgnoreCase("India On Time")) {
            value1 = "Indiaontime";
            //reportNameWithFilters = "3PL All###India On Time";
        } else if(reportName.equalsIgnoreCase("Quantium")) {
            value1 = "Quantium";
            //reportNameWithFilters = "3PL All###Quantium";
        } else if(reportName.equalsIgnoreCase("VRL Logistics")) {
            value1 = "VRL Logistics";
            //reportNameWithFilters = "3PL All###VRL Logistics";
        } else if(reportName.equalsIgnoreCase("Feedback 3PL")) {
            value1 = "71";
            field1 = "Department";
        }  else if(reportName.equalsIgnoreCase("Feedback ML")) {
            value1 = "52";
            field1 = "Department";
        }
        
        if(value1 != null) {
            filter1 = new RightnowReportFilterEntry();
            filter1.setProperty(field1);
            filter1.setValues(new String[]{value1});
            filters.add(filter1);
        }
        
        if(value2 != null) {
            filter2 = new RightnowReportFilterEntry();
            filter2.setProperty(field2);
            filter2.setValues(new String[]{value2});
            filters.add(filter2);
        }
        return filters;
    }
    
    public static String convertNoteListToTextualForm(List<RightnowNoteEntry> noteEntries) {
        if(noteEntries == null)
            return null;
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy (E) hh:mm");
        StringBuilder finalNote = new StringBuilder();
        int length = noteEntries.size();
        for(int i = 0; i < length; i++) {
            RightnowNoteEntry note = noteEntries.get(i);
            StringBuilder noteText = new StringBuilder("[" + (i+1) + "]   ***   ");
            noteText.append(note.getText()).append("   ***   ");
            noteText.append(dateFormat.format(note.getCreatedTime())).append("   ***   ");
            noteText.append(note.getCreatedBy());
            StringBuilder newLineSeparator = new StringBuilder("\n");
            for(int l = 0; l < 100; l++) {
                newLineSeparator.append("*");
            }
            newLineSeparator.append("\n\n");
            finalNote.append(noteText.toString()).append(newLineSeparator.toString());
        }
        return finalNote.toString();
    }
}
