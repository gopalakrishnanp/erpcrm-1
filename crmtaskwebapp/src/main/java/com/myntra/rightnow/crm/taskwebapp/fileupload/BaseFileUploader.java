/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.rightnow.crm.taskwebapp.fileupload;

import com.myntra.commons.excel.poiImpl.MyntraExcelReader;
import com.myntra.commons.excel.poiImpl.MyntraSheetReader;
import com.myntra.rightnow.crm.taskwebapp.fileupload.exceptions.FileUploadExceptionMessage;
import com.myntra.rightnow.crm.taskwebapp.fileupload.exceptions.FileUploadSheetMessages;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author preetam
 */
public abstract class BaseFileUploader implements Serializable {
    
    public static final int MAX_NO_OF_EMPTY_ROWS_TO_STOP_PRCESSING = 3;
    
    //error messages in a sheet
    public List<FileUploadExceptionMessage> errorMessagesList = null;
    //all error messages in each of the sheet (composed by sheet name)
    public List<FileUploadSheetMessages> allErrorMessages = null;
    
    public void handleFileUpload(FileUploadEvent event) {
        InputStream inputStream;

        try {
            UploadedFile file = event.getFile();
            inputStream = file.getInputstream();
            MyntraExcelReader excelReader = new MyntraExcelReader(inputStream);

            //get all the sheets of the excel file and iterate them to process the data
            List<MyntraSheetReader> sheets = excelReader.getSheetReaders();

            allErrorMessages = new ArrayList<FileUploadSheetMessages>();
            if (sheets.size() > 0) {
                MyntraSheetReader sheet = sheets.get(0);
                errorMessagesList = new ArrayList<FileUploadExceptionMessage>();
                processEachExcelSheet(sheet);
                if (!errorMessagesList.isEmpty()) {
                    addSheetErrorMessage(sheet.getSheetName());
                    //allErrorMessages.add(new FileUploadSheetMessages(sheet.getSheetName(), errorMessagesList));
                }
            }
            
            showUploadStatusMessages(allErrorMessages, file.getFileName());
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("Exception in handleFileUpload " + e.getMessage());
        } 
    }
    
    public abstract void processEachExcelSheet(MyntraSheetReader sheet);
    
    public abstract boolean validateColumnHeaders(MyntraSheetReader sheet);
    
    public abstract void validateData(MyntraSheetReader sheet);
    
    public abstract void showUploadStatusMessages(List<FileUploadSheetMessages> errorMessages, String fileName);
    
    
    public void addErrorMessage(String error) {
        if(errorMessagesList == null) {
            errorMessagesList = new ArrayList<FileUploadExceptionMessage>();
        }
        //create an instance of FileUploadExceptionMessage
        FileUploadExceptionMessage msg = new FileUploadExceptionMessage(error, FileUploadExceptionMessage.ERROR);
        errorMessagesList.add(msg);
    }
    
    public void addSheetErrorMessage(String sheetName) {
        if(allErrorMessages == null) {
            allErrorMessages = new ArrayList<FileUploadSheetMessages>();
        }
        
        FileUploadSheetMessages sheetMessage = new FileUploadSheetMessages(sheetName, errorMessagesList);
        allErrorMessages.add(sheetMessage);
    }
}
