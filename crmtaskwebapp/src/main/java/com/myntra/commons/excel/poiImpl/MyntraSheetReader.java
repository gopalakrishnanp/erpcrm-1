package com.myntra.commons.excel.poiImpl;

import com.myntra.commons.excel.MyntraSheetReaderInterface;
import com.myntra.commons.excel.enums.CellType;
import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;

public class MyntraSheetReader implements MyntraSheetReaderInterface {

    private Sheet sheet;
    private int rowNum; /*To keep track of current row number*/

    private int colNum; /*To keep track of current column number*/

    private int lastRowNum; /*To keep track of number of rows in the sheet*/

    private String mode; /*To keep track of writing move (row or column)*/

    private Map<String, String> namedCellMap;//to store the mapping of named string to its reference

    /**
     * Constructor. Initialises sheet and set rowNum and colNum to -1 and stores
     * lastRowNum(-1 if no rows) To read a new row, user has to check if it
     * exists using hasNextRow() function and if it does, then call nextRow()
     * function By default reading mode is row mode. User can change it by
     * calling setColumnMode() function references stores all the named
     * reference made in the sheet so as to check while referencing a cell by
     * its named reference
     *
     * @param sheetName Name of the sheet to read from workbook
     * @param workbook from which sheets needs to be read
     * @throws ExcelPOIWrapperException
     */
    public MyntraSheetReader(String sheetName, Workbook workbook) throws ExcelPOIWrapperException {
        if (sheetName == null) {
            throw new ExcelPOIWrapperException("Sheet name cannot be null");
        }
        if (workbook == null) {
            throw new ExcelPOIWrapperException("workbook cannot be null");
        }
        this.sheet = workbook.getSheet(sheetName);
        this.rowNum = -1;
        this.colNum = -1;
        //this.lastRowNum = sheet.getLastRowNum(); //=> gives 0 if no rows present and when 1 row preset. So ambiguous(in old versions but not now)
        //this.lastRowNum = sheet.getPhysicalNumberOfRows() -1; //returns the no. of rows removing empty row. problem if empty rows are in between
		/*if(sheet.getPhysicalNumberOfRows() ==0 ){
         this.lastRowNum = -1;
         }
         else{*/
        this.lastRowNum = sheet.getLastRowNum();
        //}
        mode = MODE_ROW;
        namedCellMap = new HashMap<String, String>();

        //System.out.println(lastRowNum + " is the last row number");
    }

    public int currentRowNum(){
        return rowNum;
    }
    public int lastRowNum(){
        return this.lastRowNum;
    }
//    public int delte_max(){
//        return sheet.getLastRowNum();
//    }
//    public int delete_col(){
//        return sheet.getRow(rowNum).getPhysicalNumberOfCells();
//    }
    @Override
    public String getSheetName() {
        return sheet.getSheetName();
    }

    @Override
    public void nextRow() throws ExcelPOIWrapperException {
        if (mode.equals(MODE_COLUMN)) {
            throw new ExcelPOIWrapperException("Cannot call function nextRow() in Column mode.Switch to row mode or call nextColumn");
        }
        rowNum++;
        colNum = 0;
//        colNum = -1;
//        colNum++;
    }

    @Override
    public Boolean isHidden(){
        Boolean hidden = false;
        Workbook wb = sheet.getWorkbook();
        int sheetNum = wb.getSheetIndex(sheet.getSheetName());
        if(wb.isSheetHidden(sheetNum) || wb.isSheetVeryHidden(sheetNum)){
            hidden = true;
        }
        return hidden;
    }
    
    @Override
    public Boolean hasNextRow() throws ExcelPOIWrapperException {
        if (mode.equals(MODE_COLUMN)) {
            throw new ExcelPOIWrapperException("Cannot call function hasNextRow() in Column mode.Switch to row mode or call hasNextColumn");
        }
        //if(mode.equals(MODE_ROW)){
        //System.out.println(rowNum + " " + lastRowNum);
        if (this.rowNum < this.lastRowNum) {
            return true;
        } else {
            return false;
        }
        /*}
         else
         return false;*/
    }

    @Override
    public void nextColumn() throws ExcelPOIWrapperException {
        if (mode.equals(MODE_ROW)) {
            throw new ExcelPOIWrapperException("Cannot call function nextColumn() in row mode.Switch to column mode or call nextRow");
        }
        colNum++;
        rowNum = -1;
        rowNum++;
    }

    @Override
    public Boolean hasNextColumn() throws ExcelPOIWrapperException {
        if (mode.equals(MODE_ROW)) {
            throw new ExcelPOIWrapperException("Cannot call function nextColumn() in row mode.Switch to column mode or call nextRow");
        }
        //if(mode.equals(MODE_COLUMN)){
        if (sheet.getPhysicalNumberOfRows() == 0) {
            return false;
        }
        Cell cell = sheet.getRow(0).getCell(colNum + 1);
        if (cell == null) {
            return false;
        } else {
            return true;
        }
        /*}
         else
         return false;*/
    }

    @Override
    public void nextCell() {
        if (mode.equals(MODE_COLUMN)) {
            rowNum++;
        } else {
            colNum++;
        }
    }
    
    @Override
    public Boolean isCellEmpty(){
        Cell cell = sheet.getRow(rowNum).getCell(colNum);
        if(cell == null){
            return true;
        }
        else if(cell.getCellType() == Cell.CELL_TYPE_BLANK){
            return true;
        }
        return false;
    }

    @Override
    public Boolean hasCell() {
        //System.out.println(rowNum +" "+colNum);
        //if row mode
        if (mode.equals(MODE_ROW)) {
            Row row = sheet.getRow(rowNum);
            if (row == null) {
                return false;
            } else if (colNum <= row.getLastCellNum() - 1) {
                return true;
            } else {
                return false;
            }
        } //if column mode
        else {
            if (rowNum > lastRowNum) {
                return false;
            } else if (sheet.getRow(rowNum) == null) {
                return false;
            } else if (sheet.getRow(rowNum).getCell(colNum) == null) {
                return false;
            } else {
                return true;
            }
        }
    }

    @Override
    public void setColumnMode() {
        mode = MODE_COLUMN;
    }

    @Override
    public void setRowMode() {
        mode = MODE_ROW;
    }

    @Override
    public void setNamedCell(String name) {
        //System.out.println(rowNum + " " + colNum);
        CellReference cellR = new CellReference(rowNum, colNum);

        String ret = cellR.formatAsString();
        namedCellMap.put(name, ret);

    }

    @Override
    public void moveToNamedCell(String name) throws ExcelPOIWrapperException {
        if (namedCellMap.get(name) == null) {
            throw new ExcelPOIWrapperException("Reference " + name + " does not exists");
        }
        String ref = namedCellMap.get(name);
        CellReference cellR = new CellReference(ref);
        rowNum = cellR.getRow();
        colNum = cellR.getCol();
    }

    @Override
    public CellType getCellType() {
        //System.out.println(rowNum + " " + colNum);
        Cell cell = sheet.getRow(rowNum).getCell(colNum);
        if (cell == null) {
            return CellType.DOESNOTEXISTS;
        }
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_BOOLEAN:
                return CellType.BOOLEAN;
            case Cell.CELL_TYPE_STRING:
                return CellType.STRING;
            case Cell.CELL_TYPE_BLANK:
                return CellType.BLANK;
            case Cell.CELL_TYPE_FORMULA:
                return CellType.FORMULA;
            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return CellType.DATE;
                } else {
                    return CellType.NUMERIC;
                }
            default:
                return CellType.NUMERIC;
        }
    }
    
    @Override
    public Object readCellValue(Boolean... readAndMoveParams) throws ExcelPOIWrapperException {
        Cell cell = sheet.getRow(rowNum).getCell(colNum);
        Object val;
        if (cell != null) {
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_BOOLEAN:
                    val = (Boolean) cell.getBooleanCellValue();
                    break;
                case Cell.CELL_TYPE_STRING:
                    val = (String) cell.getStringCellValue();
                    break;
                case Cell.CELL_TYPE_BLANK:
                    val = "";
                    break;
                case Cell.CELL_TYPE_FORMULA:
                    val = cell.getCellFormula();
                case Cell.CELL_TYPE_NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        val = (Date) cell.getDateCellValue();
                    } else {
                        val = (Double) cell.getNumericCellValue();
                    }
                    break;
                default:
                    val = null;
            }
        } else {
            val = null;
        }
        //check if boolean parameters are present. If not then by default write and move. Else use 1st param bool value
        if (moveToNextCell(readAndMoveParams)) {
            nextCell();
        }
        return val;
    }

    @Override
    public double readNumericCellValue(Boolean... readAndMoveParams) throws ExcelPOIWrapperException {
        Cell cell = sheet.getRow(rowNum).getCell(colNum);
        Double val;
        if (cell == null) {
            String msg = "cell is blank";
            throw new ExcelPOIWrapperException(msg);
        } else {
            if (cell.getCellType() != Cell.CELL_TYPE_NUMERIC) {
                String msg = "Not a numeric value";
                throw new ExcelPOIWrapperException(msg);
            }
            val = cell.getNumericCellValue();
        }
        //check if boolean parameters are present. If not then by default write and move. Else use 1st param bool value
        if (moveToNextCell(readAndMoveParams)) {
            nextCell();
        }
        return val;
    }

    @Override
    public String readStringCellValue(Boolean... readAndMoveParams) throws ExcelPOIWrapperException {
        Cell cell = sheet.getRow(rowNum).getCell(colNum);
        String val;
        if (cell == null) {
            val = ""; //empty string
        } else {
            if (cell.getCellType() != Cell.CELL_TYPE_STRING) {
                String msg = "Not a String value. Its a " + cell.getCellType() + " type";
                throw new ExcelPOIWrapperException(msg);
            }
            val = cell.getStringCellValue();
        }
        //check if boolean parameters are present. If not then by default write and move. Else use 1st param bool value
        if (moveToNextCell(readAndMoveParams)) {
            nextCell();
        }
        return val;
    }

    @Override
    public Date readDateCellValue(Boolean... readAndMoveParams) throws ExcelPOIWrapperException {
        Cell cell = sheet.getRow(rowNum).getCell(colNum);
        Date val;
        if (cell == null) {
            val = null;
        } else {
            val = cell.getDateCellValue();
        }

        //check if boolean parameters are present. If not then by default write and move. Else use 1st param bool value
        if (moveToNextCell(readAndMoveParams)) {
            nextCell();
        }
        return val;
    }

    @Override
    public Boolean readBoolCellValue(Boolean... readAndMoveParams) throws ExcelPOIWrapperException {
        Cell cell = sheet.getRow(rowNum).getCell(colNum);
        Boolean val;
        if (cell == null) {
            val = false;
        } else {
            if (cell.getCellType() != Cell.CELL_TYPE_BOOLEAN) {
                String msg = "Not a boolean value";
                throw new ExcelPOIWrapperException(msg);
            }
            val = cell.getBooleanCellValue();
        }
        //check if boolean parameters are present. If not then by default read and move. Else use 1st param bool value
        if (moveToNextCell(readAndMoveParams)) {
            nextCell();
        }
        return val;
    }

    /**
     * @param readAndMoveParams variable argument(varargs : present in java
     * >1.5). Only the 1st argument is read and it denotes if cursor moves to
     * next cell after reading. If no arguments are provided by default it
     * returns true
     * @return false if 1st argument is false else true
     * @throws ExcelPOIWrapperException
     */
    private Boolean moveToNextCell(Boolean... readAndMoveParams) throws ExcelPOIWrapperException {
        //check if boolean parameters are present. If not return true. Else use 1st param bool value
        Boolean readAndMove;
        if (readAndMoveParams == null) {
            throw new ExcelPOIWrapperException("Cannot pass null value. Pass boolean or don't pass anthing");
        }
        if (readAndMoveParams.length > 0) {
            if (readAndMoveParams[0] != null) {
                readAndMove = readAndMoveParams[0].booleanValue();
            } else {
                throw new ExcelPOIWrapperException("Cannot pass null value. Pass boolean or don't pass anthing");
            }
        } else {
            readAndMove = true;
        }
        return readAndMove;
    }

    @Override
    public void setRow(int rowNum) {
        this.rowNum = rowNum;
    }

    @Override
    public void setColumn(int colNum) {
        this.colNum = colNum;
    }
    
    public int getColumn() {
        return this.colNum;
    }
}
