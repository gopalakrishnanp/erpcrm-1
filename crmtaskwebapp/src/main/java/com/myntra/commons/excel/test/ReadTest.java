package com.myntra.commons.excel.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.myntra.commons.excel.enums.CellType;
import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import com.myntra.commons.excel.poiImpl.MyntraExcelReader;
import com.myntra.commons.excel.poiImpl.MyntraSheetReader;

public class ReadTest {
	public static void main(String[] args) throws ExcelPOIWrapperException, InvalidFormatException{
		try {
			//String file = "/home/paroksh/myntra/CMS/paroTesting/error.xls";
			//String file = "/home/paroksh/myntra/CMS/paroTesting/wrapper1.xls";
                  	String file = "D:\\myntra-files\\Reports\\Category.xlsx";
			FileInputStream f = new FileInputStream(file);
			MyntraExcelReader excelReader = new MyntraExcelReader(f);
                        //getRowNum(excelReader);
                        printDataType(excelReader); //print data types
				//printInMixMode(excelReader); // prints in mix read modes
			f.close();
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}		
	}
        
        public static void getRowNum(MyntraExcelReader excelReader) throws ExcelPOIWrapperException{
            List<MyntraSheetReader> sheets = excelReader.getSheetReaders();
            Iterator<MyntraSheetReader> itr = sheets.iterator();
            while(itr.hasNext()){
                MyntraSheetReader sheet = itr.next();
                System.out.println(sheet.getSheetName() +" :"+ sheet.lastRowNum());
            }
        }
	public static void printDataType(MyntraExcelReader excelReader) throws ExcelPOIWrapperException{
		List<MyntraSheetReader> sheets = excelReader.getSheetReaders();
		Iterator<MyntraSheetReader> itr = sheets.iterator();
		//itr.next();
		//itr.next();
                
                while(itr.hasNext()){
			MyntraSheetReader sheetReader = itr.next();
                        if(sheetReader.isHidden()){
                            System.out.println("hidden sheet");
                        }
			String ArticleType = sheetReader.getSheetName();
			System.out.println("Article Type "+ArticleType);
			while(sheetReader.hasNextRow()){
				sheetReader.nextRow();
				while(sheetReader.hasCell()){
                                     Object par = sheetReader.readCellValue();
                                     if(par != null) {
                                    	 System.out.print(par);
                                    	 System.out.print("		");
                                     }
                                    //String parL = par.toString();
                                    //Long x1 = (long) Double.parseDouble(parL);
                                    //System.out.println(x1);
                                    //Long x = Long.getLong(parL);
					//System.out.print( x1.toString()+ "\t");
					//sheetReader.nextCell();
				}
				System.out.println("");
			}
			
		}
	}
	public static void printInMixMode(MyntraExcelReader excelReader) throws ExcelPOIWrapperException{
		List<MyntraSheetReader> sheets = excelReader.getSheetReaders();
		Iterator<MyntraSheetReader> itr = sheets.iterator();
		//itr.next();
		//itr.next();
		while(itr.hasNext()){
			MyntraSheetReader sheetReader = itr.next();
			String ArticleType = sheetReader.getSheetName();
			System.out.println("Article Type "+ArticleType);
			sheetReader.setColumnMode();
			while(sheetReader.hasNextColumn()){
				sheetReader.nextColumn();
				while(sheetReader.hasCell()){
					if(sheetReader.getCellType() != null && sheetReader.getCellType() != CellType.BLANK){
					System.out.print(sheetReader.readCellValue() + "\t");
					}
					else{
						System.out.print("cell is blank ");
						sheetReader.nextCell();
					}
				}
				System.out.println("");
			}
			
		}
	}
	
}
