package com.myntra.commons.excel.enums;

public enum CellType {
	NUMERIC(0),
	STRING(1),
	BOOLEAN(2),
	DATE(3),
	BLANK(4),
	FORMULA(5),
	DOESNOTEXISTS(6);
	
	
	private final int value;
	
	private CellType(int value){
		this.value = value;
	}
	
	public Integer value() { return this.value; }
	
	public static CellType create(int cellType){
		switch(cellType){
			case 0: return NUMERIC;
			case 1: return STRING;
			case 2: return BOOLEAN;
			case 3: return DATE;
			case 4: return BLANK;
			case 5: return FORMULA;
			case 6: return DOESNOTEXISTS;
			default: return NUMERIC;
		}
	}
	
	
}
