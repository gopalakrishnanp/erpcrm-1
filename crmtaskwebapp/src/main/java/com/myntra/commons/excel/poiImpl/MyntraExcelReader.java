package com.myntra.commons.excel.poiImpl;

import com.myntra.commons.excel.MyntraExcelReaderInterface;
import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * @author paroksh Class to help user read excel sheets. It can return sheets
 * present in the excel file in the form of MyntraSheetReader object
 */
public class MyntraExcelReader implements MyntraExcelReaderInterface {

    private Workbook workbook;

    /**
     * @author paroksh Constructor. Creates a workbook reading all sheets
     * present in the excel file specified
     * @param file Input file stream from which sheets should be read
     * @throws IOException
     * @throws ExcelPOIWrapperException
     * @throws InvalidFormatException
     */
    public MyntraExcelReader(InputStream file) throws IOException, ExcelPOIWrapperException, InvalidFormatException {
        if (file == null) {
            throw new ExcelPOIWrapperException("Input file is null");
        }
        workbook = WorkbookFactory.create(file);
    }

    @Override
    public List<MyntraSheetReader> getSheetReaders() throws ExcelPOIWrapperException {
        List<MyntraSheetReader> list = new ArrayList<MyntraSheetReader>();
        int numSheets = workbook.getNumberOfSheets();
        for (int i = 0; i < numSheets; i++) {
            Sheet sheet = workbook.getSheetAt(i);
            String sheetName = sheet.getSheetName();
            MyntraSheetReader myntraSheetReader = new MyntraSheetReader(sheetName, workbook);
            list.add(myntraSheetReader);
        }
        return list;
    }
}
