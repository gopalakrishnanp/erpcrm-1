JAVA_OPTS="$JAVA_OPTS -Xms1024m -Xmx2048m -XX:PermSize=256m -XX:MaxPermSize=256m"
CATALINA_PID="/myntra/pid/taskwebapp.pid"
export CATALINA_PID

if [ ! -e "$CATALINA_PID" ]
then
    touch "$CATALINA_PID"
fi

