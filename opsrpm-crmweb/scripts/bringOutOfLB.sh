#!/bin/bash
servicesToBlockFile=/var/www/html/servicesToBlock.php
serviceName='prism'

if [ -f $servicesToBlockFile ]
then
    echo "Removing service - $serviceName - from LB"
    sed -i "s/^#'$serviceName'/'$serviceName'/g" $servicesToBlockFile
fi
