package com.myntra.erp.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.erp.crm.client.entry.CashbackLogEntry;
import com.myntra.erp.crm.client.response.CashbackResponse;

@Path("/cashback/")
public interface CashbackService extends BaseService<CashbackResponse, CashbackLogEntry> {

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	AbstractResponse getCashbackLogs(@QueryParam("login") String login) throws ERPServiceException;
}
