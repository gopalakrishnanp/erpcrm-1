package com.myntra.erp.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.erp.crm.client.entry.TripDeliveryStaffEntry;
import com.myntra.erp.crm.client.response.TripDeliveryStaffResponse;

/**
 * Manager interface(abstract) to retrieve trip delivery staff along with
 * delivery center detail
 * 
 * @author Arun Kumar
 */
public interface TripDeliveryStaffManager extends BaseManager<TripDeliveryStaffResponse, TripDeliveryStaffEntry> {

	public TripDeliveryStaffResponse getTripDeliveryStaffDetail(Long deliveryStaffId, boolean isDCDetailNeeded)
			throws ERPServiceException;
}