/**
 * 
 */
package com.myntra.erp.crm.manager.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.cache.annotation.Cacheable;

import com.myntra.catalog.client.ProductClient;
import com.myntra.catalog.client.domain.enums.ImageQuality;
import com.myntra.catalog.client.domain.enums.ImageType;
import com.myntra.catalog.client.domain.response.ImageParameters;
import com.myntra.catalog.client.domain.response.ProductDetailResponse;
import com.myntra.catalog.client.domain.response.ProductEntry;
import com.myntra.client.wms.ItemServiceClient;
import com.myntra.client.wms.SkuServiceClient;
import com.myntra.client.wms.location.WarehouseServiceClient;
import com.myntra.client.wms.response.ItemEntry;
import com.myntra.client.wms.response.ItemResponse;
import com.myntra.client.wms.response.SkuEntry;
import com.myntra.client.wms.response.SkuResponse;
import com.myntra.client.wms.response.location.WarehouseEntry;
import com.myntra.client.wms.response.location.WarehouseResponse;
import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.test.TestHelper;
import com.myntra.commons.utils.Context;
import com.myntra.commons.utils.ServiceURLProperty;
import com.myntra.erp.crm.util.WebserviceUtil;
import com.myntra.lms.client.DeliveryCenterServiceClient;
import com.myntra.lms.client.DeliveryStaffServiceClient;
import com.myntra.lms.client.OrderTrackingServiceClient;
import com.myntra.lms.client.TripServiceClient;
import com.myntra.lms.client.response.DeliveryCenterEntry;
import com.myntra.lms.client.response.DeliveryCenterResponse;
import com.myntra.lms.client.response.DeliveryStaffEntry;
import com.myntra.lms.client.response.DeliveryStaffResponse;
import com.myntra.lms.client.response.OrderTrackingEntry;
import com.myntra.lms.client.response.OrderTrackingResponse;
import com.myntra.lms.client.response.PincodeEntry;
import com.myntra.lms.client.response.PincodeResponse;
import com.myntra.lms.client.response.TripOrderAssignementEntry;
import com.myntra.lms.client.response.TripOrderAssignmentResponse;
import com.myntra.oms.client.OrderClient;
import com.myntra.oms.client.entry.OrderEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.oms.client.response.OrderReleaseResponse;
import com.myntra.oms.client.response.OrderResponse;
import com.myntra.portal.crm.client.OrderPortalClient;
import com.myntra.portal.crm.client.ReturnClient;
import com.myntra.portal.crm.client.entry.OrderExchangeEntry;
import com.myntra.portal.crm.client.entry.OrderRtoEntry;
import com.myntra.portal.crm.client.entry.ReturnEntry;
import com.myntra.portal.crm.client.response.OrderExchangeResponse;
import com.myntra.portal.crm.client.response.OrderRtoResponse;
import com.myntra.portal.crm.client.response.ReturnResponse;

/**
 * Wrapper APIs to connect to external(other than crm erp services) services by
 * respective clients
 * 
 * @author arunkumar
 * 
 */
public class ServiceManagerUtil {

	private static final Logger LOGGER = Logger.getLogger(ServiceManagerUtil.class);

	@Cacheable(value = "warehouseEntryCache")
	public WarehouseEntry getWarehouseEntry(Integer warehouseId) throws ERPServiceException {

		LOGGER.debug("GettingWarehouseDetails for " + warehouseId);
		if (warehouseId != null && warehouseId != 0) {
			WarehouseResponse warehouseResponse = WarehouseServiceClient.findById(null, (long) warehouseId,
					Context.getContextInfo());

			if (warehouseResponse != null) {
				List<WarehouseEntry> warehouseEntryList = warehouseResponse.getData();

				if (warehouseEntryList != null) {
					WarehouseEntry warehouseEntry = warehouseEntryList.get(0);
					return warehouseEntry;
				}
			}
		}

		return null;
	}

	public List<ReturnEntry> getReturnDetail(Long returnId, Long orderId, String login) throws ERPServiceException {

		ReturnResponse returnResponse = ReturnClient.getCustomerReturn(null, returnId, orderId, login);

		if (returnResponse != null) {
			List<ReturnEntry> returnEntryList = returnResponse.getReturnEntryList();
			return returnEntryList;
		}

		return null;
	}

	public HashMap<Long, Long> getExchangeDetailMap(Long shipmentId) throws ERPServiceException {

		HashMap<Long, Long> itemExchangeIDMap = new HashMap<Long, Long>();

		OrderExchangeResponse exchangeResponse = OrderPortalClient.getExchangeDetail(null, shipmentId);

		if (exchangeResponse != null) {
			List<OrderExchangeEntry> orderExchangeEntryList = exchangeResponse.getOrderExchangeEntryList();

			if (orderExchangeEntryList != null) {
				for (OrderExchangeEntry entry : orderExchangeEntryList) {
					itemExchangeIDMap.put(entry.getItemId(), entry.getExchangeOrderId());
				}
			}
		}

		return itemExchangeIDMap;
	}

	public SkuEntry getSKUEntry(Long skuId, String skuCode) throws ERPServiceException {

		String searchParam = "";

		// search param can either be skuId or code
		if (skuId != null) {
			searchParam = "id.eq:" + skuId;
		}

		if (skuCode != null) {
			searchParam = "code.eq:" + skuCode;
		}

		if (skuId != null || skuCode != null) {
			SkuResponse skuResponse = SkuServiceClient.search(0, -1, null, null, searchParam, null,
					Context.getContextInfo());

			if (skuResponse != null) {
				List<SkuEntry> skuEntryList = skuResponse.getData();

				if (skuEntryList != null) {
					SkuEntry skuEntry = skuEntryList.get(0);
					return skuEntry;
				}
			}
		}

		return null;
	}

	public List<ItemEntry> getSKUItemEntry(Long orderId) throws ERPServiceException {

		String searchParam = "orderId.eq:" + orderId;

		if (orderId != null) {

			ItemResponse itemResponse = ItemServiceClient.search(0, -1, null, null, searchParam, null,
					Context.getContextInfo());

			if (itemResponse != null) {
				List<ItemEntry> itemEntryList = itemResponse.getData();
				return itemEntryList;
			}
		}

		return null;
	}

	@Cacheable(value = "stylePropertyCache")
	public ProductEntry getStyleProperty(Long styleId) throws ERPServiceException {
		if (styleId != null) {			
			ContextInfo context = WebserviceUtil.getContextInfo();			
			ProductDetailResponse styleResponse = ProductClient.findById(null, styleId, null, context);			

			if (styleResponse != null && styleResponse.getStatus().getStatusType().equals("SUCCESS")
					&& styleResponse.getData() != null && styleResponse.getData().size() > 0) {
				ProductEntry styleEntry = styleResponse.getData().get(0);
				return styleEntry;
			}
		}
		return null;
	}

	public ProductEntry getStylePropertyBySku(Long skuId) throws ERPServiceException {
		if (skuId != null) {
			String searchTerms = "productOptions.sku.skuId.eq:" + skuId;
			ProductDetailResponse styleResponse = ProductClient.search(null, searchTerms, null, 0, 1, "productId", "ASC",
					Context.getContextInfo());

			if (styleResponse != null && styleResponse.getStatus().getStatusType().equals("SUCCESS")
					&& styleResponse.getData().size() > 0) {
				ProductEntry styleEntry = styleResponse.getData().get(0);
				return styleEntry;
			}
		}
		return null;
	}

	public OrderTrackingEntry getTrackingEntry(String trackingNumber, String courierCode) throws ERPServiceException {

		String trackingNo = trackingNumber;
		String code = courierCode;

		if (trackingNo != null && trackingNo != "" && code != null && code != "") {
			OrderTrackingResponse orderTrackingResponse = OrderTrackingServiceClient.getOrderTrackingDetail(null, code,
					trackingNo, Context.getContextInfo());

			if (orderTrackingResponse != null) {
				List<OrderTrackingEntry> orderTrackingEntryList = orderTrackingResponse.getOrderTrackings();

				if (orderTrackingEntryList != null) {
					OrderTrackingEntry orderTrackingEntry = orderTrackingEntryList.get(0);
					return orderTrackingEntry;
				}
			}
		}

		return null;
	}

	public List<TripOrderAssignementEntry> getTripAssignmentEntry(String trackingNumber) throws ERPServiceException {

		String trackingNo = trackingNumber;
		String searchTerms = "trackingNumber.eq:" + trackingNo;

		if (trackingNo != null && trackingNo != "") {
			TripOrderAssignmentResponse tripOrderAssignmentResponse = TripServiceClient.searchTripOrderAssignment(0,
					-1, "createdOn", "desc", searchTerms, null, Context.getContextInfo());

			if (tripOrderAssignmentResponse != null) {
				List<TripOrderAssignementEntry> tripOrderAssignmentEntryList = tripOrderAssignmentResponse
						.getTripOrders();

				return tripOrderAssignmentEntryList;
			}
		}

		return null;
	}

	@Cacheable(value = "deliveryStaffCache")
	public DeliveryStaffEntry getDeliveryStaffDetail(Long staffId) throws ERPServiceException {

		if (staffId != null) {
			DeliveryStaffResponse staffResponse = DeliveryStaffServiceClient.findById(null, staffId,
					Context.getContextInfo());

			if (staffResponse != null) {
				List<DeliveryStaffEntry> dcStaffEntryList = staffResponse.getDeliveryStaffs();

				if (dcStaffEntryList != null && dcStaffEntryList.size() > 0) {
					return dcStaffEntryList.get(0);
				}
			}
		}

		return null;
	}

	@Cacheable(value = "deliveryCenterCache")
	public DeliveryCenterEntry getDeliveryCenterEntry(Long DCId) throws ERPServiceException {

		if (DCId != null) {
			DeliveryCenterResponse dcResponse = DeliveryCenterServiceClient.findById(null, DCId,
					Context.getContextInfo());

			if (dcResponse != null) {
				List<DeliveryCenterEntry> dcEntryList = dcResponse.getDeliveryCenters();

				if (dcEntryList != null && dcEntryList.size() > 0) {
					return dcEntryList.get(0);
				}
			}
		}

		return null;
	}

	public OrderRtoEntry getRtoDetail(Long orderId) throws ERPServiceException {

		OrderRtoResponse response = OrderPortalClient.getRtoOrderDetail(null, orderId);

		if (response != null) {
			List<OrderRtoEntry> entryList = response.getOrderRtoEntryList();

			if (entryList != null) {
				return entryList.get(0);
			}
		}

		return null;
	}

	public List<OrderEntry> getOrderDetail(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		
		OrderResponse orderResponse = null;
		List<OrderEntry> orderEntryList = null;
		
		try {
			orderResponse = OrderClient.search(start, fetchSize, sortBy, sortOrder, searchTerms, null,
				Context.getContextInfo());
			
			if (orderResponse == null) {
				return null;
			}
			
			orderEntryList = orderResponse.getData();
		} catch (ERPServiceException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		// Possible Search for Shipment ID instead of Order ID
		// TODO move this to OMS Client codebase.
		BaseWebClient client = new BaseWebClient(null, ServiceURLProperty.OMS_URL, Context.getContextInfo());
		client.path("/oms/orderrelease/search");
		
		// note: id.eq in orderrelease is shipmentID, which in order it
		// is orderID
		// So we take the risk of re-using same searchTerm to get
		// possible shipment details.
		client.query("q", searchTerms);
		client.query("fetchSize", fetchSize);
		client.query("sortBy", sortBy);
		client.query("sortOrder", sortOrder);
		
		if (orderEntryList != null) {			
			if (orderEntryList.size() == 0 && searchTerms.startsWith("id.eq:")) {				
				OrderReleaseResponse releaseResponse = client.get(OrderReleaseResponse.class);
				List<OrderReleaseEntry> orderReleaseEntryList = releaseResponse.getData();
				
				if (orderReleaseEntryList.size() > 0) {
					Long orderID = orderReleaseEntryList.get(0).getOrderId();
					// Call origin order search with OrderID instead of shipmentID
					String searchTermCorrected = "id.eq:" + orderID;
					orderResponse = OrderClient.search(start, fetchSize, sortBy, sortOrder, searchTermCorrected, null,
							Context.getContextInfo());
					orderEntryList = orderResponse.getData();
				}
			}
			
		// if search based on order status then hit release service and the order service
		} else if(searchTerms.startsWith("releaseStatus.eq:") && orderEntryList == null){
			
			orderEntryList = new ArrayList<OrderEntry>();
			// TODO check for duplicate orderid among shipments before hitting order service
			// on orderid			
			
			try {
				OrderReleaseResponse releaseResponse = client.get(OrderReleaseResponse.class);
				List<OrderReleaseEntry> orderReleaseEntryList = releaseResponse.getData();
				
				if (orderReleaseEntryList.size() > 0) {
					for (OrderReleaseEntry releaseEntry : orderReleaseEntryList) {
						Long orderID = releaseEntry.getOrderId();

						// Call origin order search with OrderID instead of
						// shipmentID
						String searchTermCorrected = "id.eq:" + orderID;
						orderResponse = OrderClient.search(start, 1, null, null, searchTermCorrected, null,
								Context.getContextInfo());
						orderEntryList.addAll(orderResponse.getData());
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}

		return orderEntryList;
	}

	/**
	 * get DC detail from LMS which delivers the order
	 * @param pinCode
	 * @param courierCode
	 * @return PincodeEntry
	 * @throws ERPServiceException
	 */
	public PincodeEntry getDeliveryCenterOnPincode(String pinCode, String courierCode) throws ERPServiceException {

		if(pinCode == null || courierCode == null){
			return null;
		}
		
		String searchTerms = "number.eq:"+pinCode+"___courierCode.eq:"+courierCode;
		
		// TODO move this to LMS Client codebase.
		BaseWebClient client = new BaseWebClient(null, ServiceURLProperty.LMS_URL, Context.getContextInfo());
		client.path("/pincode/search");		
		client.query("q", searchTerms);		
		PincodeResponse response = client.get(PincodeResponse.class);
		
		if (response != null) {
			List<PincodeEntry> entryList = response.getPincodes();

			if (entryList != null) {
				return entryList.get(0);				
			}
		}

		return null;
	}
}
