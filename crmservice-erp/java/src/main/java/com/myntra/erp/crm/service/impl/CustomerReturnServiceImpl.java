package com.myntra.erp.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;
import com.myntra.erp.crm.client.response.CustomerReturnResponse;
import com.myntra.erp.crm.manager.CustomerReturnManager;
import com.myntra.erp.crm.service.CustomerReturnService;

/**
 * Customer return web service implementation which integrates detail of
 * return comments, skus, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
public class CustomerReturnServiceImpl extends BaseServiceImpl<CustomerReturnResponse, CustomerReturnEntry> implements
		CustomerReturnService {

	@Override
	public AbstractResponse getReturnDetail(Long returnId, Long orderId, String login, boolean isLogisticDetailNeeded) {
		try {
			return ((CustomerReturnManager) getManager()).getReturnDetail(returnId, orderId, login,
					isLogisticDetailNeeded);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}

	}

}
