package com.myntra.erp.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.erp.crm.client.entry.CashbackLogEntry;
import com.myntra.erp.crm.client.response.CashbackResponse;
import com.myntra.erp.crm.manager.CashbackManager;
import com.myntra.erp.crm.service.CashbackService;

/**
 * Cashback related web services.
 * 
 * @author Pravin Mehta
 */
public class CashbackServiceImpl extends BaseServiceImpl<CashbackResponse, CashbackLogEntry> implements CashbackService {

	@Override
	public AbstractResponse getCashbackLogs(String login) throws ERPServiceException {
		try {
            return ((CashbackManager) getManager()).getCashbackLogs(login);
		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}
}
