package com.myntra.erp.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.erp.crm.client.entry.CashbackLogEntry;
import com.myntra.erp.crm.client.response.CashbackResponse;

/**
 * Manager interface(abstract) for cashback.
 * 
 * @author Pravin Mehta
 */
public interface CashbackManager extends
		BaseManager<CashbackResponse, CashbackLogEntry> {

	public CashbackResponse getCashbackLogs(String login)
			throws ERPServiceException;
}