package com.myntra.erp.crm.client.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.entry.TripDeliveryStaffEntry;

/**
 * Response for trip delivery staff web service with detail delivery staff along
 * with delivery center detail
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "tripDeliveryStaffResponse")
public class TripDeliveryStaffResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;
	private TripDeliveryStaffEntry tripDeliveryStaffEntry;

	@XmlElement(name = "tripDeliveryStaffEntry")
	public TripDeliveryStaffEntry getTripDeliveryStaffEntry() {
		return tripDeliveryStaffEntry;
	}

	public void setTripDeliveryStaffEntry(TripDeliveryStaffEntry tripDeliveryStaffEntry) {
		this.tripDeliveryStaffEntry = tripDeliveryStaffEntry;
	}

	@Override
	public String toString() {
		return "TripDeliveryStaffResponse [tripDeliveryStaffEntry=" + tripDeliveryStaffEntry + "]";
	}

}