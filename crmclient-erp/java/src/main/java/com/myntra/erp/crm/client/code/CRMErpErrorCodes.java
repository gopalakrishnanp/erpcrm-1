package com.myntra.erp.crm.client.code;

import com.myntra.commons.codes.ERPErrorCodes;
import com.myntra.commons.codes.StatusCodes;

/**
 * CRM error codes
 * 
 * @author Arun Kumar
 * 
 */
public class CRMErpErrorCodes extends ERPErrorCodes {
	
	public static final StatusCodes NO_CUSTOMER_ORDER_RETRIEVED = new CRMErpErrorCodes(801, "NO_CUSTOMER_ORDER_RETRIEVED");
	public static final StatusCodes NO_CUSTOMER_ORDER_SUMMARY_RETRIEVED = new CRMErpErrorCodes(802,
			"NO_CUSTOMER_ORDER_SUMMARY_RETRIEVED");
	public static final StatusCodes NO_CUSTOMER_RETURN_RETRIEVED = new CRMErpErrorCodes(803, "NO_CUSTOMER_RETURN_RETRIEVED");		
	public static final StatusCodes NO_CASHBACK_LOG = new CRMErpErrorCodes(806, "NO_CASHBACK_LOG");
	public static final StatusCodes NO_TRIP_DELIVERY_STAFF_DETAIL = new CRMErpErrorCodes(807, "NO_TRIP_DELIVERY_STAFF_DETAIL");	
	
	private static final String BUNDLE_NAME = "CRMErpErrorCodes";
	

	public CRMErpErrorCodes(int errorCode, String errorMessage) {
		setAll(errorCode, errorMessage, BUNDLE_NAME);
	}

}
