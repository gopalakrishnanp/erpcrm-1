package com.myntra.erp.crm.client;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.test.TestHelper;
import com.myntra.commons.utils.ResponseHandler;
import com.myntra.erp.crm.client.response.CustomerOrderSummaryResponse;
import com.myntra.erp.crm.constant.CRMErpClientConstants;

/**
 * Web client for customer order summary web service
 * 
 * @author Arun Kumar
 */
public class CustomerOrderSummaryClient extends ResponseHandler {

	public static final String SERVICE_PREFIX = "/crm/orderSummary/";

	public static CustomerOrderSummaryResponse getCustomerOrderSummary(String serviceURL, String login)
			throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMErpClientConstants.CRM_ERP_URL, TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX);
		client.query("login", login);

		return client.get(CustomerOrderSummaryResponse.class);
	}

	public static void main(String args[]) throws ERPServiceException {
		CustomerOrderSummaryResponse test = getCustomerOrderSummary("http://localhost:7090/crmservice-erp",
				"arun.kumar@myntra.com");
		System.out.println(test);
	}

}