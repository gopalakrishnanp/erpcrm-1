package com.myntra.erp.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for customer order tracking detail fields
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "customerOrderTrackingDetail")
public class CustomerOrderTrackingDetailEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private String location = "NA";
	private Date actionDate;
	private String activityType;
	private String remark;

	public CustomerOrderTrackingDetailEntry(String location, Date actionDate, String activityType, String remark) {

		this.location = location;
		this.actionDate = actionDate;
		this.activityType = activityType;
		this.remark = remark;

	}

	public CustomerOrderTrackingDetailEntry() {
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CustomerOrderTrackingEntry [location =" + location + " actiondate=" + actionDate + " activityType="
				+ activityType + " remark=" + remark + "]" + super.toString() );
		return builder.toString();
	}
}
