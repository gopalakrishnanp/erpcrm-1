package com.myntra.erp.crm.client.entry;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * @author Pravin Mehta
 */
@XmlRootElement(name = "shipmentStatusRevenueEntry")
public class ShipmentStatusRevenueEntry extends BaseEntry {
	
	private static final long serialVersionUID = 1;
	
	private HashMap<String, Double> shipmentStatusRevenueMap;
	private HashMap<String, Integer> shipmentStatusQuantityMap;
	
	public ShipmentStatusRevenueEntry() {
	}

	public HashMap<String, Double> getShipmentStatusRevenueMap() {
		return shipmentStatusRevenueMap;
	}

	public void setShipmentStatusRevenueMap(HashMap<String, Double> shipmentStatusRevenueHashMap) {
		this.shipmentStatusRevenueMap = shipmentStatusRevenueHashMap;
	}
	
	public HashMap<String, Integer> getShipmentStatusQuantityMap() {
		return shipmentStatusQuantityMap;
	}

	public void setShipmentStatusQuantityMap(HashMap<String, Integer> shipmentStatusQuantityMap) {
		this.shipmentStatusQuantityMap = shipmentStatusQuantityMap;
	}

	@Override
	public String toString() {
		return "ShipmentStatusRevenueEntry [shipmentStatusRevenueMap=" + shipmentStatusRevenueMap
				+ ", shipmentStatusQuantityMap=" + shipmentStatusQuantityMap + "]";
	}

}
