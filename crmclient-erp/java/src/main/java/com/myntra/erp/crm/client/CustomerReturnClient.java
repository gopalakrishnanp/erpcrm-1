package com.myntra.erp.crm.client;

import java.util.ArrayList;
import java.util.List;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.test.TestHelper;
import com.myntra.commons.utils.ResponseHandler;
import com.myntra.erp.crm.client.code.CRMErpSuccessCodes;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;
import com.myntra.erp.crm.client.response.CustomerReturnResponse;
import com.myntra.erp.crm.constant.CRMErpClientConstants;

/**
 * Web client for customer return web service which integrates detail of return,
 * status, comments, skus, tracking, trip detail.
 * 
 * @author Arun Kumar
 */
public class CustomerReturnClient extends ResponseHandler {

	public static final String SERVICE_PREFIX = "/crm/return/";
	
	public static CustomerReturnResponse getReturnById(String serviceURL, Long returnId) throws ERPServiceException {		
		return getCustomerReturnDetail(serviceURL,returnId,null,null,true);
	}
	
	public static CustomerReturnResponse getReturnByShipmentId(String serviceURL, Long shipmentId) throws ERPServiceException {		
		return getCustomerReturnDetail(serviceURL,null,shipmentId,null,true);
	}

	public static CustomerReturnResponse getReturnByLogin(String serviceURL, String login) throws ERPServiceException {		
		return getCustomerReturnDetail(serviceURL,null,null,login,true);
	}
	// TODO need to develop an in query service for return, 
	// and can send multiple shipments in single request
	// now whatever we are doing here is not saving http requests.
	public static CustomerReturnResponse getReturnByShipmentIdList(String serviceURL, List<Long> shipmentIdList) throws ERPServiceException {
		CustomerReturnResponse response = new CustomerReturnResponse();
		List<CustomerReturnEntry> returnList = new ArrayList<CustomerReturnEntry>();
		
		if(shipmentIdList != null){
			for(Long shipmentId: shipmentIdList) {
				CustomerReturnResponse responseSingle = getCustomerReturnDetail(serviceURL,null,shipmentId,null,true);
				
				if(responseSingle != null && responseSingle.getStatus().getStatusType().equals("SUCCESS")){
					returnList.addAll(responseSingle.getCustomerReturnEntryList());
				}
			}
		}
		
		response.setCustomerReturnEntryList(returnList);
		
		StatusResponse success = new StatusResponse(CRMErpSuccessCodes.CUSTOMER_RETURN_RETRIEVED,
				StatusResponse.Type.SUCCESS, response.getCustomerReturnEntryList().size());
		response.setStatus(success);
		
		return response;
	}
	
	public static CustomerReturnResponse getCustomerReturnDetail(String serviceURL, Long returnId, Long orderId,
			String login, boolean isLogisticDetailNeeded) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMErpClientConstants.CRM_ERP_URL, TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX);
		
		if(returnId!=null) {
			client.query("returnId", returnId);
		}
		if(orderId!=null) {
			client.query("orderId", orderId);
		}
		if(login!=null && !login.trim().equals("")){
			client.query("login", login);	
		}
		
		client.query("isLogisticDetailNeeded", isLogisticDetailNeeded);

		return client.get(CustomerReturnResponse.class);
	}

	public static void main(String args[]) throws ERPServiceException {
		// 56932&orderId=2611060
		CustomerReturnResponse test = getCustomerReturnDetail("http://localhost:7090/crmservice-erp/", 56932L,
				2611060L, "pravin.mehta@myntra.com", true);
		System.out.println(test);
	}

}