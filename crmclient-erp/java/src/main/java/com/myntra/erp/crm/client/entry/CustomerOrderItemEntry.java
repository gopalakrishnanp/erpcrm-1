package com.myntra.erp.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for customer order item(sku) fields
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "customerOrderItem")
public class CustomerOrderItemEntry extends BaseEntry {

	private static final long serialVersionUID = 1;

	// unique ids
	private Long styleId;
	private Long optionId;
	private Long skuId;
	private Integer discountRuleId;
	private Integer discountRuleRevId;
	private Integer promotionId;
	private Long cancellationReasonId;
	private Long releaseId;
	private Long exchangeOrderId;
	private Long exchangeLineId;

	// status/reason
	private String status;
	private String statusDisplayName;
	private Boolean isDiscountedProduct;
	private Boolean isReturnableProduct;
	private String cancellationReason;
	private Boolean isCustomizable;

	// qty
	private Integer quantity;
	private Integer discountedQuantity;

	// amount detail
	private Double taxRate;
	private Double taxAmount;
	private Double unitPrice;
	private Double discount;
	private Double couponDiscount;
	private Double cartDiscount;
	private Double cashRedeemed;
	private Double pgDiscount;
	private Double finalAmount;
	private Double cashbackOffered;

	// date
	private Date cancelledOn;

	// sku detail
	private String name;
	private String code;
	private String lastUser;
	private Boolean adminDisabled;
	private String description;
	private String vendorArticleNo;
	private String vendorArticleName;
	private String size;
	private Integer brandId;
	private String brandName;
	private String brandCode;
	private Integer articleTypeId;
	private String articleTypeName;
	private String articleTypeCode;
	private String remarks;
	private Boolean jitSourced;
	private String gtin;
	private Boolean enabled;
	private Long version;
	private String styleName;
	private String searchImagezURL;
	private String supplyType;

	// sku item(wms item) detail
	private String itemBarcodes;

	// custom fields
	private String customizedMessage;
	private String customizedName;
	private String customizedNumber;

	// loyalty detail
	private Double loyaltyPointAwarded;
	private Double loyaltyPointUsed;
	private Double loyaltyAmountUsed;

	public Long getStyleId() {
		return styleId;
	}

	public void setStyleId(Long styleId) {
		this.styleId = styleId;
	}

	public Long getOptionId() {
		return optionId;
	}

	public void setOptionId(Long optionId) {
		this.optionId = optionId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getDiscountRuleId() {
		return discountRuleId;
	}

	public void setDiscountRuleId(Integer discountRuleId) {
		this.discountRuleId = discountRuleId;
	}

	public Integer getDiscountRuleRevId() {
		return discountRuleRevId;
	}

	public void setDiscountRuleRevId(Integer discountRuleRevId) {
		this.discountRuleRevId = discountRuleRevId;
	}

	public Integer getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Integer promotionId) {
		this.promotionId = promotionId;
	}

	public Long getCancellationReasonId() {
		return cancellationReasonId;
	}

	public void setCancellationReasonId(Long cancellationReasonId) {
		this.cancellationReasonId = cancellationReasonId;
	}

	public Long getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(Long releaseId) {
		this.releaseId = releaseId;
	}

	public Long getExchangeOrderId() {
		return exchangeOrderId;
	}

	public void setExchangeOrderId(Long exchangeOrderId) {
		this.exchangeOrderId = exchangeOrderId;
	}

	public Long getExchangeLineId() {
		return exchangeLineId;
	}

	public void setExchangeLineId(Long exchangeLineId) {
		this.exchangeLineId = exchangeLineId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDisplayName() {
		return statusDisplayName;
	}

	public void setStatusDisplayName(String statusDisplayName) {
		this.statusDisplayName = statusDisplayName;
	}

	public Boolean getIsDiscountedProduct() {
		return isDiscountedProduct;
	}

	public void setIsDiscountedProduct(Boolean isDiscountedProduct) {
		this.isDiscountedProduct = isDiscountedProduct;
	}

	public Boolean getIsReturnableProduct() {
		return isReturnableProduct;
	}

	public void setIsReturnableProduct(Boolean isReturnableProduct) {
		this.isReturnableProduct = isReturnableProduct;
	}

	public String getCancellationReason() {
		return cancellationReason;
	}

	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}

	public Boolean getIsCustomizable() {
		return isCustomizable;
	}

	public void setIsCustomizable(Boolean isCustomizable) {
		this.isCustomizable = isCustomizable;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getDiscountedQuantity() {
		return discountedQuantity;
	}

	public void setDiscountedQuantity(Integer discountedQuantity) {
		this.discountedQuantity = discountedQuantity;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getCouponDiscount() {
		return couponDiscount;
	}

	public void setCouponDiscount(Double couponDiscount) {
		this.couponDiscount = couponDiscount;
	}

	public Double getCartDiscount() {
		return cartDiscount;
	}

	public void setCartDiscount(Double cartDiscount) {
		this.cartDiscount = cartDiscount;
	}

	public Double getCashRedeemed() {
		return cashRedeemed;
	}

	public void setCashRedeemed(Double cashRedeemed) {
		this.cashRedeemed = cashRedeemed;
	}

	public Double getPgDiscount() {
		return pgDiscount;
	}

	public void setPgDiscount(Double pgDiscount) {
		this.pgDiscount = pgDiscount;
	}

	public Double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(Double finalAmount) {
		this.finalAmount = finalAmount;
	}

	public Double getCashbackOffered() {
		return cashbackOffered;
	}

	public void setCashbackOffered(Double cashbackOffered) {
		this.cashbackOffered = cashbackOffered;
	}

	public Date getCancelledOn() {
		return cancelledOn;
	}

	public void setCancelledOn(Date cancelledOn) {
		this.cancelledOn = cancelledOn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLastUser() {
		return lastUser;
	}

	public void setLastUser(String lastUser) {
		this.lastUser = lastUser;
	}

	public Boolean getAdminDisabled() {
		return adminDisabled;
	}

	public void setAdminDisabled(Boolean adminDisabled) {
		this.adminDisabled = adminDisabled;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVendorArticleNo() {
		return vendorArticleNo;
	}

	public void setVendorArticleNo(String vendorArticleNo) {
		this.vendorArticleNo = vendorArticleNo;
	}

	public String getVendorArticleName() {
		return vendorArticleName;
	}

	public void setVendorArticleName(String vendorArticleName) {
		this.vendorArticleName = vendorArticleName;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Integer getBrandId() {
		return brandId;
	}

	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public Integer getArticleTypeId() {
		return articleTypeId;
	}

	public void setArticleTypeId(Integer articleTypeId) {
		this.articleTypeId = articleTypeId;
	}

	public String getArticleTypeName() {
		return articleTypeName;
	}

	public void setArticleTypeName(String articleTypeName) {
		this.articleTypeName = articleTypeName;
	}

	public String getArticleTypeCode() {
		return articleTypeCode;
	}

	public void setArticleTypeCode(String articleTypeCode) {
		this.articleTypeCode = articleTypeCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Boolean getJitSourced() {
		return jitSourced;
	}

	public void setJitSourced(Boolean jitSourced) {
		this.jitSourced = jitSourced;
	}

	public String getGtin() {
		return gtin;
	}

	public void setGtin(String gtin) {
		this.gtin = gtin;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getStyleName() {
		return styleName;
	}

	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}

	public String getSearchImagezURL() {
		return searchImagezURL;
	}

	public void setSearchImagezURL(String searchImagezURL) {
		this.searchImagezURL = searchImagezURL;
	}

	public String getSupplyType() {
		return supplyType;
	}

	public void setSupplyType(String supplyType) {
		this.supplyType = supplyType;
	}

	public String getItemBarcodes() {
		return itemBarcodes;
	}

	public void setItemBarcodes(String itemBarcodes) {
		this.itemBarcodes = itemBarcodes;
	}

	public String getCustomizedMessage() {
		return customizedMessage;
	}

	public void setCustomizedMessage(String customizedMessage) {
		this.customizedMessage = customizedMessage;
	}

	public String getCustomizedName() {
		return customizedName;
	}

	public void setCustomizedName(String customizedName) {
		this.customizedName = customizedName;
	}

	public String getCustomizedNumber() {
		return customizedNumber;
	}

	public void setCustomizedNumber(String customizedNumber) {
		this.customizedNumber = customizedNumber;
	}

	public Double getLoyaltyPointAwarded() {
		return loyaltyPointAwarded;
	}

	public void setLoyaltyPointAwarded(Double loyaltyPointAwarded) {
		this.loyaltyPointAwarded = loyaltyPointAwarded;
	}

	public Double getLoyaltyPointUsed() {
		return loyaltyPointUsed;
	}

	public void setLoyaltyPointUsed(Double loyaltyPointUsed) {
		this.loyaltyPointUsed = loyaltyPointUsed;
	}

	public Double getLoyaltyAmountUsed() {
		return loyaltyAmountUsed;
	}

	public void setLoyaltyAmountUsed(Double loyaltyAmountUsed) {
		this.loyaltyAmountUsed = loyaltyAmountUsed;
	}

	@Override
	public String toString() {
		return "CustomerOrderItemEntry [styleId=" + styleId + ", optionId=" + optionId + ", skuId=" + skuId
				+ ", discountRuleId=" + discountRuleId + ", discountRuleRevId=" + discountRuleRevId + ", promotionId="
				+ promotionId + ", cancellationReasonId=" + cancellationReasonId + ", releaseId=" + releaseId
				+ ", exchangeOrderId=" + exchangeOrderId + ", exchangeLineId=" + exchangeLineId + ", status=" + status
				+ ", statusDisplayName=" + statusDisplayName + ", isDiscountedProduct=" + isDiscountedProduct
				+ ", isReturnableProduct=" + isReturnableProduct + ", cancellationReason=" + cancellationReason
				+ ", isCustomizable=" + isCustomizable + ", quantity=" + quantity + ", discountedQuantity="
				+ discountedQuantity + ", taxRate=" + taxRate + ", taxAmount=" + taxAmount + ", unitPrice=" + unitPrice
				+ ", discount=" + discount + ", couponDiscount=" + couponDiscount + ", cartDiscount=" + cartDiscount
				+ ", cashRedeemed=" + cashRedeemed + ", pgDiscount=" + pgDiscount + ", finalAmount=" + finalAmount
				+ ", cashbackOffered=" + cashbackOffered + ", cancelledOn=" + cancelledOn + ", name=" + name
				+ ", code=" + code + ", lastUser=" + lastUser + ", adminDisabled=" + adminDisabled + ", description="
				+ description + ", vendorArticleNo=" + vendorArticleNo + ", vendorArticleName=" + vendorArticleName
				+ ", size=" + size + ", brandId=" + brandId + ", brandName=" + brandName + ", brandCode=" + brandCode
				+ ", articleTypeId=" + articleTypeId + ", articleTypeName=" + articleTypeName + ", articleTypeCode="
				+ articleTypeCode + ", remarks=" + remarks + ", jitSourced=" + jitSourced + ", gtin=" + gtin
				+ ", enabled=" + enabled + ", version=" + version + ", styleName=" + styleName + ", searchImagezURL="
				+ searchImagezURL + ", supplyType=" + supplyType + ", itemBarcodes=" + itemBarcodes
				+ ", customizedMessage=" + customizedMessage + ", customizedName=" + customizedName
				+ ", customizedNumber=" + customizedNumber + ", loyaltyPointAwarded=" + loyaltyPointAwarded
				+ ", loyaltyPointUsed=" + loyaltyPointUsed + ", loyaltyAmountUsed=" + loyaltyAmountUsed + "]";
	}
	
}