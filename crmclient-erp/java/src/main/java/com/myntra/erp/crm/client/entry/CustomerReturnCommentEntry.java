package com.myntra.erp.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for customer return comments
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "customerReturnCommentEntry")
public class CustomerReturnCommentEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private String returnComment;
	private String returnCommentBy;
	private String returnCommentDescription;
	private Date returnCommentCreatedDate;
	private String imageURL;

	public CustomerReturnCommentEntry() {
	}

	public CustomerReturnCommentEntry(String returnComment, String returnCommentBy, String returnCommentDescription,
			Date returnCommentCreatedDate, String imageURL) {
		this.returnComment = returnComment;
		this.returnCommentBy = returnCommentBy;
		this.returnCommentDescription = returnCommentDescription;
		this.returnCommentCreatedDate = returnCommentCreatedDate;
		this.imageURL = imageURL;
	}

	public String getReturnComment() {
		return returnComment;
	}

	public void setReturnComment(String returnComment) {
		this.returnComment = returnComment;
	}

	public String getReturnCommentBy() {
		return returnCommentBy;
	}

	public void setReturnCommentBy(String returnCommentBy) {
		this.returnCommentBy = returnCommentBy;
	}

	public String getReturnCommentDescription() {
		return returnCommentDescription;
	}

	public void setReturnCommentDescription(String returnCommentDescription) {
		this.returnCommentDescription = returnCommentDescription;
	}

	public Date getReturnCommentCreatedDate() {
		return returnCommentCreatedDate;
	}

	public void setReturnCommentCreatedDate(Date returnCommentCreatedDate) {
		this.returnCommentCreatedDate = returnCommentCreatedDate;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	@Override
	public String toString() {
		return "CustomerReturnCommentEntry [returnComment=" + returnComment + ", returnCommentBy=" + returnCommentBy
				+ ", returnCommentDescription=" + returnCommentDescription + ", returnCommentCreatedDate="
				+ returnCommentCreatedDate + ", imageURL=" + imageURL + "]";
	}
}
