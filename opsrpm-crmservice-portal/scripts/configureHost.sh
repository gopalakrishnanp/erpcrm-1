#!/bin/sh
SRC_DIR=$1
TARGET_DIR=$2
echo "Copying from OPS_DIR Located at $SRC_DIR"
echo "Copying to   APP_DIR Located at $TARGET_DIR"

## Copy properties into tomcat conf folder
cp  $SRC_DIR/crm-dbconfig.properties $TARGET_DIR/conf 
cp  $SRC_DIR/rollback-dbconfig.properties $TARGET_DIR/conf 
cp  $SRC_DIR/serviceurls.properties $TARGET_DIR/conf 
cp  $SRC_DIR/statsd.properties $TARGET_DIR/conf 

## Copy runtime configurations into tomcat bin folder
#cp  $SRC_DIR/setenv.sh $TARGET_DIR/bin

chmod -R +x $TARGET_DIR/bin
mkdir -p $TARGET_DIR/logs
mkdir -p $TARGET_DIR/temp

echo "Configured"
