package com.myntra.drishti.crm.client.entry;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

@XmlRootElement(name = "drishtiOrderEntry")
public class DrishtiOrderEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;
	Long orderId;

	List<DrishtiShipmentEntry> drishtiShipments;

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	@XmlElementWrapper(name = "drishtiShipments")
	@XmlElement(name = "drishtiShipment")
	public List<DrishtiShipmentEntry> getDrishtiShipments() {
		return drishtiShipments;
	}

	public void setDrishtiShipments(List<DrishtiShipmentEntry> drishtiShipments) {
		this.drishtiShipments = drishtiShipments;
	}

	@Override
	public String toString() {
		return "DrishtiOrderEntry [orderId=" + orderId + ", drishtiShipments=" + drishtiShipments + "]";
	}	
}
