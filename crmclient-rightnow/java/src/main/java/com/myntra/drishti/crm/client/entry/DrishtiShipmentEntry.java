package com.myntra.drishti.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

@XmlRootElement(name = "drishtiShipmentEntry")
public class DrishtiShipmentEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	String drishtiOrderStatus;
	String OMSStatus;
	String OMSStatusDisplayName;
	Long shipmentId;
	String remark;
	Date date;

	public String getDrishtiOrderStatus() {
		return drishtiOrderStatus;
	}

	public void setDrishtiOrderStatus(String drishtiOrderStatus) {
		this.drishtiOrderStatus = drishtiOrderStatus;
	}

	public String getOMSStatus() {
		return OMSStatus;
	}

	public void setOMSStatus(String OMSStatus) {
		this.OMSStatus = OMSStatus;
	}

	public String getOMSStatusDisplayName() {
		return OMSStatusDisplayName;
	}

	public void setOMSStatusDisplayName(String oMSStatusDisplayName) {
		OMSStatusDisplayName = oMSStatusDisplayName;
	}

	public Long getShipmentId() {
		return shipmentId;
	}

	public void setShipmentId(Long shipmentId) {
		this.shipmentId = shipmentId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "DrishtiShipmentEntry [drishtiOrderStatus=" + drishtiOrderStatus + ", OMSStatus=" + OMSStatus
				+ ", OMSStatusDisplayName=" + OMSStatusDisplayName + ", shipmentId=" + shipmentId + ", remark="
				+ remark + ", date=" + date + "]";
	}

}