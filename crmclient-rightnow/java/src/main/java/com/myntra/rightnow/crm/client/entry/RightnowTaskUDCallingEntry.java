/**
 * 
 */
package com.myntra.rightnow.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author preetam
 *
 */
@XmlRootElement(name = "rightnowTaskUDCallingEntry")
public class RightnowTaskUDCallingEntry extends RightnowTaskOrderEntry {

	private static final long serialVersionUID = 1L;
	
	private String udReason;
	private Date udQueuedDate;
	
	
	public String getUdReason() {
		return udReason;
	}
	public void setUdReason(String udReason) {
		this.udReason = udReason;
	}
	public Date getUdQueuedDate() {
		return udQueuedDate;
	}
	public void setUdQueuedDate(Date udQueuedDate) {
		this.udQueuedDate = udQueuedDate;
	}
	
	@Override
	public String toString() {
		return "RightnowTaskUDCallingEntry [udReason=" + udReason
				+ ", udQueuedDate=" + udQueuedDate + "]";
	}
	
	
}
