package com.myntra.rightnow.crm.client.entry;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;
import com.myntra.commons.tranformer.TransformIgnoreAttribute;

@XmlRootElement(name = "rightnowBaseTaskListEntry")
public class RightnowBaseTaskListEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private List<RightnowBaseTaskEntry> taskEntryList;

	@XmlElementWrapper(name = "rightnowBaseTaskEntrys")
	@XmlElement(name = "rightnowBaseTaskEntry")
	@TransformIgnoreAttribute
	public List<RightnowBaseTaskEntry> getTaskEntryList() {
		return taskEntryList;
	}

	public void setTaskEntryList(List<RightnowBaseTaskEntry> taskEntryList) {
		this.taskEntryList = taskEntryList;
	}

	@Override
	public String toString() {
		return "RightnowBaseTaskListEntry [taskEntryList=" + taskEntryList + "]";
	}
}
