package com.myntra.rightnow.crm.client.entry;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * @author Pravin Mehta
 */
@XmlRootElement(name = "rightnowReportColumnEntry")
public class RightnowReportColumnEntry extends BaseEntry {
	
	private static final long serialVersionUID = 1;
	
	private String columnName;
	private String rnObjectName; // Rightnow object - task / incident / contact.. etc
	private String dataType;
	private boolean isEditable;
	private boolean isCustomField;
	
	public RightnowReportColumnEntry() {
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getRnObjectName() {
		return rnObjectName;
	}

	public void setRnObjectName(String rnObjectName) {
		this.rnObjectName = rnObjectName;
	}

	public boolean isEditable() {
		return isEditable;
	}

	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}

	public boolean isCustomField() {
		return isCustomField;
	}

	public void setCustomField(boolean isCustomField) {
		this.isCustomField = isCustomField;
	}

	@Override
	public String toString() {
		return "RightnowReportColumnEntry [columnName=" + columnName
				+ ", rnObjectName=" + rnObjectName + ", dataType=" + dataType
				+ ", isEditable=" + isEditable + ", isCustomField="
				+ isCustomField + "]";
	}
}
