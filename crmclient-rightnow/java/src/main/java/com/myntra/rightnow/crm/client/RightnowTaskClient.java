package com.myntra.rightnow.crm.client;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.ContentDisposition;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.apache.cxf.jaxrs.impl.MetadataMap;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.test.TestHelper;
import com.myntra.commons.utils.ResponseHandler;
import com.myntra.rightnow.crm.client.entry.RightnowBaseTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowFileAttachmentEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskIdEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskListEntry;
import com.myntra.rightnow.crm.client.response.RightnowBaseTaskResponse;
import com.myntra.rightnow.crm.client.response.RightnowTaskResponse;
import com.myntra.rightnow.crm.constants.RightnowConstants;

/**
 * Web client for all the contact web services
 */
public class RightnowTaskClient extends ResponseHandler {

	public static final String SERVICE_PREFIX = "/crm/task/";

	public static RightnowTaskResponse updateTasks(String serviceURL, RightnowTaskListEntry taskList)
			throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, RightnowConstants.CRM_RIGHTNOW_URL,
				TestHelper.dummyContextInfo1);

		client.path(SERVICE_PREFIX + "/updateTask/");
		RightnowTaskResponse response = new RightnowTaskResponse(taskList);

		return client.put(response, RightnowTaskResponse.class);
	}

	public static RightnowTaskResponse getTaskNotes(String serviceURL, Long taskId) throws ERPServiceException {
		BaseWebClient client = new BaseWebClient(serviceURL, RightnowConstants.CRM_RIGHTNOW_URL,
				TestHelper.dummyContextInfo1);

		client.path(SERVICE_PREFIX + "/note/{taskId}", taskId);
		return client.get(RightnowTaskResponse.class);
	}

	public static RightnowTaskResponse getListOfTaskNotes(String serviceURL, RightnowTaskListEntry taskList)
			throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, RightnowConstants.CRM_RIGHTNOW_URL,
				TestHelper.dummyContextInfo1);

		client.path(SERVICE_PREFIX + "/note/list");

		RightnowTaskResponse response = new RightnowTaskResponse(taskList);

		return client.put(response, RightnowTaskResponse.class);

	}

	public static RightnowTaskResponse getTaskDetails(String serviceURL, Long taskId) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, RightnowConstants.CRM_RIGHTNOW_URL,
				TestHelper.dummyContextInfo1);

		client.path(SERVICE_PREFIX + "/detail/{taskId}", taskId);

		return client.get(RightnowTaskResponse.class);
	}

	public static RightnowTaskResponse getTaskAttachments(String serviceURL, Long taskId) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, RightnowConstants.CRM_RIGHTNOW_URL,
				TestHelper.dummyContextInfo1);

		client.path(SERVICE_PREFIX + "/attachment/");
		client.query("taskId", taskId);

		return client.get(RightnowTaskResponse.class);
	}

	public static RightnowTaskResponse getOriginalAttachment(String serviceURL, RightnowFileAttachmentEntry fileEntry)
			throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, RightnowConstants.CRM_RIGHTNOW_URL,
				TestHelper.dummyContextInfo1);
		client.setReceiveTimeOut(5 * 60 * 1000);
		client.path(SERVICE_PREFIX + "/originalAttachment/");

		return client.put(fileEntry, RightnowTaskResponse.class);
	}

	public static RightnowTaskResponse getRightnowTaskNotesFromReport(String serviceURL, RightnowTaskIdEntry taskIdEntry)
			throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, RightnowConstants.CRM_RIGHTNOW_URL,
				TestHelper.dummyContextInfo1);

		client.path(SERVICE_PREFIX + "/noteFromReport/");

		return client.put(taskIdEntry, RightnowTaskResponse.class);
	}

	public static RightnowBaseTaskResponse createProactiveTasks(String serviceURL,
			List<RightnowBaseTaskEntry> baseTaskListEntry) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, RightnowConstants.CRM_RIGHTNOW_URL,
				TestHelper.dummyContextInfo1);

		client.path(SERVICE_PREFIX + "/createTask/");
		RightnowBaseTaskResponse response = new RightnowBaseTaskResponse(baseTaskListEntry);

		return client.post(response, RightnowBaseTaskResponse.class);
	}
	
	public static RightnowTaskResponse uploadAttachment(String serviceURL, InputStream is, String name, String contentType, Long taskId) throws ERPServiceException {
		
		BaseWebClient client = new BaseWebClient(serviceURL, RightnowConstants.CRM_RIGHTNOW_URL, TestHelper.dummyContextInfo1);
        client.path(SERVICE_PREFIX + "/upload/attachment");
        client.setContentType("multipart/form-data");
        client.setReceiveTimeOut(5 * 60 * 1000);//setting timeout to 5mins
        ContentDisposition cd = new ContentDisposition("attachment;filename="+name+";taskId="+taskId+";contentType="+contentType);
        MultivaluedMap<String, String> headers = new MetadataMap<String, String>();
        headers.putSingle("Content-ID", "image");
        headers.putSingle("Content-Disposition", cd.toString());
       
        Attachment attachment = new Attachment(is, headers);
        MultipartBody body = new MultipartBody(attachment);
        
        return client.post(body, RightnowTaskResponse.class);
	}
	
	/**
	 * Bulk close Tasks & Incidents
	 * @param serviceURL
	 * @param taskIdEntry
	 * @return
	 * @throws ERPServiceException
	 */
	public static RightnowTaskResponse closeTasks(String serviceURL,
			RightnowTaskIdEntry taskIdEntry) throws ERPServiceException {
		
		BaseWebClient client = new BaseWebClient(serviceURL,
				RightnowConstants.CRM_RIGHTNOW_URL,
				TestHelper.dummyContextInfo1);
		
		client.path(SERVICE_PREFIX + "/closeTasks/");
		
		return client.put(taskIdEntry, RightnowTaskResponse.class);
	}

	private static void testSaveTaskFunctionality(Long taskId, String status) throws ERPServiceException {
		RightnowTaskEntry taskEntryTest = new RightnowTaskEntry();
		taskEntryTest.setId(taskId);
		taskEntryTest.setNote("Testing notes from services.. Random " + (int) (Math.random() * 100));
		taskEntryTest.setSrStatus(status);
		RightnowTaskListEntry taskEntryList = new RightnowTaskListEntry();

		List<RightnowTaskEntry> taskList = new ArrayList<RightnowTaskEntry>();
		taskList.add(taskEntryTest);
		taskEntryList.setTaskEntryList(taskList);

		try {
			RightnowTaskResponse test = updateTasks(null, taskEntryList);
			System.out.println("Task saved successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void testGetTaskNotesFunctionality(Long taskId) throws ERPServiceException {
		RightnowTaskResponse test;
		try {
			test = getTaskNotes(null, taskId);
			System.out.println("Task Note Details:");
			System.out.println("------------------");
			System.out.println(test);
			System.out.println("Task Notes Fetched successfully !" + test);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void testGetListOfTaskNotes() throws ERPServiceException {
		RightnowTaskListEntry taskEntryList = new RightnowTaskListEntry();
		List<RightnowTaskEntry> taskList = new ArrayList<RightnowTaskEntry>();
		Long[] taskArray = new Long[] { 32L, 76L, 79L, 114L };
		for (Long taskId : taskArray) {
			RightnowTaskEntry taskEntryTest = new RightnowTaskEntry();
			taskEntryTest.setId(taskId);
			taskList.add(taskEntryTest);
		}
		taskEntryList.setTaskEntryList(taskList);

		RightnowTaskResponse test;
		try {
			test = getListOfTaskNotes(null, taskEntryList);
			System.out.println(test);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String args[]) throws ERPServiceException {
		try {
			testSaveTaskFunctionality(16L, "Resolved");
			RightnowTaskResponse check = getTaskNotes(null, 32L);
			System.out.println(check.getRightnowTaskEntryList());
			testGetTaskNotesFunctionality(32L);
			testGetListOfTaskNotes();
		} catch (Exception e) {
			System.out.println("Major " + e.getMessage());
			e.printStackTrace();
		}
	}

}