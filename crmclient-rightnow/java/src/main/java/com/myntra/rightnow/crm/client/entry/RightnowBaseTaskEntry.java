/**
 * 
 */
package com.myntra.rightnow.crm.client.entry;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * @author preetam
 *
 */
@XmlRootElement(name = "rightnowBaseTaskEntry")
public class RightnowBaseTaskEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;
	
	private String mainCategory;
	private String subCategory;
	private String subSubCategory;
	private String comments;
	
	
	public String getMainCategory() {
		return mainCategory;
	}
	public void setMainCategory(String mainCategory) {
		this.mainCategory = mainCategory;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	public String getSubSubCategory() {
		return subSubCategory;
	}
	public void setSubSubCategory(String subSubCategory) {
		this.subSubCategory = subSubCategory;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
	@Override
	public String toString() {
		return "RightnowBaseTaskEntry [mainCategory=" + mainCategory
				+ ", subCategory=" + subCategory + ", subSubCategory="
				+ subSubCategory + ", comments=" + comments + "]";
	}
	
	
}
