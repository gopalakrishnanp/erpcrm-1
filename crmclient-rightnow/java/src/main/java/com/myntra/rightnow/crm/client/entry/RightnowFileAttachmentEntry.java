package com.myntra.rightnow.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

@XmlRootElement(name = "rightnowFileAttachmentEntry")
public class RightnowFileAttachmentEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private String fileName;
	private String contentType;
	private int fileSize;
	private Date updatedTime;
	private String filePath;
	private long taskId;
	
	public RightnowFileAttachmentEntry() {
	}
	
	public RightnowFileAttachmentEntry(long id, String fileName, String contentType,
			int fileSize, Date updatedTime, long taskId) {
		super();
		super.setId(id);
		this.fileName = fileName;
		this.contentType = contentType;
		this.fileSize = fileSize;
		this.updatedTime = updatedTime;		
		this.taskId = taskId;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public int getFileSize() {
		return fileSize;
	}
	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}
	public Date getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public long getTaskId() {
		return taskId;
	}
	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "RightnowFileAttachmentEntry [fileName=" + fileName
				+ ", contentType=" + contentType + ", fileSize=" + fileSize
				+ ", updatedTime=" + updatedTime + ", filePath=" + filePath
				+ ", taskId=" + taskId + "]";
	}
	
	
}
