package com.myntra.rightnow.crm.client.entry;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "rightnowTaskWelcomeCallingEntry")
public class RightnowTaskWelcomeCallingEntry extends RightnowTaskOrderEntry {
	
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "RightnowTaskWelcomeCallingEntry []";
	}		

}
