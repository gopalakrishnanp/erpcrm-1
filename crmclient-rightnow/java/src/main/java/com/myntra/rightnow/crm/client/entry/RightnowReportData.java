/**
 * 
 */
package com.myntra.rightnow.crm.client.entry;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author Preetam
 *
 */
public class RightnowReportData {

	private String[] columnData;
	private String[] rowData;
	private HashMap<String, Integer> columnIndexMap;
	private HashMap<String, Integer> reportPropertiesMap;
	
	public RightnowReportData() {
	}
	
	public RightnowReportData(String[] columnData, String[] rowData) {
		this.columnData = columnData;
		this.rowData = rowData;
		populateColumnIndexMap();
	}
	
	public String[] getColumnData() {
		return columnData;
	}
	public void setColumnData(String[] columnData) {
		this.columnData = columnData;
	}
	public String[] getRowData() {
		return rowData;
	}
	public void setRowData(String[] rowData) {
		this.rowData = rowData;
	}
	public HashMap<String, Integer> getColumnIndexMap() {
		return columnIndexMap;
	}
	public void setColumnIndexMap(HashMap<String, Integer> columnIndexMap) {
		this.columnIndexMap = columnIndexMap;
	}
	public HashMap<String, Integer> getReportPropertiesMap() {
		return reportPropertiesMap;
	}
	public void setReportPropertiesMap(HashMap<String, Integer> reportPropertiesMap) {
		this.reportPropertiesMap = reportPropertiesMap;
	}
	
	
	/*public int getIndexOfColumn(String columnName) {
		int colIndex = -1;
		//initialize the map, if not yet initialized
		if(columnIndexMap == null)
			columnIndexMap = new HashMap<String, Integer>();
		
		//find the index of the column from the map (by checking for lower case column name)
		Integer index = columnIndexMap.get(columnName.toLowerCase());
		//if not found, find the column index from array
		if(index == null) {
			List<String> columnList = Arrays.asList(columnData);
			colIndex = columnList.indexOf(columnName);
			if(colIndex > -1) {
				columnIndexMap.put(columnName.toLowerCase(), colIndex);
			}
			return colIndex;
		}
			//return -1;
		return index.intValue();		
	}*/
	
	
	public int getIndexOfColumn(String columnName) {
		if(columnIndexMap == null)
			return -1;
		
		Integer index = columnIndexMap.get(columnName.toLowerCase());
		if(index == null)
			return -1;
		return index.intValue();
	}
	
	public void populateColumnIndexMap() {
		columnIndexMap = new HashMap<String, Integer>();
		if(columnData == null || columnData.length == 0)
			return;
		
		for(int i = 0; i < columnData.length; i++) {
			columnIndexMap.put(columnData[i].toLowerCase(), i);
		}
	}		
}
