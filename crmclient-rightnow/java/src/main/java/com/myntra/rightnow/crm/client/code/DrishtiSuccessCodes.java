package com.myntra.rightnow.crm.client.code;

import com.myntra.commons.codes.ERPSuccessCodes;
import com.myntra.commons.codes.StatusCodes;

/**
 * List of success codes
 */
public class DrishtiSuccessCodes extends ERPSuccessCodes {

	public static final StatusCodes DRISHTI_ORDERS_RETRIEVED = new DrishtiSuccessCodes(
			807, "DRISHTI_ORDERS_RETRIEVED");

	private static final String BUNDLE_NAME = "DrishtiSuccessCodes";

	public DrishtiSuccessCodes(int successCode, String successMessage) {
		setAll(successCode, successMessage, BUNDLE_NAME);
	}

}
