

/**
 * RightNowSyncService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package com.rightnow.ws.wsdl;

    /*
     *  RightNowSyncService java interface
     */

    public interface RightNowSyncService {
          

        /**
          * Auto generated method signature
          * 
                    * @param getValuesForNamedIDHierarchy44
                
                    * @param clientInfoHeader46
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.base.NamedIDWithParent[] getValuesForNamedIDHierarchy(

                        java.lang.String fieldName45,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader46)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getValuesForNamedIDHierarchy44
            
                * @param clientInfoHeader46
            
          */
        public void startgetValuesForNamedIDHierarchy(

            java.lang.String fieldName45,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader46,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param destroy49
                
                    * @param clientInfoHeader52
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.messages.DestroyResponseMsg destroy(

                        com.rightnow.ws.base.RNObject[] rNObjects50,com.rightnow.ws.messages.DestroyProcessingOptions processingOptions51,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader52)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param destroy49
            
                * @param clientInfoHeader52
            
          */
        public void startdestroy(

            com.rightnow.ws.base.RNObject[] rNObjects50,com.rightnow.ws.messages.DestroyProcessingOptions processingOptions51,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader52,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getMetaData54
                
                    * @param clientInfoHeader55
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.metadata.MetaDataClass[] getMetaData(

                        com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader55)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getMetaData54
            
                * @param clientInfoHeader55
            
          */
        public void startgetMetaData(

            com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader55,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param queryObjects58
                
                    * @param clientInfoHeader62
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.messages.QueryResultData[] queryObjects(

                        java.lang.String query59,com.rightnow.ws.base.RNObject[] objectTemplates60,int pageSize61,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader62)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param queryObjects58
            
                * @param clientInfoHeader62
            
          */
        public void startqueryObjects(

            java.lang.String query59,com.rightnow.ws.base.RNObject[] objectTemplates60,int pageSize61,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader62,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getMetaDataForClass65
                
                    * @param clientInfoHeader69
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.metadata.MetaDataClass[] getMetaDataForClass(

                        java.lang.String[] className66,com.rightnow.ws.generic.RNObjectType[] qualifiedClassName67,java.lang.String[] metaDataLink68,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader69)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getMetaDataForClass65
            
                * @param clientInfoHeader69
            
          */
        public void startgetMetaDataForClass(

            java.lang.String[] className66,com.rightnow.ws.generic.RNObjectType[] qualifiedClassName67,java.lang.String[] metaDataLink68,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader69,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param update72
                
                    * @param clientInfoHeader75
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.messages.UpdateResponseMsg update(

                        com.rightnow.ws.base.RNObject[] rNObjects73,com.rightnow.ws.messages.UpdateProcessingOptions processingOptions74,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader75)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param update72
            
                * @param clientInfoHeader75
            
          */
        public void startupdate(

            com.rightnow.ws.base.RNObject[] rNObjects73,com.rightnow.ws.messages.UpdateProcessingOptions processingOptions74,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader75,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param queryCSV77
                
                    * @param clientInfoHeader83
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.messages.QueryCSVResponseMsg queryCSV(

                        java.lang.String query78,int pageSize79,java.lang.String delimiter80,boolean returnRawResult81,boolean disableMTOM82,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader83)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param queryCSV77
            
                * @param clientInfoHeader83
            
          */
        public void startqueryCSV(

            java.lang.String query78,int pageSize79,java.lang.String delimiter80,boolean returnRawResult81,boolean disableMTOM82,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader83,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param convertGenericToObject87
                
                    * @param clientInfoHeader89
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.messages.RNObjectsResult convertGenericToObject(

                        com.rightnow.ws.base.RNObject[] rNObjects88,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader89)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param convertGenericToObject87
            
                * @param clientInfoHeader89
            
          */
        public void startconvertGenericToObject(

            com.rightnow.ws.base.RNObject[] rNObjects88,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader89,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param sendMailingToContact92
                
                    * @param clientInfoHeader99
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.messages.SendMailingToContactResponseMsg sendMailingToContact(

                        com.rightnow.ws.base.ID contactID93,com.rightnow.ws.base.ID mailingID94,java.util.Calendar scheduledTime95,com.rightnow.ws.base.ID incidentID96,com.rightnow.ws.base.ID opportunityID97,com.rightnow.ws.base.ID chatID98,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader99)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param sendMailingToContact92
            
                * @param clientInfoHeader99
            
          */
        public void startsendMailingToContact(

            com.rightnow.ws.base.ID contactID93,com.rightnow.ws.base.ID mailingID94,java.util.Calendar scheduledTime95,com.rightnow.ws.base.ID incidentID96,com.rightnow.ws.base.ID opportunityID97,com.rightnow.ws.base.ID chatID98,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader99,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param batch101
                
                    * @param clientInfoHeader103
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.messages.BatchResponseItem[] batch(

                        com.rightnow.ws.messages.BatchRequestItem[] batchRequestItem102,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader103)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param batch101
            
                * @param clientInfoHeader103
            
          */
        public void startbatch(

            com.rightnow.ws.messages.BatchRequestItem[] batchRequestItem102,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader103,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getFileData106
                
                    * @param clientInfoHeader110
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public javax.activation.DataHandler getFileData(

                        com.rightnow.ws.base.RNObject rNObject107,com.rightnow.ws.base.ID fileID108,boolean disableMTOM109,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader110)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getFileData106
            
                * @param clientInfoHeader110
            
          */
        public void startgetFileData(

            com.rightnow.ws.base.RNObject rNObject107,com.rightnow.ws.base.ID fileID108,boolean disableMTOM109,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader110,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param executeMarketingFlow113
                
                    * @param clientInfoHeader117
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.messages.ExecuteMarketingFlowResponseMsg executeMarketingFlow(

                        com.rightnow.ws.base.ID contactID114,com.rightnow.ws.base.ID campaignID115,java.lang.String entryPoint116,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader117)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param executeMarketingFlow113
            
                * @param clientInfoHeader117
            
          */
        public void startexecuteMarketingFlow(

            com.rightnow.ws.base.ID contactID114,com.rightnow.ws.base.ID campaignID115,java.lang.String entryPoint116,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader117,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getMetaDataLastChangeTime119
                
                    * @param clientInfoHeader120
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public java.util.Calendar getMetaDataLastChangeTime(

                        com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader120)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getMetaDataLastChangeTime119
            
                * @param clientInfoHeader120
            
          */
        public void startgetMetaDataLastChangeTime(

            com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader120,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getValuesForNamedID123
                
                    * @param clientInfoHeader126
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.base.NamedID[] getValuesForNamedID(

                        java.lang.String packageName124,java.lang.String fieldName125,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader126)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getValuesForNamedID123
            
                * @param clientInfoHeader126
            
          */
        public void startgetValuesForNamedID(

            java.lang.String packageName124,java.lang.String fieldName125,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader126,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param convertObjectToGeneric129
                
                    * @param clientInfoHeader131
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.messages.RNObjectsResult convertObjectToGeneric(

                        com.rightnow.ws.base.RNObject[] rNObjects130,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader131)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param convertObjectToGeneric129
            
                * @param clientInfoHeader131
            
          */
        public void startconvertObjectToGeneric(

            com.rightnow.ws.base.RNObject[] rNObjects130,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader131,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param runAnalyticsReport134
                
                    * @param clientInfoHeader141
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.messages.RunAnalyticsReportResponseMsg runAnalyticsReport(

                        com.rightnow.ws.objects.AnalyticsReport analyticsReport135,int limit136,int start137,java.lang.String delimiter138,boolean returnRawResult139,boolean disableMTOM140,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader141)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param runAnalyticsReport134
            
                * @param clientInfoHeader141
            
          */
        public void startrunAnalyticsReport(

            com.rightnow.ws.objects.AnalyticsReport analyticsReport135,int limit136,int start137,java.lang.String delimiter138,boolean returnRawResult139,boolean disableMTOM140,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader141,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param create145
                
                    * @param clientInfoHeader148
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.messages.RNObjectsResult create(

                        com.rightnow.ws.base.RNObject[] rNObjects146,com.rightnow.ws.messages.CreateProcessingOptions processingOptions147,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader148)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param create145
            
                * @param clientInfoHeader148
            
          */
        public void startcreate(

            com.rightnow.ws.base.RNObject[] rNObjects146,com.rightnow.ws.messages.CreateProcessingOptions processingOptions147,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader148,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param get151
                
                    * @param clientInfoHeader154
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.messages.RNObjectsResult get(

                        com.rightnow.ws.base.RNObject[] rNObjects152,com.rightnow.ws.messages.GetProcessingOptions processingOptions153,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader154)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param get151
            
                * @param clientInfoHeader154
            
          */
        public void startget(

            com.rightnow.ws.base.RNObject[] rNObjects152,com.rightnow.ws.messages.GetProcessingOptions processingOptions153,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader154,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param transferSubObjects157
                
                    * @param clientInfoHeader160
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.messages.TransferSubObjectsResponseMsg transferSubObjects(

                        com.rightnow.ws.base.RNObject destinationRNObject158,com.rightnow.ws.base.RNObject[] sourceRNObjects159,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader160)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param transferSubObjects157
            
                * @param clientInfoHeader160
            
          */
        public void starttransferSubObjects(

            com.rightnow.ws.base.RNObject destinationRNObject158,com.rightnow.ws.base.RNObject[] sourceRNObjects159,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader160,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param resetContactPassword162
                
                    * @param clientInfoHeader164
                
             * @throws com.rightnow.ws.wsdl.ServerErrorFault : 
             * @throws com.rightnow.ws.wsdl.RequestErrorFault : 
             * @throws com.rightnow.ws.wsdl.UnexpectedErrorFault : 
         */

         
                     public com.rightnow.ws.messages.ResetContactPasswordResponseMsg resetContactPassword(

                        com.rightnow.ws.base.ID contactID163,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader164)
                        throws java.rmi.RemoteException
             
          ,com.rightnow.ws.wsdl.ServerErrorFault
          ,com.rightnow.ws.wsdl.RequestErrorFault
          ,com.rightnow.ws.wsdl.UnexpectedErrorFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param resetContactPassword162
            
                * @param clientInfoHeader164
            
          */
        public void startresetContactPassword(

            com.rightnow.ws.base.ID contactID163,com.rightnow.ws.messages.ClientInfoHeader clientInfoHeader164,
                

            final com.rightnow.ws.wsdl.RightNowSyncServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        
       //
       }
    