package com.myntra.rightnow.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.rightnow.crm.client.entry.RightnowReportEntry;
import com.myntra.rightnow.crm.client.response.RightnowReportResponse;
import com.myntra.rightnow.crm.manager.RightnowReportManager;
import com.myntra.rightnow.crm.service.RightnowReportService;

public class RightnowReportServiceImpl extends BaseServiceImpl<RightnowReportResponse, RightnowReportEntry> implements
		RightnowReportService {

	@Override
	public AbstractResponse getReportData(String reportName, String startDate, String endDate)
			throws ERPServiceException {
		try {
			if (startDate != null || endDate != null) {
				return ((RightnowReportManager) getManager()).getReportDataOnDaterange(reportName, startDate, endDate);
			} else {
				return ((RightnowReportManager) getManager()).getReportData(reportName, null);
			}

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}

	@Override
	public AbstractResponse getReportData(String reportName, RightnowReportResponse reportResponse)
			throws ERPServiceException {
		try {

			return ((RightnowReportManager) getManager()).getReportData(reportName,
					reportResponse.getRightnowReportFilterEntryList());

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}
}
