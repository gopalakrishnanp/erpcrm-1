package com.myntra.rightnow.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;
import com.myntra.erp.crm.client.response.CustomerReturnResponse;
import com.myntra.rightnow.crm.manager.RightnowReturnManager;
import com.myntra.rightnow.crm.service.RightnowReturnService;

/**
 * Customer order search web service implementation which integrates detail of
 * order, shipments, skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
public class RightnowReturnServiceImpl extends BaseServiceImpl<CustomerReturnResponse, CustomerReturnEntry> implements
		RightnowReturnService {

	@Override
	public AbstractResponse getReturnDetail(Long returnId, Long orderId, String login, boolean isLogisticDetailNeeded)
			throws ERPServiceException {
		try {
			return ((RightnowReturnManager) getManager()).getReturnDetail(returnId, orderId, login,
					isLogisticDetailNeeded);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}
}
