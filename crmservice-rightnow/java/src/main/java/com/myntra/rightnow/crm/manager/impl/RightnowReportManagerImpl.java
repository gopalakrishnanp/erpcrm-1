/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.rightnow.crm.manager.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.axis2.AxisFault;
import org.apache.commons.lang.ArrayUtils;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;
import com.myntra.rightnow.crm.base.BasicRunAnalyticsReport;
import com.myntra.rightnow.crm.client.code.RightnowErrorCodes;
import com.myntra.rightnow.crm.client.code.RightnowSuccessCodes;
import com.myntra.rightnow.crm.client.entry.RightnowReportColumnEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportFilterEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportRowEntry;
import com.myntra.rightnow.crm.client.response.RightnowReportResponse;
import com.myntra.rightnow.crm.constants.RightnowConstants;
import com.myntra.rightnow.crm.manager.RightnowReportManager;
import com.myntra.rightnow.crm.utils.GetReturnDetails;
import com.myntra.rightnow.crm.utils.TaskyUtils;
import com.rightnow.ws.messages.CSVRow;
import com.rightnow.ws.messages.CSVTable;

/**
 * @author Pravin Mehta
 */
public class RightnowReportManagerImpl extends BaseManagerImpl<RightnowReportResponse, RightnowReportEntry> implements RightnowReportManager {
	
	public static final int FETCH_LIMIT_FOR_REPORT_DATA = 2000;
	
	//private static final Logger LOGGER = Logger.getLogger(RightnowReportManagerImpl.class);

	@Override
	public RightnowReportResponse getReportDataOnDaterange(String reportName, String startDate, String endDate)
			throws ERPServiceException {
		
		List<RightnowReportFilterEntry> filters = new ArrayList<RightnowReportFilterEntry>();
		RightnowReportFilterEntry filterEntry = new RightnowReportFilterEntry();
		filterEntry.setProperty("Date Range");
		filterEntry.setDataType(4);
		filterEntry.setOperator(9);
		filterEntry.setValues(new String[] {startDate, endDate});
		
		filters.add(filterEntry);

		return getReportData(reportName, filters);		
	}
	
	@Override
	public RightnowReportResponse getReportData(String reportName, List<RightnowReportFilterEntry> filters) throws ERPServiceException {
		RightnowReportResponse response = new RightnowReportResponse();
		String[] rowData = {};
		BasicRunAnalyticsReport report;
		try {
			report = new BasicRunAnalyticsReport();
		} catch (AxisFault e) {
			throw new ERPServiceException(e.getMessage(), 1);
		}
		
		CSVTable table;
		int start = 0;
		while(true) {
			try {				
				table = report.runAnalyticsReport(reportName, filters, start, FETCH_LIMIT_FOR_REPORT_DATA);			
				
			} catch (ERPServiceException e) {
				
				StatusResponse error = new StatusResponse(RightnowErrorCodes.NO_REPORT_FETCHED, 
						StatusResponse.Type.ERROR, 0);
	
				if(e.getCode()==RightnowErrorCodes.NO_REPORTNAME_IN_RIGHTNOW.getStatusCode()) {
					error = new StatusResponse(RightnowErrorCodes.NO_REPORTNAME_IN_RIGHTNOW, 
							StatusResponse.Type.ERROR, 0);
				}
				
				response.setStatus(error);
				return response;
			}
		
			CSVRow rows = table.getRows();
			String[] data = rows.getRow();
			rowData = (String[]) ArrayUtils.addAll(rowData, data);
			if(data == null) {
				//System.out.println("No more data found");
				break;
			}
			start += FETCH_LIMIT_FOR_REPORT_DATA;
		}
		
		String columnsCSV = table.getColumns();
		
		String []columnNames = columnsCSV.split(RightnowConstants.RIGHTNOW_REPORT_DELIMITER_SPLIT_ESCAPE);
		
		if(columnNames==null || !columnNames[0].equalsIgnoreCase("Task ID")) {
			// return response with status
			StatusResponse error = new StatusResponse(RightnowErrorCodes.NO_TASKID_IN_RIGHTNOW_REPORT,
					StatusResponse.Type.ERROR, 0);
			response.setStatus(error);
			return response;
		}
		
		List<RightnowReportColumnEntry> rightnowReportColumnEntryList = new ArrayList<RightnowReportColumnEntry>();
		List<Integer> ageingColumnIndices = new ArrayList<Integer>();
		boolean ageingColumnExists = false;
		for(int i=0; i<columnNames.length; i++) {
			RightnowReportColumnEntry columnEntry = new RightnowReportColumnEntry();
			columnEntry.setColumnName(columnNames[i]);
			ageingColumnExists = false;
			if(columnEntry.getColumnName().equalsIgnoreCase("ageing") || columnEntry.getColumnName().toLowerCase().contains("ageing")) {
				ageingColumnIndices.add(i);
				ageingColumnExists = true;
			}
			columnEntry.setDataType("string");
			columnEntry.setEditable(false);
			
			if(columnNames[i].equalsIgnoreCase(RightnowConstants.TASK_STATUS_COLUMN))
				columnEntry.setEditable(false);
			
			// TODO.. Integrate this portion with DAO where we have configured report datatypes.
			
			rightnowReportColumnEntryList.add(columnEntry);
			
			if(ageingColumnExists) {
				RightnowReportColumnEntry ceAgeing = new RightnowReportColumnEntry();
				ceAgeing.setColumnName("Ageing in Days");
				ceAgeing.setDataType("string");
				ceAgeing.setEditable(false);
				rightnowReportColumnEntryList.add(ceAgeing);
			}
		}
		
		response.setRightnowReportColumnEntryList(rightnowReportColumnEntryList);
		Integer[] ageingColumnArray = ageingColumnIndices.toArray(new Integer[ageingColumnIndices.size()]);
		
		List<RightnowReportRowEntry> rightnowReportRowEntryList = new ArrayList<RightnowReportRowEntry>();
		//String[] rowData = rows.getRow();
		
		if(rowData!=null) {
			for(String row : rowData)
			{
				RightnowReportRowEntry rowEntry = new RightnowReportRowEntry();
	
				String[] data = row.split(RightnowConstants.RIGHTNOW_REPORT_DELIMITER_SPLIT_ESCAPE, -1);
				
				List<String> reportData = new ArrayList<String>();
				for(int i=0;i<data.length;i++) {
					//if(i == columnAgeingIndex) {
					if(Arrays.binarySearch(ageingColumnArray, 0, ageingColumnArray.length, i) > -1) {
						reportData.add(TaskyUtils.getTimeInDayHourMinuteFormat(data[i], false));
						reportData.add(TaskyUtils.getTimeInDayHourMinuteFormat(data[i], true));
					} else {
						reportData.add(data[i]);
					}
				}
				
				// First element is the task ID.
				rowEntry.setId(Long.valueOf(data[0]));
				
				rowEntry.setRightnowReportDataList(reportData);
				rightnowReportRowEntryList.add(rowEntry);
			}
		}
		
		response.setRightnowReportRowEntryList(rightnowReportRowEntryList);
		
		fixReportResponseReturnCases(reportName, response);
		
		// return response with status
		StatusResponse success = new StatusResponse(RightnowSuccessCodes.RIGHTNOW_REPORT_RETRIEVED,
				StatusResponse.Type.SUCCESS, rightnowReportRowEntryList.size());
		response.setStatus(success);

		return response;
	}
	
	private void fixReportResponseReturnCases(String reportName, RightnowReportResponse response) {
		if(reportName.equalsIgnoreCase("Return") || reportName.equalsIgnoreCase("Pick Up")) {
			// Continue;
		} else {
			return;
		}

		// Handle 'Return' Report
		
		List<RightnowReportColumnEntry> columnEntry = response.getRightnowReportColumnEntryList();

		int returnIDcolumn = -1;
		int returnIDStatusColumn = -1;
		int warehouseNameColumn = -1;
		int courierPartnerColumn = -1;
		int trackingNumberColumn = -1;
		int dcColumn = -1;
		
		for(int i=0; i<columnEntry.size(); i++) {
			String columnName = columnEntry.get(i).getColumnName();
			
			if(columnName.equalsIgnoreCase("Return ID")) {
				returnIDcolumn = i;
			} else if(columnName.equalsIgnoreCase("Return ID Status")) {
				returnIDStatusColumn = i;
			} else if(columnName.equalsIgnoreCase("Warehouse Name")) {
				warehouseNameColumn = i;
			} else if(columnName.equalsIgnoreCase("Name of Courier Partner")) {
				courierPartnerColumn = i;
			} else if(columnName.equalsIgnoreCase("Tracking Number")) {
				trackingNumberColumn = i;
			} else if(columnName.equalsIgnoreCase("DC Name")) {
				dcColumn = i;
			}
		}
		
		HashMap<Long, CustomerReturnEntry> returnResponseMap = new HashMap<Long, CustomerReturnEntry>();
		List<RightnowReportRowEntry> rowEntryList = response.getRightnowReportRowEntryList();
		
		List<FutureTask<Map<Long, CustomerReturnEntry>>> futureTaskList = 
				new ArrayList<FutureTask<Map<Long, CustomerReturnEntry>>>();
		
		for(RightnowReportRowEntry row: rowEntryList) {
			String returnIDstr = row.getRightnowReportDataList().get(returnIDcolumn);
			if(returnIDstr==null || returnIDstr.trim().isEmpty())
				continue;
			
			try {
				Long returnID = Long.valueOf(returnIDstr);
				GetReturnDetails returnDetails = new GetReturnDetails(returnID);
				FutureTask<Map<Long, CustomerReturnEntry>> futureTask = new FutureTask<Map<Long, CustomerReturnEntry>>(
						returnDetails);
				futureTaskList.add(futureTask);
			} catch(NumberFormatException n) {
				// Do nothing.. No future task created for bad return id.
			}
		}
		
		ExecutorService executor = Executors.newFixedThreadPool(futureTaskList.size()/5 + 1);
		for (int i = 0; i < futureTaskList.size(); i++) {
			executor.execute(futureTaskList.get(i));
		}
			
		try {
			for (FutureTask<Map<Long, CustomerReturnEntry>> task : futureTaskList) {
				try {
					returnResponseMap.putAll(task.get(120,TimeUnit.SECONDS));
				} catch(TimeoutException t) {
					task.cancel(true);
					t.printStackTrace();
				}
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch(ExecutionException e) {
			e.printStackTrace();
		}
		
		executor.shutdown();
		
		for(RightnowReportRowEntry row: rowEntryList) {
			String returnIDstr = row.getRightnowReportDataList().get(returnIDcolumn);
			
			if(returnIDstr==null || returnIDstr.trim().isEmpty())
				continue;
		
			Long returnID = 0L;
			try {
				returnID = Long.valueOf(returnIDstr);
			} catch (NumberFormatException ne) {
				
			}
			if(returnID>0 && returnResponseMap.containsKey(returnID)) {
				List<String> dataList = row.getRightnowReportDataList();

				CustomerReturnEntry returnEntry = returnResponseMap.get(returnID);
//				int returnIDStatusColumn = -1;
//				int warehouseNameColumn = -1;
//				int courierPartnerColumn = -1;
//				int trackingNumberColumn = -1;
//				int dcColumn = -1;
				
				if(returnIDStatusColumn>=0) {
					String returnStatus = returnEntry.getReturnStatus();
					if(returnStatus==null)
						returnStatus = "";
					dataList.set(returnIDStatusColumn, returnStatus);	
				}
				
				if(warehouseNameColumn>=0) {
					String warehouseName = returnEntry.getWareHouseName();
					if(warehouseName==null)
						warehouseName="";
					dataList.set(warehouseNameColumn, warehouseName);
				}
				
				if(trackingNumberColumn>=0) {
					String trackingNumber = returnEntry.getTrackingNumber();
					if(trackingNumber==null)
						trackingNumber="";
					dataList.set(trackingNumberColumn, trackingNumber);
				}
				
				if(courierPartnerColumn>=0) {
					String courierPartner = returnEntry.getCourierService();
					if(courierPartner==null)
						courierPartner="";
					dataList.set(courierPartnerColumn, courierPartner);
				}
				
				if(dcColumn>=0) {
					String dcName = ""; // Setting blank for non-ML
					
					if(returnEntry.getCourierService()!=null 
							&& returnEntry.getCourierService().equalsIgnoreCase("ML") && returnEntry.getDCCode()!=null) {
						dcName = returnEntry.getDCCode();
					}
					dataList.set(dcColumn, dcName);
				}

				row.setRightnowReportDataList(dataList);
			}
		}
		
		response.setRightnowReportRowEntryList(rowEntryList);
	}


	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse create(RightnowReportEntry entry) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(RightnowReportEntry entry, Long itemId) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}
			
	
	public static void main(String args[]) throws ERPServiceException {
		Date date = new Date();
		System.out.println("Started on " + date);
		RightnowReportManagerImpl impl = new RightnowReportManagerImpl();
		/*List<RightnowReportFilterEntry> filters = new ArrayList<RightnowReportFilterEntry>();
		RightnowReportFilterEntry filter1 = new RightnowReportFilterEntry();
        filter1.setProperty("Courier Name");
        filter1.setValues(new String[]{"Blue Dart"});
        filters.add(filter1);
		RightnowReportFilterEntry filter2 = new RightnowReportFilterEntry();
		filter2.setProperty("Warehouse");
        filter2.setValues(new String[]{"Delhi-Gurgaon"});
        filters.add(filter2);*/
		RightnowReportResponse response = impl.getReportData("BI_Report1_DONOTEXECUTE", null);
		//RightnowReportResponse response = impl.getReportData("Feedback", new RightnowReportFilterEntry[]{filterEntry});
		
		date = new Date();
		System.out.println("Finished on " + date);
		
		System.out.println(response.getRightnowReportRowEntryList().size());				
		
	}

	
}
