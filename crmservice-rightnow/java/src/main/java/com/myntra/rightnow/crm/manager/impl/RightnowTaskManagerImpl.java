package com.myntra.rightnow.crm.manager.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.ContentDisposition;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.apache.log4j.Logger;

import com.myntra.client.tools.CoreServiceClient;
import com.myntra.client.tools.response.ApplicationPropertiesEntry;
import com.myntra.client.tools.response.ApplicationPropertiesResponse;
import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.codes.StatusCodes;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.utils.Context;
import com.myntra.commons.utils.Misc;
import com.myntra.commons.utils.ServiceURLProperty;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.lms.client.status.OrderStatus;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.oms.client.response.OrderReleaseResponse;
import com.myntra.rightnow.crm.client.code.RightnowErrorCodes;
import com.myntra.rightnow.crm.client.code.RightnowSuccessCodes;
import com.myntra.rightnow.crm.client.entry.RightnowBaseTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowFileAttachmentEntry;
import com.myntra.rightnow.crm.client.entry.RightnowNoteEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportData;
import com.myntra.rightnow.crm.client.entry.RightnowReportFilterEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportRowEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskIdEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskListEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskWelcomeCallingEntry;
import com.myntra.rightnow.crm.client.response.RightnowBaseTaskResponse;
import com.myntra.rightnow.crm.client.response.RightnowReportResponse;
import com.myntra.rightnow.crm.client.response.RightnowTaskResponse;
import com.myntra.rightnow.crm.constants.RightnowConstants;
import com.myntra.rightnow.crm.constants.RightnowReportColumnConstants;
import com.myntra.rightnow.crm.dao.WelcomeCallTaskLogDAO;
import com.myntra.rightnow.crm.entity.WelcomeCallTaskLogEntity;
import com.myntra.rightnow.crm.manager.RightnowProactiveTaskManager;
import com.myntra.rightnow.crm.manager.RightnowReportManager;
import com.myntra.rightnow.crm.manager.RightnowTaskManager;
import com.myntra.rightnow.crm.utils.RightnowConnectUtility;
import com.myntra.rightnow.crm.utils.TaskyUtils;
import com.rightnow.ws.base.ActionEnum;
import com.rightnow.ws.base.ID;
import com.rightnow.ws.base.NamedID;
import com.rightnow.ws.base.RNObject;
import com.rightnow.ws.generic.DataTypeEnum;
import com.rightnow.ws.generic.DataValue;
import com.rightnow.ws.generic.GenericField;
import com.rightnow.ws.generic.GenericObject;
import com.rightnow.ws.generic.RNObjectType;
import com.rightnow.ws.messages.BatchRequestItem;
import com.rightnow.ws.messages.BatchResponseItem;
import com.rightnow.ws.messages.ClientInfoHeader;
import com.rightnow.ws.messages.GetProcessingOptions;
import com.rightnow.ws.messages.RNObjectsResult;
import com.rightnow.ws.messages.UpdateProcessingOptions;
import com.rightnow.ws.objects.FileAttachmentCommon;
import com.rightnow.ws.objects.FileAttachmentCommonList;
import com.rightnow.ws.objects.Incident;
import com.rightnow.ws.objects.Note;
import com.rightnow.ws.objects.NoteList;
import com.rightnow.ws.objects.StatusWithType;
import com.rightnow.ws.objects.Task;
import com.rightnow.ws.wsdl.RequestErrorFault;
import com.rightnow.ws.wsdl.RightNowSyncService;
import com.rightnow.ws.wsdl.ServerErrorFault;
import com.rightnow.ws.wsdl.UnexpectedErrorFault;

public class RightnowTaskManagerImpl extends BaseManagerImpl<RightnowTaskResponse, RightnowTaskEntry> implements
		RightnowTaskManager {

	private static final Logger LOGGER = Logger.getLogger(RightnowTaskManagerImpl.class);

	private static HashMap<String, Long> statusMap;
	//private static HashMap<String, Long> categoryMap;
	private HashMap<Long, String> closedTasks;
	private List<String> unclosedTasks;
	private WelcomeCallTaskLogDAO welcomeCallTaskLogDAO;	

	public WelcomeCallTaskLogDAO getWelcomeCallTaskLogDAO() {
		return welcomeCallTaskLogDAO;
	}

	public void setWelcomeCallTaskLogDAO(WelcomeCallTaskLogDAO welcomeCallTaskLogDAO) {
		this.welcomeCallTaskLogDAO = welcomeCallTaskLogDAO;
	}
	
	private RightnowProactiveTaskManager rightnowProactiveTaskManager;
	

	public RightnowProactiveTaskManager getRightnowProactiveTaskManager() {
		return rightnowProactiveTaskManager;
	}

	public void setRightnowProactiveTaskManager(RightnowProactiveTaskManager rightnowProactiveTaskManager) {
		this.rightnowProactiveTaskManager = rightnowProactiveTaskManager;
	}

	@Override
	public RightnowTaskResponse updateTasks(RightnowTaskListEntry taskList) throws ERPServiceException {

		RightnowTaskResponse response = new RightnowTaskResponse();
		response.setRightnowTaskEntryList(null);

		try {
			// set client info header
			ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
			clientInfoHeader.setAppID("Basic Update");
			
			//get how many tasks are already in closed status out of all the tasks
			getClosedTasks(taskList.getTaskEntryList());
			
			// Create the RNObject array
			RNObject[] objects = getRNObjectArray(taskList.getTaskEntryList());

			// Create the update processing options
			UpdateProcessingOptions options = new UpdateProcessingOptions();
			options.setSuppressExternalEvents(false);
			options.setSuppressRules(false);

			RightNowSyncService service = RightnowConnectUtility.getInstance().getService();

			// Update the task object
			service.update(objects, options, clientInfoHeader);

			StatusResponse success = new StatusResponse(RightnowSuccessCodes.RIGHTNOW_TASKS_SAVED,
					StatusResponse.Type.SUCCESS, taskList.getTaskEntryList().size());
			response.setStatus(success);
			return response;

		} catch (RemoteException e) {
			LOGGER.error("RemoteException while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		} catch (ServerErrorFault e) {
			LOGGER.error("ServerErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		} catch (RequestErrorFault e) {
			LOGGER.error("RequestErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		} catch (UnexpectedErrorFault e) {
			LOGGER.error("UnexpectedErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		} catch (Exception e) {
			LOGGER.error("Exception while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		}
	}

	/**
	 * This method returns an array of RNObject from an array of
	 * RightnowTaskEntry object
	 * 
	 * @param taskArray
	 * @return
	 */
	private RNObject[] getRNObjectArray(List<RightnowTaskEntry> taskList) {
		List<RNObject> rnObjects = new ArrayList<RNObject>();

		// Loop through the input array to build a Task object from
		// RightnowTaskEntry object
		for (RightnowTaskEntry taskEntry : taskList) {
			Task task = buildTaskObject(taskEntry);
			if (task != null) {
				rnObjects.add(task);
			}
		}

		return rnObjects.toArray(new RNObject[rnObjects.size()]);
	}

	/**
	 * Creates a Rightnow Task object and sets Notes, Status & File Attachment
	 * if any. Returns a Task object if ID is there. Otherwise, returns null.
	 * 
	 * @param taskEntry
	 * @return
	 */
	private Task buildTaskObject(RightnowTaskEntry taskEntry) {
		// proceed only if ID is set for the task object to be updated
		if (taskEntry == null || taskEntry.getId() == null || taskEntry.getId().longValue() <= 0) {
			return null;
		}
		
		//check whether the task is already closed and in which case we do not have to update its status
		if(closedTasks != null && closedTasks.get(taskEntry.getId()) != null) {
			return null;
		}

		// set the id of the Task object to be updated
		Task task = new Task();
		ID id = new ID();
		id.setId(taskEntry.getId().longValue());
		task.setID(id);

		// set notes in task
		if (taskEntry.getNote() != null && !taskEntry.getNote().isEmpty()) {
			setTaskNote(taskEntry.getNote(), task);
		}

		// set file attachment
		if (taskEntry.getFilePath() != null && !taskEntry.getFilePath().isEmpty()) {
			setAttachmentToTask(taskEntry.getFilePath(), taskEntry.getFileName(), taskEntry.getContentType(), task);
		}

		if (taskEntry.getSrStatus() != null && !taskEntry.getSrStatus().isEmpty()) {
			setTaskStatus(taskEntry.getSrStatus(), task);
		}

		return task;
	}

	/**
	 * Add a note into the task object
	 * 
	 * @param noteText
	 * @param task
	 */
	private void setTaskNote(String noteText, Task task) {
		NoteList noteList = new NoteList();
		Note note = new Note();
		note.setAction(ActionEnum.add);
		note.setText(noteText);
		noteList.addNoteList(note);
		task.setNotes(noteList);
	}

	/**
	 * Add a file attachment to the task object
	 * 
	 * @param file
	 * @param task
	 */
	private void setAttachmentToTask(String filePath, String fileName, String contentType, Task task) {
		// set data source
		DataSource dataSource = new FileDataSource(filePath);
		DataHandler dataHandler = new DataHandler(dataSource);

		// set the file attachment list
		FileAttachmentCommonList fileAttachmentCommonList = new FileAttachmentCommonList();
		FileAttachmentCommon[] fileAttachmentCommon = new FileAttachmentCommon[1];
		fileAttachmentCommon[0] = new FileAttachmentCommon();
		fileAttachmentCommon[0].setAction(ActionEnum.add);
		fileAttachmentCommon[0].setContentType(contentType);
		fileAttachmentCommon[0].setData(dataHandler);
		fileAttachmentCommon[0].setDescription(fileName);
		fileAttachmentCommon[0].setFileName(fileName);
		fileAttachmentCommon[0].setName(fileName);
		fileAttachmentCommonList.setFileAttachmentList(fileAttachmentCommon);
		task.setFileAttachments(fileAttachmentCommonList);
	}
	
	/**
	 * Upload a file attachment for the given task id
	 */
	@Override
	public RightnowTaskResponse uploadAttachment(MultipartBody body) throws ERPServiceException {
		RightnowTaskResponse response = new RightnowTaskResponse();
		response.setRightnowTaskEntryList(null);
		
		//get all the attachments
		List<Attachment> lst = body.getAllAttachments();
		if(lst == null || lst.size() != 1) {
			LOGGER.error("Invalid data passed from client. Throwing exception");
            throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { "Invalid data passed from client!" });
		}
		
		//get the attachment & disposition
		Attachment a = lst.get(0);
        ContentDisposition disposition = a.getContentDisposition();
        
        //get the file name, task id & content type
        String fileName = TaskyUtils.stripQuotes(disposition.getParameter("filename"));
        String taskid = TaskyUtils.stripQuotes(disposition.getParameter("taskId"));
        String contentType = TaskyUtils.stripQuotes(disposition.getParameter("contentType"));
        
        //if file name is empty, throw error
        if(StringUtils.isEmpty(disposition.getParameter("filename"))){
            LOGGER.error("No file name found in asset uploader. Throwing exception");
            throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { "No filename found in asset uploader!" });
        }
        
        //cast the task id parameter to long & set the Task's ID
        Long taskId = null;
        try {
        	taskId = Long.parseLong(taskid);
        } catch (NumberFormatException ex) {
        	LOGGER.error("Invalid Task Id. Throwing exception");
            throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { "Invalid Task Id!" });
        }
        Task task = new Task();
        ID id = new ID();
		id.setId(taskId);
		task.setID(id);
		
		// set the file attachment list
		FileAttachmentCommonList fileAttachmentCommonList = new FileAttachmentCommonList();
		FileAttachmentCommon[] fileAttachmentCommon = new FileAttachmentCommon[1];
		fileAttachmentCommon[0] = new FileAttachmentCommon();
		fileAttachmentCommon[0].setAction(ActionEnum.add);
		fileAttachmentCommon[0].setContentType(contentType);
		fileAttachmentCommon[0].setData(a.getDataHandler());
		fileAttachmentCommon[0].setDescription(fileName);
		fileAttachmentCommon[0].setFileName(fileName);
		fileAttachmentCommon[0].setName(fileName);
		fileAttachmentCommonList.setFileAttachmentList(fileAttachmentCommon);
		task.setFileAttachments(fileAttachmentCommonList);   
		
        // set client info header
		ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
		clientInfoHeader.setAppID("Basic Update");
		
		// Create the RNObject array
		RNObject[] objects = new RNObject[] {task};

		// Create the update processing options
		UpdateProcessingOptions options = new UpdateProcessingOptions();
		options.setSuppressExternalEvents(false);
		options.setSuppressRules(false);

		try {
			RightnowConnectUtility.getInstance().getService().update(objects, options, clientInfoHeader);
		} catch (RemoteException e) {
			LOGGER.error("RemoteException while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		} catch (ServerErrorFault e) {
			LOGGER.error("ServerErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		} catch (RequestErrorFault e) {
			LOGGER.error("RequestErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		} catch (UnexpectedErrorFault e) {
			LOGGER.error("UnexpectedErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		} catch (Exception e) {
			LOGGER.error("Exception while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		}
		StatusResponse success = new StatusResponse(RightnowSuccessCodes.RIGHTNOW_TASKS_SAVED,
				StatusResponse.Type.SUCCESS, 1);
		response.setStatus(success);
		return response;
	}

	/**
	 * Set a status value into the custom field of Task object
	 * 
	 * @param status
	 * @param task
	 */
	private void setTaskStatus(String status, Task task) {
		// get the status id
		Long id = getRightnowTaskStatusValues().get(status);

		if (id == null || id.longValue() == 0) {
			LOGGER.debug("Wrong status '" + status + "' set for Task " + task.getID().getId() + " : " + task.getName());
			return;
		}

		// set the custom field SR Status
		GenericObject genericCustomObject = new GenericObject();
		RNObjectType customObjType = new RNObjectType();
		customObjType.setTypeName("TaskCustomFieldsc");
		GenericField[] genericCustomFieldsArray = new GenericField[1];
		GenericField genericCustomField = new GenericField();
		genericCustomField.setDataType(DataTypeEnum.NAMED_ID);
		genericCustomField.setName("sr_status");
		NamedID ni = new NamedID();
		ID niID = new ID();
		niID.setId(id);
		ni.setID(niID);
		// ni.setName(status);
		DataValue dv = new DataValue();
		dv.setNamedIDValue(ni);
		genericCustomField.setDataValue(dv);
		genericCustomFieldsArray[0] = genericCustomField;
		genericCustomObject.setGenericFields(genericCustomFieldsArray);
		genericCustomObject.setObjectType(customObjType);

		// set the Custom Field
		GenericObject genericObject = new GenericObject();
		RNObjectType objType = new RNObjectType();
		objType.setTypeName("TaskCustomFields");
		GenericField[] genericFieldsArray = new GenericField[1];
		GenericField genericField = new GenericField();
		genericField.setName("c");
		genericField.setDataType(DataTypeEnum.OBJECT);
		DataValue dv1 = new DataValue();
		dv1.setObjectValue(genericCustomObject);
		genericField.setDataValue(dv1);
		genericFieldsArray[0] = genericField;
		genericObject.setGenericFields(genericFieldsArray);
		genericObject.setObjectType(objType);
		task.setCustomFields(genericObject);
	}

	/**
	 * Fetches list of notes for a single task by calling fetchRightnowTaskNotes
	 */
	@Override
	public RightnowTaskResponse getTaskNotes(Long taskId) throws ERPServiceException {
		/*
		 * Task task = new Task(); ID id = new ID(); id.setId(taskId);
		 * task.setID(id);
		 * 
		 * NoteList noteList = new NoteList(); task.setNotes(noteList);
		 * 
		 * //Build the RNObject array RNObject[] objects = new RNObject[]
		 * {task};
		 */
		List<RightnowNoteEntry> notes = null;
		List<RightnowTaskEntry> taskEntryList = new ArrayList<RightnowTaskEntry>();
		RightnowTaskResponse response = new RightnowTaskResponse();
		RightnowTaskEntry taskEntry = null;

		Map<Long, List<RightnowNoteEntry>> taskNoteEntryMap = new HashMap<Long, List<RightnowNoteEntry>>();

		// fetch Rightnow Task notes from the report
		RightnowReportManager reportManager = new RightnowReportManagerImpl();
		RightnowReportFilterEntry filter = new RightnowReportFilterEntry();
		filter.setProperty("Task ID");
		filter.setValues(new String[] { taskId.toString() });
		// RightnowReportResponse reportResponse =
		// reportManager.getReportData("Task Notes Multi",
		// new RightnowReportFilterEntry[] { filter });
		RightnowReportResponse reportResponse = reportManager.getReportData("Task Notes Multi",
				(List<RightnowReportFilterEntry>) filter);
		if (reportResponse != null
				&& (reportResponse.getStatus().getStatusType().equals(StatusResponse.Type.SUCCESS) || reportResponse
						.getStatus().getStatusType().equals("SUCCESS"))) {
			for (RightnowReportRowEntry rowEntry : reportResponse.getRightnowReportRowEntryList()) {
				notes = taskNoteEntryMap.get(rowEntry.getId());
				if (notes == null) {
					notes = new ArrayList<RightnowNoteEntry>();
				}
				if (rowEntry.getRightnowReportDataList() != null && rowEntry.getRightnowReportDataList().size() == 4) {
					RightnowNoteEntry noteEntry = new RightnowNoteEntry();
					noteEntry.setCreatedBy(rowEntry.getRightnowReportDataList().get(1));
					noteEntry.setCreatedTime(TaskyUtils
							.convertStringToDate(rowEntry.getRightnowReportDataList().get(2)));
					noteEntry.setText(rowEntry.getRightnowReportDataList().get(3));
					notes.add(noteEntry);
				}
				taskNoteEntryMap.put(rowEntry.getId(), notes);
			}
		}

		taskEntry = new RightnowTaskEntry();
		taskEntry.setNoteList(taskNoteEntryMap.get(taskId));
		taskEntry.setId(taskId);
		taskEntryList.add(taskEntry);
		response.setRightnowTaskEntryList(taskEntryList);
		// return fetchRightnowTaskDetails(objects);
		return response;
	}

	/**
	 * Fetches Task details for a given task id
	 */
	@Override
	public RightnowTaskResponse getTaskDetails(Long taskId) throws ERPServiceException {
		Task task = new Task();
		ID id = new ID();
		id.setId(taskId);
		task.setID(id);

		// get Notes
		NoteList noteList = new NoteList();
		task.setNotes(noteList);

		// get File Attachments
		FileAttachmentCommonList fileList = new FileAttachmentCommonList();
		task.setFileAttachments(fileList);

		// Build the RNObject array
		RNObject[] objects = new RNObject[] { task };
		return fetchRightnowTaskDetails(objects);
	}

	/**
	 * Fetches list of notes associated with a list of tasks by calling
	 * fetchRightnowTaskNotes
	 */
	@Override
	public RightnowTaskResponse getListOfTaskNotes(RightnowTaskListEntry taskList) throws ERPServiceException {
		List<Task> taskObjectList = new ArrayList<Task>();
		for (RightnowTaskEntry taskEntry : taskList.getTaskEntryList()) {
			Task task = new Task();
			ID id = new ID();
			id.setId(taskEntry.getId());
			task.setID(id);

			NoteList noteList = new NoteList();
			task.setNotes(noteList);

			taskObjectList.add(task);
		}

		// Build the RNObject array
		RNObject[] objects = taskObjectList.toArray(new RNObject[taskObjectList.size()]);
		return fetchRightnowTaskDetails(objects);
	}

	@Override
	public RightnowTaskResponse getRightnowTaskNotesFromReport(RightnowTaskIdEntry taskIDEntry)
			throws ERPServiceException {
		List<RightnowNoteEntry> notes = null;
		List<RightnowTaskEntry> tasks = new ArrayList<RightnowTaskEntry>();
		Map<Long, List<RightnowNoteEntry>> taskNoteEntryMap = new HashMap<Long, List<RightnowNoteEntry>>();

		try {
			// fetch Rightnow Task notes from the report
			RightnowReportManager reportManager = new RightnowReportManagerImpl();
			RightnowReportFilterEntry filter = new RightnowReportFilterEntry();
			filter.setProperty("Task ID");
			filter.setValues(taskIDEntry.getTaskId().toArray(new String[taskIDEntry.getTaskId().size()]));
			List<RightnowReportFilterEntry> filters = new ArrayList<RightnowReportFilterEntry>();
			filters.add(filter);
			// RightnowReportResponse reportResponse =
			// reportManager.getReportData("Task Notes Multi",
			// new RightnowReportFilterEntry[] { filter });
			RightnowReportResponse reportResponse = reportManager.getReportData("Task Notes Multi", filters);
			if (reportResponse != null) {
				for (RightnowReportRowEntry rowEntry : reportResponse.getRightnowReportRowEntryList()) {
					notes = taskNoteEntryMap.get(rowEntry.getId());
					if (notes == null) {
						notes = new ArrayList<RightnowNoteEntry>();
					}
					if (rowEntry.getRightnowReportDataList() != null
							&& rowEntry.getRightnowReportDataList().size() == 4) {
						RightnowNoteEntry noteEntry = new RightnowNoteEntry();
						noteEntry.setCreatedBy(rowEntry.getRightnowReportDataList().get(1));
						noteEntry.setCreatedTime(TaskyUtils.convertStringToDate(rowEntry.getRightnowReportDataList()
								.get(2)));
						noteEntry.setText(rowEntry.getRightnowReportDataList().get(3));
						notes.add(noteEntry);
					}
					taskNoteEntryMap.put(rowEntry.getId(), notes);
				}
			}

			for (Long id : taskNoteEntryMap.keySet()) {
				RightnowTaskEntry taskEntry = new RightnowTaskEntry();
				taskEntry.setId(id);
				taskEntry.setNoteList(taskNoteEntryMap.get(id));
				tasks.add(taskEntry);
			}

			RightnowTaskResponse taskResponse = new RightnowTaskResponse();
			taskResponse.setRightnowTaskEntryList(tasks);

			StatusResponse success = new StatusResponse(RightnowSuccessCodes.RIGHTNOW_TASKS_FETCHED,
					StatusResponse.Type.SUCCESS, notes.size());
			taskResponse.setStatus(success);
			return taskResponse;
			// return null;
		} catch (Exception e) {
			LOGGER.error("Exception while Fetching notes from report: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		}
	}

	/**
	 * Actual code to fetch details for single or multiple tasks.
	 * 
	 * @param tasks
	 *            an array of RNObject
	 * @return RightnowTaskResponse
	 * @throws ERPServiceException
	 */
	private RightnowTaskResponse fetchRightnowTaskDetails(RNObject[] tasks) throws ERPServiceException {
		List<RightnowNoteEntry> notes = null;
		List<RightnowFileAttachmentEntry> files = null;
		List<RightnowTaskEntry> taskEntryList = new ArrayList<RightnowTaskEntry>();
		RightnowTaskResponse response = new RightnowTaskResponse();
		RightnowTaskEntry taskEntry = null;
		try {
			// Build and set the processing options
			GetProcessingOptions options = new GetProcessingOptions();
			options.setFetchAllNames(false);

			ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
			clientInfoHeader.setAppID("Basic Get");

			// call the get method to fetch the details required for the
			// RNObject array
			RNObjectsResult getResults = RightnowConnectUtility.getInstance().getService()
					.get(tasks, options, clientInfoHeader);
			// get the RNObject array from the result
			RNObject[] rnObjects = getResults.getRNObjects();

			// iterate the RNObject array to get Task notes for each of the task
			for (RNObject object : rnObjects) {
				Task currentTask = (Task) object;
				notes = new ArrayList<RightnowNoteEntry>();
				if (currentTask.getNotes() != null) {
					for (Note note : currentTask.getNotes().getNoteList()) {
						// set each note in a Note Entry object
						RightnowNoteEntry noteEntry = new RightnowNoteEntry(note.getID().getId(), note.getText(), note
								.getCreatedTime().getTime());
						notes.add(noteEntry);
					}
				}

				files = new ArrayList<RightnowFileAttachmentEntry>();
				if (currentTask.getFileAttachments() != null) {
					for (FileAttachmentCommon file : currentTask.getFileAttachments().getFileAttachmentList()) {
						RightnowFileAttachmentEntry fileEntry = new RightnowFileAttachmentEntry(file.getID().getId(),
								file.getFileName(), file.getContentType(), file.getSize() / 1024, file.getUpdatedTime()
										.getTime(), currentTask.getID().getId());
						files.add(fileEntry);
					}
				}

				taskEntry = new RightnowTaskEntry();
				taskEntry.setId(currentTask.getID().getId());
				taskEntry.setNoteList(notes);
				taskEntry.setFileList(files);
				taskEntryList.add(taskEntry);
			}

			response.setRightnowTaskEntryList(taskEntryList);

			StatusResponse success = new StatusResponse(RightnowSuccessCodes.RIGHTNOW_TASKS_FETCHED,
					StatusResponse.Type.SUCCESS, notes.size());
			response.setStatus(success);
			return response;
		} catch (RemoteException e) {
			LOGGER.error("RemoteException while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (ServerErrorFault e) {
			LOGGER.error("ServerErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (RequestErrorFault e) {
			LOGGER.error("RequestErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (UnexpectedErrorFault e) {
			LOGGER.error("UnexpectedErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (Exception e) {
			LOGGER.error("Exception while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		}
	}

	@Override
	public RightnowTaskResponse getTaskAttachments(Long taskId) throws ERPServiceException {
		List<RightnowFileAttachmentEntry> files = null;
		List<RightnowTaskEntry> taskEntryList = new ArrayList<RightnowTaskEntry>();
		RightnowTaskResponse response = new RightnowTaskResponse();
		RightnowTaskEntry taskEntry = null;

		// Build and set the processing options
		GetProcessingOptions options = new GetProcessingOptions();
		options.setFetchAllNames(false);

		ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
		clientInfoHeader.setAppID("Basic Get");

		Task task = new Task();
		ID id = new ID();
		id.setId(taskId);
		task.setID(id);

		FileAttachmentCommonList fileList = new FileAttachmentCommonList();
		task.setFileAttachments(fileList);

		RNObject[] tasks = new RNObject[] { task };

		// call the get method to fetch the details required for the RNObject
		// array
		try {
			RNObjectsResult getResults = RightnowConnectUtility.getInstance().getService()
					.get(tasks, options, clientInfoHeader);

			// get the RNObject array from the result
			RNObject[] rnObjects = getResults.getRNObjects();

			// iterate the RNObject array to get Task notes for each of the task
			for (RNObject object : rnObjects) {
				Task currentTask = (Task) object;
				files = new ArrayList<RightnowFileAttachmentEntry>();
				if (currentTask.getFileAttachments() != null) {
					for (FileAttachmentCommon file : currentTask.getFileAttachments().getFileAttachmentList()) {

						RightnowFileAttachmentEntry fileEntry = new RightnowFileAttachmentEntry(file.getID().getId(),
								file.getFileName(), file.getContentType(), file.getSize() / 1024, file.getUpdatedTime()
										.getTime(), currentTask.getID().getId());
						files.add(fileEntry);
					}
				}
				taskEntry = new RightnowTaskEntry();
				taskEntry.setId(currentTask.getID().getId());
				taskEntry.setFileList(files);
				taskEntryList.add(taskEntry);
			}

			response.setRightnowTaskEntryList(taskEntryList);

			StatusResponse success = new StatusResponse(RightnowSuccessCodes.RIGHTNOW_TASKS_FETCHED,
					StatusResponse.Type.SUCCESS, files.size());
			response.setStatus(success);
			return response;
		} catch (RemoteException e) {
			LOGGER.error("RemoteException while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (ServerErrorFault e) {
			LOGGER.error("ServerErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (RequestErrorFault e) {
			LOGGER.error("RequestErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (UnexpectedErrorFault e) {
			LOGGER.error("UnexpectedErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (Exception e) {
			LOGGER.error("Exception while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		}
	}

	@Override
	public RightnowTaskResponse getOriginalAttachment(RightnowFileAttachmentEntry fileEntry) throws ERPServiceException {
		ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
		clientInfoHeader.setAppID("Basic Get");

		ID fileID = new ID();
		fileID.setId(fileEntry.getId());

		RNObject object = new Task();
		ID taskID = new ID();
		taskID.setId(fileEntry.getTaskId());
		object.setID(taskID);

		try {
			DataHandler fileData = RightnowConnectUtility.getInstance().getService()
					.getFileData(object, fileID, true, clientInfoHeader);
			String dirSep = System.getProperty("file.separator");
			URL resource = getClass().getResource("/");
			String path = resource.getPath();
			if(path != null && path.contains("WEB-INF/classes/")) {
				path = path.replace("WEB-INF/classes/", "");
			}
			String topLevelTmpDirectory = path + "tmp" + dirSep + "images" + dirSep;
			File file = new File(topLevelTmpDirectory + "crm" + dirSep + "attachments");
			FileOutputStream outputStream = null;
			InputStream is = null;
			
			try {
				if (!file.exists()) {
					file.mkdirs();
				}
				String finalFilePath = file.getPath() + dirSep + fileEntry.getFileName();
				outputStream = new FileOutputStream(finalFilePath);
				is = fileData.getInputStream();
				byte data[] = new byte[1024];
				int br;
				while ((br = is.read(data)) > 0) {
					outputStream.write(data, 0, br);				    
				}
				//fileData.writeTo(outputStream);				
				fileEntry.setFilePath(finalFilePath);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(outputStream != null) {
					outputStream.flush();
					outputStream.close();
				}
				
				if(is != null) {
					is.close();
				}
			}
			List<RightnowFileAttachmentEntry> fileList = new ArrayList<RightnowFileAttachmentEntry>();
			fileList.add(fileEntry);
			
			RightnowTaskEntry taskEntry = new RightnowTaskEntry();
			taskEntry.setFileList(fileList);

			List<RightnowTaskEntry> taskList = new ArrayList<RightnowTaskEntry>();
			taskList.add(taskEntry);

			RightnowTaskResponse response = new RightnowTaskResponse();
			response.setRightnowTaskEntryList(taskList);
			
			StatusResponse success = new StatusResponse(RightnowSuccessCodes.RIGHTNOW_TASKS_FETCHED,
					StatusResponse.Type.SUCCESS, 1);
			response.setStatus(success);
			// return fileEntry;
			
			return response;
		} catch (RemoteException e) {
			LOGGER.error("RemoteException while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (ServerErrorFault e) {
			LOGGER.error("ServerErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (RequestErrorFault e) {
			e.printStackTrace();
			LOGGER.error("RequestErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (UnexpectedErrorFault e) {
			LOGGER.error("UnexpectedErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (Exception e) {
			LOGGER.error("Exception while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		}
	}

	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse create(RightnowTaskEntry entry) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(RightnowTaskEntry entry, Long itemId) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	private static HashMap<String, Long> getRightnowTaskStatusValues() {

		if (statusMap != null)
			return statusMap;

		statusMap = new HashMap<String, Long>();
		try {
			// set client info header
			ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
			clientInfoHeader.setAppID("Get Custom Field Values");

			NamedID[] results = RightnowConnectUtility.getInstance().getService()
					.getValuesForNamedID(null, "Task.CustomFields.c.sr_status", clientInfoHeader);
			for (NamedID status : results) {
				statusMap.put(status.getName(), status.getID().getId());
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ServerErrorFault e) {
			e.printStackTrace();
		} catch (RequestErrorFault e) {
			e.printStackTrace();
		} catch (UnexpectedErrorFault e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusMap;
	}

	/*public static HashMap<String, Long> getCategoriesForTasks() throws ERPServiceException {
		if (categoryMap != null)
			return categoryMap;		
		String[] rowData = TaskyUtils.getReportData("engg_category", null);
		if (rowData != null) {
			categoryMap = new HashMap<String, Long>();
			for (String row : rowData) {
				String[] data = row.split(RightnowConstants.RIGHTNOW_REPORT_DELIMITER_SPLIT_ESCAPE, -1);
				if (data != null && data.length == 4) {
					Long id = Long.parseLong(data[0]);
					String category = data[1] == null ? "" : data[1];
					if (data[2] != null && !data[2].isEmpty())
						category += ":" + data[2];
					if (data[3] != null && !data[3].isEmpty())
						category += ":" + data[3];
					categoryMap.put(category.toLowerCase(), id);
				}
			}
		}
		return categoryMap;
	}*/	
	
	private void getClosedTasks(List<RightnowTaskEntry> taskList)  throws ERPServiceException {
		
		StringBuilder ids = new StringBuilder("");
		for(RightnowTaskEntry taskEntry : taskList) {
			if(ids.length() > 0) {
				ids.append(",");
			}
			ids.append(taskEntry.getId());
		}
		RightnowReportFilterEntry filterEntry = new RightnowReportFilterEntry();
		filterEntry.setProperty("Task ID Filter");
		filterEntry.setValues(new String[] {ids.toString()});
		filterEntry.setOperator(10);
		filterEntry.setDataType(3);
		List<RightnowReportFilterEntry> filters = new ArrayList<RightnowReportFilterEntry>();
		filters.add(filterEntry);
		
		RightnowReportData reportData = TaskyUtils.getReportData("closed_tasks", filters); 
		int taskIdIndex = reportData.getIndexOfColumn(RightnowReportColumnConstants.TASK_ID_COLUMN);
		int taskStatusIndex = reportData.getIndexOfColumn(RightnowReportColumnConstants.TASK_STATUS_COLUMN);
		String[] rowData = reportData.getRowData();
		if (rowData != null && taskIdIndex > -1 && taskStatusIndex > -1) {
			closedTasks = new HashMap<Long, String>();
			for (String row : rowData) {
				String[] data = row.split(RightnowConstants.RIGHTNOW_REPORT_DELIMITER_SPLIT_ESCAPE, -1);
				if (data != null) {
					Long id = Long.parseLong(data[taskIdIndex]);
					String status = data[taskStatusIndex] == null ? "" : data[taskStatusIndex];
					closedTasks.put(id, status);
					
				}
			}
		}
	}
	
	/**
	 * Close all the tasks with incidents (if incident does not have any other unclosed tasks)
	 * 1. Get all the Incident IDs corresponding to the given Task IDs
	 * 2. Close all the Tasks
	 * 3. Get all the Incidents which still has open Tasks
	 * 4. Close all the Incidents which are not on the above list
	 */
	@Override
	public RightnowTaskResponse closeTasks(RightnowTaskIdEntry taskIDEntry) throws ERPServiceException {
		
		List<RNObject> objectList = new ArrayList<RNObject>();
		List<RightnowTaskEntry> failedTasks = new ArrayList<RightnowTaskEntry>();
		RightnowTaskResponse response = new RightnowTaskResponse();
		response.setRightnowTaskEntryList(null);
		
		//if no task ids are there
		if(taskIDEntry == null || taskIDEntry.getTaskId() == null || taskIDEntry.getTaskId().isEmpty())
			return null;
		
		//get the list of all the tasks along with incidents. We will not filter out closed tasks.
		List<RightnowTaskEntry> tasksWithIncidents = getUnclosedTaskIncidents(taskIDEntry.getTaskId());
		
		//if no records are fetched from the report, raise an error response
		if(tasksWithIncidents == null || tasksWithIncidents.isEmpty()) {
			StatusResponse error = new StatusResponse(RightnowErrorCodes.NO_TASK_FETCHED, StatusResponse.Type.SUCCESS, 1);
			response.setStatus(error);
			return response;
		}
		
		// Create the update processing options
		UpdateProcessingOptions options = new UpdateProcessingOptions();
		options.setSuppressExternalEvents(false);
		options.setSuppressRules(false);
		
		// set client info header
		ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
		clientInfoHeader.setAppID("Basic Update");
		
	
		//update all the tasks to Closed state
		for(RightnowTaskEntry taskEntry : tasksWithIncidents) {
			//set task id
			Task task = new Task();
			ID taskID = new ID();
			taskID.setId(taskEntry.getId());
			task.setID(taskID);
			
			//set the task status to closed
			setTaskStatus("Closed", task);				
			
			objectList.add(task);
		}				
		
		try {
			// Create the RNObject array
			RNObject[] objects = objectList.toArray(new RNObject[objectList.size()]);	
			
			//update tasks
			RightnowConnectUtility.getInstance().getService().update(objects, options, clientInfoHeader);
			
			//get open incidents
			List<Long> openIncidents = getIncidentsWithOpenTaks(tasksWithIncidents);
			
			objectList = new ArrayList<RNObject>();
			
			//check which of the incidents can be closed
			for(RightnowTaskEntry taskEntry : tasksWithIncidents) {
				Long incidentId = taskEntry.getIncidentId();
				
				if(openIncidents != null && openIncidents.indexOf(incidentId) > -1) {
					failedTasks.add(taskEntry);
					continue;
				}
				
				Incident incident = new Incident();
				ID incidentID = new ID();
				incidentID.setId(taskEntry.getIncidentId());
				incident.setID(incidentID);
				
				//set incident status to Closed
				StatusWithType statusWithType = new StatusWithType();
				NamedID statusNId = new NamedID();
				statusNId.setName("Solved");
				statusWithType.setStatus(statusNId);
				incident.setStatusWithType(statusWithType);
				
				objectList.add(incident);
			}
			
			//update incidents which does not have any open tasks
			if(!objectList.isEmpty()) {
				// Create the RNObject array
				objects = objectList.toArray(new RNObject[objectList.size()]);
				
				//update tasks
				RightnowConnectUtility.getInstance().getService().update(objects, options, clientInfoHeader);
			
			}
			
			StatusResponse success = null;
			//set the task entry list to notify the user which of the incidents cannot be closed
			if(failedTasks.size() > 0) {
				success = new StatusResponse(RightnowErrorCodes.TASKS_CLOSED_WITH_EXCEPTIONS,
					StatusResponse.Type.SUCCESS, tasksWithIncidents.size());
				response.setRightnowTaskEntryList(failedTasks);
			} else {
				success = new StatusResponse(RightnowSuccessCodes.RIGHTNOW_TASKS_SAVED,
						StatusResponse.Type.SUCCESS, tasksWithIncidents.size());
			}
			response.setStatus(success);
		} catch (RemoteException e) {
			e.printStackTrace();
			LOGGER.error("RemoteException while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		} catch (ServerErrorFault e) {
			e.printStackTrace();
			LOGGER.error("ServerErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		} catch (RequestErrorFault e) {
			e.printStackTrace();
			LOGGER.error("RequestErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		} catch (UnexpectedErrorFault e) {
			e.printStackTrace();
			LOGGER.error("UnexpectedErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
					new String[] { e.getMessage() });
		}
		
		return response;
				
	}
	
	private List<RightnowTaskEntry> getUnclosedTaskIncidents(List<String> taskIdList) throws ERPServiceException {
		List<RightnowTaskEntry> unclosedTaskIncidents = null;
		//create the report filter object
		StringBuilder ids = new StringBuilder("");
		for(String taskId : taskIdList) {
			if(ids.length() > 0) {
				ids.append(",");
			}
			ids.append(taskId);
		}
		
		RightnowReportFilterEntry filterEntry = new RightnowReportFilterEntry();
		filterEntry.setProperty("task");
		filterEntry.setValues(new String[] {ids.toString()});
		filterEntry.setOperator(10);
		filterEntry.setDataType(3);
		List<RightnowReportFilterEntry> filters = new ArrayList<RightnowReportFilterEntry>();
		filters.add(filterEntry);
		
		RightnowReportData reportData = TaskyUtils.getReportData("unclosed_tasks_incidents", filters);
		int taskIdIndex = reportData.getIndexOfColumn(RightnowReportColumnConstants.TASK_ID_COLUMN);
		int incidentIdIndex = reportData.getIndexOfColumn(RightnowReportColumnConstants.INCIDENT_ID_COLUMN);
		int catLvl1Index = reportData.getIndexOfColumn(RightnowReportColumnConstants.CATEGORY_LEVEL_1_COLUMN);
		String[] rowData = reportData.getRowData();
		if(rowData != null && taskIdIndex > -1 && incidentIdIndex > -1 && catLvl1Index > -1) {
			unclosedTaskIncidents = new ArrayList<RightnowTaskEntry>();
			unclosedTasks = new ArrayList<String>();
			for (String row : rowData) {
				String[] data = row.split(RightnowConstants.RIGHTNOW_REPORT_DELIMITER_SPLIT_ESCAPE, -1);
				if (data != null) {
					try {
						RightnowTaskEntry taskEntry = new RightnowTaskEntry();
						taskEntry.setId(Long.parseLong(data[taskIdIndex]));
						taskEntry.setIncidentId(Long.parseLong(data[incidentIdIndex]));
						taskEntry.setMainCategory(data[catLvl1Index]);
						unclosedTaskIncidents.add(taskEntry);
						unclosedTasks.add(data[taskIdIndex]);
					} catch (NumberFormatException ex) {
						
					}					
				}
			}
		}
		return unclosedTaskIncidents;
	}
	
	private List<Long> getIncidentsWithOpenTaks(List<RightnowTaskEntry> taskEntries) throws ERPServiceException {
		List<Long> openIncidents = null;
		//create the report filter object
		StringBuilder ids = new StringBuilder("");
		for(RightnowTaskEntry taskEntry : taskEntries) {
			if(ids.length() > 0) {
				ids.append(",");
			}
			ids.append(taskEntry.getIncidentId());
		}
		
		RightnowReportFilterEntry filterEntry = new RightnowReportFilterEntry();
		filterEntry.setProperty("incident");
		filterEntry.setValues(new String[] {ids.toString()});
		filterEntry.setOperator(10);
		filterEntry.setDataType(3);
		List<RightnowReportFilterEntry> filters = new ArrayList<RightnowReportFilterEntry>();
		filters.add(filterEntry);
		
		RightnowReportData reportData = TaskyUtils.getReportData("Incident_open_task_count", filters);
		int incidentIdIndex = reportData.getIndexOfColumn(RightnowReportColumnConstants.INCIDENT_ID_COLUMN);
		String[] rowData = reportData.getRowData();
		if(rowData != null && incidentIdIndex > -1) {
			openIncidents = new ArrayList<Long>();
			for (String row : rowData) {
				String[] data = row.split(RightnowConstants.RIGHTNOW_REPORT_DELIMITER_SPLIT_ESCAPE, -1);
				if (data != null) {
					try {
						openIncidents.add(Long.parseLong(data[incidentIdIndex]));
					} catch (NumberFormatException ex) {
						
					}					
				}
			}
		}
		return openIncidents;
	}
	
	/**
	 * Hits the report 'Find missing Tasks' and get all the tasks (along with other information). Then update all those tasks which don't have Category info set.
	 * @return
	 * @throws ERPServiceException
	 */
	public RightnowTaskResponse fixTasksWithNoCategories() throws ERPServiceException {
		//get the tasks which do not have proper category info
		List<RightnowTaskEntry> missingCatTasks = getMissingCategoriesTasksReportData();
		List<Task> taskList = new ArrayList<Task>();
		
		ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
		clientInfoHeader.setAppID("Batcher");
		
		RightnowTaskResponse response = new RightnowTaskResponse();
		response.setRightnowTaskEntryList(null);
		
		if(missingCatTasks.size() == 0) {
			StatusResponse error = new StatusResponse(RightnowErrorCodes.NO_TASK_FETCHED,
					StatusResponse.Type.SUCCESS, 0);
			response.setStatus(error);
			return response;
		}
		
		for(RightnowTaskEntry taskEntry : missingCatTasks) {
			
			//set Task ID
			Task task = new Task();
			ID id = new ID();
			id.setId(taskEntry.getId());
			task.setID(id);
			
			//set category
			GenericField[] genericFields = new GenericField[1];
			genericFields[0] = TaskyUtils.addCustomFields("Category", taskEntry.getCategoryId(), true, false);			
			GenericObject genericCustomCategory = TaskyUtils.createCustomObject(genericFields, "TaskCustomFieldsco");
			
			//set category to CO namespace
			GenericField[] genericFieldsArray = new GenericField[1];
			// set the custom objects for task
			genericFieldsArray[0] = TaskyUtils.addCustomFields("CO", genericCustomCategory, false, false);
			// add the custom array to the task object
			task.setCustomFields(TaskyUtils.createCustomObject(genericFieldsArray, "TaskCustomFields"));
			taskList.add(task);
		}
		
		
		List<BatchRequestItem> batchRequestItems = new ArrayList<BatchRequestItem>();
		int length = taskList.size();
		int i = 0;
		int startIndex = 0;
		int endIndex = 0;
		int batchSize = 700;
		while (i < length) {
			startIndex = i;
			endIndex = startIndex+batchSize > length ? length : startIndex+batchSize;			
			List<Task> batchList = taskList.subList(startIndex, endIndex);			
			batchRequestItems.add(TaskyUtils.getBatchRequestItemForUpdate(batchList.toArray(new RNObject[batchList.size()])));
			i+= batchSize;
		}
		
		try {
			BatchResponseItem[] responseItems = RightnowConnectUtility
					.getInstance()
					.getService()
					.batch(batchRequestItems.toArray(new BatchRequestItem[batchRequestItems.size()]),
							clientInfoHeader);
			String errorMessage = "";
			for (BatchResponseItem responseItem : responseItems) {
				if (responseItem.isUpdateResponseMsgSpecified()) {
					//UpdateResponseMsg updateResponseMsg = responseItem.getUpdateResponseMsg();
					LOGGER.error("Batch Update successful");
				} else if (responseItem.isRequestErrorFaultSpecified()) {
					LOGGER.error(responseItem.getRequestErrorFault().getExceptionMessage());
					errorMessage += responseItem.getRequestErrorFault().getExceptionMessage();
				}
			}
			
			if (!errorMessage.isEmpty()) {
				StatusResponse error = new StatusResponse(RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW,
						StatusResponse.Type.SUCCESS, 0);
				error.setStatusMessage(errorMessage);
				response.setStatus(error);
				return response;
			} else {
				StatusResponse success = new StatusResponse(RightnowSuccessCodes.RIGHTNOW_TASKS_SAVED,
						StatusResponse.Type.SUCCESS, length);
				response.setStatus(success);
				return response;
			}
		} catch (RemoteException e) {
			LOGGER.error("RemoteException while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (ServerErrorFault e) {
			LOGGER.error("ServerErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (RequestErrorFault e) {
			e.printStackTrace();
			LOGGER.error("RequestErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (UnexpectedErrorFault e) {
			LOGGER.error("UnexpectedErrorFault while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception while updating Tasks: " + e.getMessage(), e);
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_TASK_FETCHED, new String[] { e.getMessage() });
		}
	}
	
	private List<RightnowTaskEntry> getMissingCategoriesTasksReportData() throws ERPServiceException {
		List<RightnowTaskEntry> missingCatTasks = new ArrayList<RightnowTaskEntry>();
		
		RightnowReportData reportData = TaskyUtils.getReportData("Find missing Tasks", null);
		int taskIdIndex = reportData.getIndexOfColumn(RightnowReportColumnConstants.TASK_ID_COLUMN);
		int categoryIndex = reportData.getIndexOfColumn(RightnowReportColumnConstants.CATEGORY_COLUMN);
		int catIdIndex = reportData.getIndexOfColumn(RightnowReportColumnConstants.CATEGORY_ID_COLUMN);
		String[] rowData = reportData.getRowData();
		
		if(rowData != null && taskIdIndex > -1 && categoryIndex > -1 && catIdIndex > -1) {			 
			for (String row : rowData) {
				String[] data = row.split(RightnowConstants.RIGHTNOW_REPORT_DELIMITER_SPLIT_ESCAPE, -1);
				if (data != null) {
					try {
						String category = data[categoryIndex];
						Long taskId = Long.parseLong(data[taskIdIndex]);
						Long catId = Long.parseLong(data[catIdIndex]);
						if((category == null || category.isEmpty()) 
								&& catId > 0) {
							RightnowTaskEntry taskEntry = new RightnowTaskEntry();
							taskEntry.setId(taskId);
							taskEntry.setCategoryId(catId);
							missingCatTasks.add(taskEntry);
						}
						
					} catch (NumberFormatException ex) {
						
					}					
				}
			}
		}
		
		return missingCatTasks;
	}
	
	@Override
	public RightnowTaskResponse createWelcomeCallTask(OrderEntry lmsOrderEntry) throws ERPServiceException {		

		RightnowTaskResponse taskResponse = new RightnowTaskResponse();
		String searchTerms = "id.eq:" + lmsOrderEntry.getOrderId();
		OrderReleaseResponse releaseResponse = new OrderReleaseResponse();

		try {
			// TODO move this to OMS Client codebase.
			BaseWebClient client = new BaseWebClient(null, ServiceURLProperty.OMS_URL, Context.getContextInfo());
			client.path("/oms/orderrelease/search");
			client.query("q", searchTerms);
			releaseResponse = client.get(OrderReleaseResponse.class);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			// TODO need proper handling of exception
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			taskResponse.setStatus(error);
			throw new ERPServiceException(taskResponse);
		}

		if (releaseResponse != null && releaseResponse.getData() != null && releaseResponse.getData().size()>0) {

			List<OrderReleaseEntry> releaseEntryList = releaseResponse.getData();
			OrderReleaseEntry releaseEntry = releaseEntryList.get(0);
			
			// Rule 0 : check for delivered order			
			if (!lmsOrderEntry.getStatus().name().equalsIgnoreCase("DL")) {				
				StatusCodes errorCreateWelcomeCallTask = RightnowErrorCodes.ERROR_WELCOME_CALL_FILTER_STATUS;
				return setResponseStatusAndReturn(taskResponse, errorCreateWelcomeCallTask);
			}			

			// Rule 1 : check for logistic ML
			if (!releaseEntry.getCourierCode().equalsIgnoreCase("ML")) {				
				StatusCodes errorCreateWelcomeCallTask = RightnowErrorCodes.ERROR_WELCOME_CALL_FILTER_ML;
				return setResponseStatusAndReturn(taskResponse, errorCreateWelcomeCallTask);
			}

			// Rule 2 : check for not delayed delivery order release
			try {
				// compare delivery date of LMS entry with OMS promise date
				// because by this time delivery date in OMS might not have updated
				// and hence may be null
				if (lmsOrderEntry.getDeliveryDate().compareTo(releaseEntry.getCustomerPromiseTime()) > 0) {					
					StatusCodes errorCreateWelcomeCallTask = RightnowErrorCodes.ERROR_WELCOME_CALL_FILTER_DELIVERY_TAT;
					return setResponseStatusAndReturn(taskResponse, errorCreateWelcomeCallTask);
				}
			} catch (NullPointerException e) {
				LOGGER.error(e.getMessage(), e);
				// TODO need proper handling of exception
				StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
				taskResponse.setStatus(error);
				throw new ERPServiceException(taskResponse);
			}

			// Rule 3 : check release has atleast one returnable item
			boolean hasReturnableItem = false;
			for (OrderLineEntry lineEntry : releaseEntry.getOrderLines()) {
				if (lineEntry.getIsReturnableProduct()) {
					hasReturnableItem = true;
					break;
				}
			}
			if (!hasReturnableItem) {				
				StatusCodes errorCreateWelcomeCallTask = RightnowErrorCodes.ERROR_WELCOME_CALL_FILTER_RETURNABLE;
				return setResponseStatusAndReturn(taskResponse, errorCreateWelcomeCallTask);
			}

			// Rule 4 : check if release is the first delivered release for the customer
			String searchTermsForOldDeliveredRelease = "login.eq:" + releaseEntry.getLogin() + "___releaseStatus.in:C,DL";
					
			try {				
				// TODO move this to OMS Client codebase.
				BaseWebClient client = new BaseWebClient(null, ServiceURLProperty.OMS_URL, Context.getContextInfo());
				client.path("/oms/orderrelease/search");
				client.query("q", searchTermsForOldDeliveredRelease);
				client.query("fetchSize", 2);
				OrderReleaseResponse oldReleaseResponse = client.get(OrderReleaseResponse.class);
				
				if(oldReleaseResponse != null && oldReleaseResponse.getData() != null && oldReleaseResponse.getData().size()>=2){					
					StatusCodes errorCreateWelcomeCallTask = RightnowErrorCodes.ERROR_WELCOME_CALL_FILTER_FIRST_ORDER;
					return setResponseStatusAndReturn(taskResponse, errorCreateWelcomeCallTask);
					
				} else if(oldReleaseResponse.getData().size()==1){
					// this check in case where order status is not updated to DL in OMS					
					if(!releaseEntry.getId().equals(oldReleaseResponse.getData().get(0).getId())){
						StatusCodes errorCreateWelcomeCallTask = RightnowErrorCodes.ERROR_WELCOME_CALL_FILTER_FIRST_ORDER;
						return setResponseStatusAndReturn(taskResponse, errorCreateWelcomeCallTask);
					}
					
				}
				// TODO what if oldReleaseResponse is null or oldReleaseResponse.getData() is null
				// okey if they are null then rule is passed anyway				
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
				// TODO need proper handling of exception
				StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
				taskResponse.setStatus(error);
				throw new ERPServiceException(taskResponse);
			}
			
			// Rule 5 : check welcome call task is already created for the releaseid in crm schema			
			WelcomeCallTaskLogEntity welcomeCallTaskLogEntity = welcomeCallTaskLogDAO.getWelcomeCallTaskLog(lmsOrderEntry
					.getOrderId());
			if(welcomeCallTaskLogEntity != null){				
				StatusCodes errorCreateWelcomeCallTask = RightnowErrorCodes.ERROR_WELCOME_CALL_TASK_EXISTS;
				return setResponseStatusAndReturn(taskResponse, errorCreateWelcomeCallTask);
			}
			
			// Rule 6: check task created per hour does not exceed CC hourly capacity			
	
			// set task creation date and take all other date references			 
			// from this date such as date range to get total tasks for the hour,
			// to get the app property for the hour etc..
			Date taskCreationDate = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(taskCreationDate);
			int hour = calendar.get(Calendar.HOUR_OF_DAY);
			
			// get app property from lala
			ApplicationPropertiesResponse appPropResponse = CoreServiceClient.getApplicationPropertyByName(null, "crm.cc_task_capacity_" +hour,
					Context.getContextInfo());
			if (appPropResponse == null || appPropResponse.getData() == null 
					|| appPropResponse.getData().size()<1) {				
				StatusCodes errorCreateWelcomeCallTask = RightnowErrorCodes.ERROR_WELCOME_CALL_CC_CAPACITY;
				return setResponseStatusAndReturn(taskResponse, errorCreateWelcomeCallTask);				
			}
			
			ApplicationPropertiesEntry appPropEntry = appPropResponse.getData().get(0);		
			int ccCapacity = Integer.parseInt(appPropEntry.getValue());			
			
			if(ccCapacity < 1){				
				StatusCodes errorCreateWelcomeCallTask = RightnowErrorCodes.ERROR_WELCOME_CALL_CC_CAPACITY;
				return setResponseStatusAndReturn(taskResponse, errorCreateWelcomeCallTask);
			}
			
			// set start date range from task date as yyyy-MM-dd HH:00:00
			Date startDateRange = taskCreationDate;
			Date endDateRange = taskCreationDate;
			Calendar calForDateRange = Calendar.getInstance();
			calForDateRange.setTime(startDateRange);			
			calForDateRange.set(Calendar.MINUTE, 0);
			calForDateRange.set(Calendar.SECOND, 0);
			startDateRange = calForDateRange.getTime();
						
			// set end date range from task date as yyyy-MM-dd HH:59:59
			calForDateRange.set(Calendar.MINUTE, 59);
			calForDateRange.set(Calendar.SECOND, 59);
			endDateRange = calForDateRange.getTime();			
			
			Integer totalTasks = welcomeCallTaskLogDAO.getTotalWelcomeCallTask(startDateRange, endDateRange);			

			if(totalTasks >= ccCapacity){
				StatusCodes errorCreateWelcomeCallTask = RightnowErrorCodes.ERROR_WELCOME_CALL_CC_CAPACITY;
				return setResponseStatusAndReturn(taskResponse, errorCreateWelcomeCallTask);
			}
			
			// Rule 7: distribution criteria for hour(60 mins) - 
			// i.e distributing tasks generation through 60 mins
			// eg: 5 tasks need to be created in 60 mins then every 
			// task needs to be created on every 12th min(60/5) in that hour
			// TODO commenting out rule 7, will enable later
			// taking numerator as 55 rather than 60(mins) because we are 
			// having hourly capacity from say 9:00 to 9:59:59, we are taking 
			// as 55 mins , just to make sure that last 
			// task of the hour is created well before 59th minute's 59th sec.
			/*double distributionInterval = (double) 55/ccCapacity;
			int Mins = calendar.get(Calendar.MINUTE);
			int Secs = calendar.get(Calendar.SECOND);
			double elapsedMins = Mins + Secs/60;
			
			if (elapsedMins/(totalTasks+1) < distributionInterval){
				LOGGER.debug("@@@   Distribution criteria failed : dist.Interval="+distributionInterval+",cc-capacity="+ccCapacity+
						",elapsed mins="+elapsedMins+",total tasks="+totalTasks+"     @@@"); 
				StatusCodes errorCreateWelcomeCallTask = RightnowErrorCodes.ERROR_WELCOME_CALL_DISTRIBUTION_CRITERIA;
				return setResponseStatusAndReturn(taskResponse, errorCreateWelcomeCallTask);
			}*/

			// create welcome call task on all rules are passed 
			List<RightnowBaseTaskEntry> baseTaskEntrieList = new ArrayList<RightnowBaseTaskEntry>();			 
			RightnowTaskWelcomeCallingEntry welcomeCallTaskEntry = new RightnowTaskWelcomeCallingEntry();			
			// setting release id in orderId
			welcomeCallTaskEntry.setOrderId(releaseEntry.getId());
			welcomeCallTaskEntry.setMainCategory("Proactive OB");
			welcomeCallTaskEntry.setSubCategory("Welcome Calling");
			welcomeCallTaskEntry.setSubSubCategory("First Time Customer");
			// welcomeCallTaskEntry.setComments("Test notes welcome calling");
			baseTaskEntrieList.add(welcomeCallTaskEntry);
			RightnowBaseTaskResponse baseTaskResponse = rightnowProactiveTaskManager.createTasks(baseTaskEntrieList);
			
			if(baseTaskResponse == null || baseTaskResponse.getStatus().getStatusCode() == 811 
					|| baseTaskResponse.getRightnowBaseTaskEntryList().size()<1){
				StatusCodes errorCreateWelcomeCallTask = RightnowErrorCodes.ERROR_CREATE_WELCOME_CALL_TASK;
				return setResponseStatusAndReturn(taskResponse, errorCreateWelcomeCallTask);
			}		
			
			// log the task into table welcome_call_task_log
			// get the task id generated to log
			List<RightnowBaseTaskEntry> respBaseTaskEntrieList = baseTaskResponse.getRightnowBaseTaskEntryList();
			WelcomeCallTaskLogEntity entity = new WelcomeCallTaskLogEntity();
			entity.setCreatedBy("system");
			entity.setCreatedOn(taskCreationDate);
			entity.setLastModifiedOn(taskCreationDate);
			entity.setVersion(1l);
			entity.setOrderId(releaseEntry.getOrderId());
			entity.setReleaseId(releaseEntry.getId());			
			entity.setTaskId(respBaseTaskEntrieList.get(0).getId());
			
			try {				
				WelcomeCallTaskLogEntity loggedEntity;
				loggedEntity = welcomeCallTaskLogDAO.create(entity);				

			} catch (ERPServiceException e) {
				LOGGER.error(e.getMessage(), e);
				taskResponse.setStatus(new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR));
				throw new ERPServiceException(taskResponse);
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
				StatusResponse error = new StatusResponse(RightnowErrorCodes.ERROR_CREATE_WELCOME_CALL_TASK_LOG,
						StatusResponse.Type.ERROR);
				taskResponse.setStatus(error);
				throw new ERPServiceException(taskResponse);
			}
			
			StatusResponse success = new StatusResponse(RightnowSuccessCodes.SUCCESS_CREATE_WELCOME_CALL_TASK,
					StatusResponse.Type.SUCCESS, 1);
			taskResponse.setStatus(success);
			return taskResponse;			

		} else {			
			StatusCodes errorCreateWelcomeCallTask = RightnowErrorCodes.ERROR_CREATE_WELCOME_CALL_TASK;
			return setResponseStatusAndReturn(taskResponse, errorCreateWelcomeCallTask);
		}
	}

	/**
	 * @param taskResponse
	 * @param errorCreateWelcomeCallTask
	 * @return
	 */
	public RightnowTaskResponse setResponseStatusAndReturn(RightnowTaskResponse taskResponse,
			StatusCodes errorCreateWelcomeCallTask) {
		StatusResponse error = new StatusResponse(errorCreateWelcomeCallTask, StatusResponse.Type.ERROR);
		taskResponse.setStatus(error);
		return taskResponse;
	}


	public static void main(String args[]) throws ERPServiceException {	
		RightnowTaskManagerImpl rtml = new RightnowTaskManagerImpl();		
		//List<String> taskIdList = new ArrayList<String>();
		String ids = "67123,67124,67125,67126,67127,67128,67129,67130,67229";
		List<String> taskIdList = Arrays.asList(ids.split(","));
		//taskIdList.add("67126");
		RightnowTaskIdEntry taskIdEntry = new RightnowTaskIdEntry();
		taskIdEntry.setTaskId(taskIdList);
		List<RightnowTaskEntry> taskList = new ArrayList<RightnowTaskEntry>();
		RightnowTaskEntry taskEntry1 = new RightnowTaskEntry();
		taskEntry1.setId(67123L);
		RightnowTaskEntry taskEntry2 = new RightnowTaskEntry();
		taskEntry1.setId(67131L);
		taskList.add(taskEntry1);
		taskList.add(taskEntry2);
		rtml.getClosedTasks(taskList);
		//rtml.fixTasksWithNoCategories();
	}

}