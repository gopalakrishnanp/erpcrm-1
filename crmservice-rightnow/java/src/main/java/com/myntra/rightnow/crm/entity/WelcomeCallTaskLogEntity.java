package com.myntra.rightnow.crm.entity;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import com.myntra.commons.entities.BaseEntity;
import com.myntra.rightnow.crm.constant.RightnowConstants;

/**
 * Entity for welcome call task log
 * 
 * @author Arun Kumar
 */
@Entity
@Table(name = "welcome_call_task_log")
@NamedNativeQueries({ 
	@NamedNativeQuery(
		name = RightnowConstants.NAMED_QUERIES.WELCOME_CALL_LOG_BY_RELEASE, 
		query = "select "
			+ "release_id , "
			+ "task_id  "
			+ "from welcome_call_task_log where "
			+ "release_id=:orderReleaseId", 
		resultSetMapping = RightnowConstants.SQL_RESULT_SET_MAP.MAP_WELCOME_CALL_LOG_BY_RELEASE
	)
})

@NamedQueries(value = {
	@NamedQuery(
		name = WelcomeCallTaskLogEntity.TOTAL_WELCOME_CALL_TASK_BY_CREATE_DATE, 
		query = "select " +
			"count(*) as totalTask " +
			"from WelcomeCallTaskLogEntity " +
			"where createdOn between :startDate and :endDate"		
	)
})


@SqlResultSetMappings({ 
	@SqlResultSetMapping(
		name = RightnowConstants.SQL_RESULT_SET_MAP.MAP_WELCOME_CALL_LOG_BY_RELEASE, 
		columns = {
		@ColumnResult(name = "release_id"), @ColumnResult(name = "task_id") 
		}
	) 
})

public class WelcomeCallTaskLogEntity extends BaseEntity {

	private static final long serialVersionUID = 3688398701763484215L;
	
	public static final String TOTAL_WELCOME_CALL_TASK_BY_CREATE_DATE = "WelcomeCallTaskLogEntity.totalTask";
	
	@Column(name = "release_id")
	private Long releaseId;

	@Column(name = "task_id")
	private Long taskId;	
	
	@Column(name = "order_id")
	private Long orderId;

	public WelcomeCallTaskLogEntity() {}

	public WelcomeCallTaskLogEntity(Long releaseId, Long taskId, Long orderId) {
		this.releaseId = releaseId;
		this.taskId = taskId;
		this.orderId = orderId;
	}

	public Long getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(Long releaseId) {
		this.releaseId = releaseId;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	@Override
	public String toString() {
		return "WelcomeCallTaskLogEntity [releaseId=" + releaseId + ", taskId=" + taskId + ", orderId=" + orderId + "]";
	}
	
}