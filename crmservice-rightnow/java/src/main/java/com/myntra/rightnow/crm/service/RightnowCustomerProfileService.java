package com.myntra.rightnow.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.portal.crm.client.entry.CustomerProfileEntry;
import com.myntra.portal.crm.client.response.CustomerProfileResponse;

/**
 * Customer order search web service interface(abstract) which integrates detail
 * of order, shipments, skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
@Path("/customerprofile/")
public interface RightnowCustomerProfileService extends BaseService<CustomerProfileResponse, CustomerProfileEntry>{

    @GET
    @Consumes({"application/xml", "application/json"})
    @Produces({"application/xml", "application/json"})
    @Path("/")
    AbstractResponse getCustomerProfileByLogin(
            @QueryParam("login") String login) throws ERPServiceException;
    
    
    @GET
    @Consumes({"application/xml", "application/json"})
    @Produces({"application/xml", "application/json"})
    @Path("/RN/createcontact/")
    AbstractResponse createCustomerProfileInRightnow(
            @QueryParam("email") String email) throws ERPServiceException;
    
}

