/**
 * 
 */
package com.myntra.rightnow.crm.manager;

import java.util.List;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.rightnow.crm.client.entry.RightnowBaseTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskEntry;
import com.myntra.rightnow.crm.client.response.RightnowBaseTaskResponse;
import com.myntra.rightnow.crm.client.response.RightnowTaskResponse;

/**
 * @author preetam
 * 
 */
public interface RightnowProactiveTaskManager extends BaseManager<RightnowTaskResponse, RightnowTaskEntry> {
	
	//create all types of proactive task
	public RightnowBaseTaskResponse createTasks(List<RightnowBaseTaskEntry> baseTaskList) throws ERPServiceException;	
}
