package com.myntra.rightnow.crm.dao;

import java.util.Date;

import com.myntra.commons.dao.BaseDAO;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.rightnow.crm.entity.WelcomeCallTaskLogEntity;

/**
 * DAO for welcome call task log
 * 
 * @author Arun Kumar
 */
public interface WelcomeCallTaskLogDAO extends BaseDAO<WelcomeCallTaskLogEntity> {
	
	public WelcomeCallTaskLogEntity getWelcomeCallTaskLog(Long orderReleaseId) throws ERPServiceException;
	public Integer getTotalWelcomeCallTask(Date startDate, Date endDate) throws ERPServiceException;
	
}
