package com.myntra.rightnow.crm.utils;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import com.myntra.erp.crm.client.CustomerOrderClient;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderShipmentEntry;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;

public class GetOrderDetails implements Callable<Map<Long, CustomerOrderEntry>> {
	
	private static final Logger LOGGER = Logger.getLogger(GetOrderDetails.class);
	
	public static final String ORDER = "order";
	public static final String RELEASE = "release";
	
	private String orderIds;
	private String type;
	
	public GetOrderDetails(String orderIds, String type) {
		this.orderIds = orderIds;
		this.type = type;
	}

	@Override
	public Map<Long, CustomerOrderEntry> call() throws Exception {
		LOGGER.debug(Thread.currentThread().getName() + " Started for fetching " + type + " details");
		Map<Long, CustomerOrderEntry> orderMap = new HashMap<Long, CustomerOrderEntry>();

		Calendar c1 = Calendar.getInstance();
		long t1 = c1.getTimeInMillis();
		
		String field = "id.in:";
		if(type.equalsIgnoreCase(RELEASE)) {	
			field = "id.eq:";
		} 
		CustomerOrderResponse response = CustomerOrderClient.getCustomerOrderDetails(null, 0, 100, 
				null, null, field + orderIds, true, false, false, false);	
		
		Calendar c2 = Calendar.getInstance();
		long t2 = c2.getTimeInMillis();

		LOGGER.debug("Time Taken by Thread " + Thread.currentThread().getName() + ": " + ((t2-t1)/1000) + " seconds");
		
		if(response != null && response.getCustomerOrderEntryList() != null && response.getCustomerOrderEntryList().size() > 0) {
			for(CustomerOrderEntry orderEntry : response.getCustomerOrderEntryList()) {
				if(type.equalsIgnoreCase(RELEASE)) {
					for(CustomerOrderShipmentEntry shipmentEntry : orderEntry.getOrderShipments()) {
						if(shipmentEntry.getShipmentId() == Long.parseLong(orderIds)) {
							orderMap.put(shipmentEntry.getShipmentId(), orderEntry);
							break;
						}
					}
					
				} else {
					orderMap.put(orderEntry.getOrderId(), orderEntry);
				}
			}
		}
		LOGGER.debug(Thread.currentThread().getName() + " Finished for fetching " + type + " details");
		return orderMap;
	}

}
