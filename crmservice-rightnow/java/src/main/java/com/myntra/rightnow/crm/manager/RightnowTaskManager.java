package com.myntra.rightnow.crm.manager;

import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.springframework.transaction.annotation.Transactional;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.rightnow.crm.client.entry.RightnowFileAttachmentEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskIdEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskListEntry;
import com.myntra.rightnow.crm.client.response.RightnowTaskResponse;

/**
 * Manager interface to create, update & delete Rightnow task
 * 
 */
public interface RightnowTaskManager extends BaseManager<RightnowTaskResponse, RightnowTaskEntry> {

	public RightnowTaskResponse updateTasks(RightnowTaskListEntry taskList) throws ERPServiceException;

	public RightnowTaskResponse getTaskNotes(Long taskId) throws ERPServiceException;

	public RightnowTaskResponse getListOfTaskNotes(RightnowTaskListEntry taskList) throws ERPServiceException;

	public RightnowTaskResponse getTaskAttachments(Long taskId) throws ERPServiceException;

	public RightnowTaskResponse getOriginalAttachment(RightnowFileAttachmentEntry fileEntry) throws ERPServiceException;

	public RightnowTaskResponse getTaskDetails(Long taskId) throws ERPServiceException;

	public RightnowTaskResponse getRightnowTaskNotesFromReport(RightnowTaskIdEntry taskIDEntry)
			throws ERPServiceException;
	
	//close one or more tasks and incidents associated with it
	public RightnowTaskResponse closeTasks(RightnowTaskIdEntry taskIDEntry) throws ERPServiceException;
	
	//upload attachment to a task
	public RightnowTaskResponse uploadAttachment(MultipartBody body) throws ERPServiceException;
	
	//fix tasks with missing categories
	public RightnowTaskResponse fixTasksWithNoCategories() throws ERPServiceException;
	
	@Transactional(readOnly = true)
	public RightnowTaskResponse createWelcomeCallTask(OrderEntry lmsOrderEntry) throws ERPServiceException;
}