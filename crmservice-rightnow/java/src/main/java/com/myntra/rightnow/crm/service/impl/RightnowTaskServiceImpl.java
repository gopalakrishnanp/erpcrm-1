package com.myntra.rightnow.crm.service.impl;

import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.lms.client.response.OrderEntry;
import com.myntra.rightnow.crm.client.entry.RightnowFileAttachmentEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskIdEntry;
import com.myntra.rightnow.crm.client.response.RightnowBaseTaskResponse;
import com.myntra.rightnow.crm.client.response.RightnowTaskResponse;
import com.myntra.rightnow.crm.manager.RightnowProactiveTaskManager;
import com.myntra.rightnow.crm.manager.RightnowTaskManager;
import com.myntra.rightnow.crm.service.RightnowTaskService;

public class RightnowTaskServiceImpl extends BaseServiceImpl<RightnowTaskResponse, RightnowTaskEntry> implements
		RightnowTaskService {

	public RightnowProactiveTaskManager proactiveTaskManager;

	public void setProactiveTaskManager(RightnowProactiveTaskManager proactiveTaskManager) {
		this.proactiveTaskManager = proactiveTaskManager;
	}

	public RightnowProactiveTaskManager getProactiveTaskManager() {
		return proactiveTaskManager;
	}

	@Override
	public AbstractResponse updateTasks(RightnowTaskResponse taskResponse) throws ERPServiceException {
		try {

			return ((RightnowTaskManager) getManager()).updateTasks(taskResponse.getRightnowTaskListEntry());

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}

	@Override
	public AbstractResponse getTaskNotes(Long taskId) throws ERPServiceException {
		try {
			return ((RightnowTaskManager) getManager()).getTaskNotes(taskId);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}

	@Override
	public AbstractResponse getListOfTaskNotes(RightnowTaskResponse taskResponse) throws ERPServiceException {
		try {
			return ((RightnowTaskManager) getManager()).getListOfTaskNotes(taskResponse.getRightnowTaskListEntry());

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}

	@Override
	public AbstractResponse getTaskAttachments(Long taskId) throws ERPServiceException {
		try {
			return ((RightnowTaskManager) getManager()).getTaskAttachments(taskId);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}

	@Override
	public AbstractResponse getOriginalAttachment(RightnowFileAttachmentEntry fileEntry) throws ERPServiceException {
		try {
			return ((RightnowTaskManager) getManager()).getOriginalAttachment(fileEntry);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}

	@Override
	public AbstractResponse getTaskDetails(Long taskId) throws ERPServiceException {
		try {
			return ((RightnowTaskManager) getManager()).getTaskDetails(taskId);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}

	@Override
	public AbstractResponse getRightnowTaskNotesFromReport(RightnowTaskIdEntry taskIDEntry) throws ERPServiceException {
		try {
			return ((RightnowTaskManager) getManager()).getRightnowTaskNotesFromReport(taskIDEntry);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}

	@Override
	public AbstractResponse createTasks(RightnowBaseTaskResponse baseTaskResponse) throws ERPServiceException {
		try {
			return ((RightnowProactiveTaskManager) getProactiveTaskManager()).createTasks(baseTaskResponse
					.getRightnowBaseTaskEntryList());

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}
	
	@Override
	public AbstractResponse uploadAttachment(MultipartBody body) throws ERPServiceException {
		try {
			return ((RightnowTaskManager) getManager()).uploadAttachment(body);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}
	
	@Override
	public AbstractResponse closeTasks(RightnowTaskIdEntry taskIDEntry) throws ERPServiceException {
		try {
			return ((RightnowTaskManager) getManager()).closeTasks(taskIDEntry);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}
	
	@Override
	public AbstractResponse fixTasksWithNoCategories() throws ERPServiceException {
		try {
			return ((RightnowTaskManager) getManager()).fixTasksWithNoCategories();

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}
	
	@Override
	public AbstractResponse createWelcomeCallTask(OrderEntry lmsOrderEntry) throws ERPServiceException {
		try {
			return ((RightnowTaskManager) getManager()).createWelcomeCallTask(lmsOrderEntry);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}
}
