package com.myntra.drishti.crm.manager;

import java.util.List;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.drishti.crm.client.entry.DrishtiShipmentEntry;
import com.myntra.drishti.crm.client.response.DrishtiOrderResponse;
import com.myntra.erp.crm.client.entry.CustomerOrderShipmentEntry;

public interface DrishtiOrderManager extends BaseManager<DrishtiOrderResponse, DrishtiShipmentEntry> {

	public DrishtiOrderResponse getOrderDetailsForDrishti(long orderid) throws ERPServiceException;
	
	public DrishtiOrderResponse getLastNopenOrdersForDrishti(String userContact, int fetchSize) throws ERPServiceException;

	public void getDrishtiShipment(List<CustomerOrderShipmentEntry> customerOrderShipments,
			List<DrishtiShipmentEntry> drishtiShipmentEntryList);
}