package com.myntra.drishti.crm.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.drishti.crm.client.entry.DrishtiOrderEntry;
import com.myntra.drishti.crm.client.entry.DrishtiShipmentEntry;
import com.myntra.drishti.crm.client.response.DrishtiIvrOrderTestResponse;
import com.myntra.drishti.crm.constants.DrishtiContants;
import com.myntra.drishti.crm.manager.DrishtiIvrOrderTestManager;
import com.myntra.drishti.crm.manager.DrishtiOrderManager;
import com.myntra.erp.crm.client.CustomerOrderClient;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderShipmentEntry;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;

public class DrishtiIvrOrderTestManagerImpl extends
		BaseManagerImpl<DrishtiIvrOrderTestResponse, DrishtiOrderEntry> implements DrishtiIvrOrderTestManager {

	private static final Logger LOGGER = Logger.getLogger(CustomerOrderClient.class);
	private DrishtiOrderManager drishtiOrderManager;

	public DrishtiOrderManager getDrishtiOrderManager() {
		return drishtiOrderManager;
	}

	public void setDrishtiOrderManager(DrishtiOrderManager drishtiOrderManager) {
		this.drishtiOrderManager = drishtiOrderManager;
	} 

	@Override
	public DrishtiIvrOrderTestResponse getIvrOrderTestDetailsForDrishti(int fetchSize, String date) throws ERPServiceException {

		// limit max records to 5
		if (fetchSize > 5) {
			fetchSize = 5;
		}
		
		// suffix the date with time to fulfil the filter format
		if(date != null){
			date = date.concat(" 12:00:00");
		}

		String sortBy = "createdOn";
		String sortOrder = "DESC";
		String searchTerms = "";

		DrishtiIvrOrderTestResponse drishtiResponse = new DrishtiIvrOrderTestResponse();
		List<DrishtiOrderEntry> drishtiEntryList = new ArrayList<DrishtiOrderEntry>();

		for (DrishtiContants.ORDER_STATUS orderStatus : DrishtiContants.ORDER_STATUS.values()) {
			// TODO to handle 'OH' later, since is throwing exception by OMS service
			if(orderStatus.name().equals("OH")){
				continue;
			}
			
			// build search term
			searchTerms = "releaseStatus.eq:" + orderStatus.name();
			if(date != null){
				searchTerms = searchTerms.concat("___createdOn.lt:"+date);						
			}

			// Call CRM order service to get order response
			CustomerOrderResponse orderResponse = CustomerOrderClient.getCustomerOrderDetails(null, 0, fetchSize,
					sortBy, sortOrder, searchTerms, false, false, true, false);

			if (orderResponse != null && orderResponse.getStatus().getStatusType().equals("SUCCESS")) {
				List<CustomerOrderEntry> orderEntryList = orderResponse.getCustomerOrderEntryList();

				// set the right total count for drishti response
				long countInResponse = 0L;
				if(drishtiResponse.getStatus() != null){
					countInResponse = drishtiResponse.getStatus().getTotalCount();
				}
				drishtiResponse.setStatus(orderResponse.getStatus());
				drishtiResponse.getStatus()
						.setTotalCount(drishtiResponse.getStatus().getTotalCount() + countInResponse);

				if (orderEntryList != null && orderEntryList.size() > 0) {
					for (CustomerOrderEntry orderEntry : orderEntryList) {
						DrishtiOrderEntry singleIvrOrderEntry = new DrishtiOrderEntry();

						List<CustomerOrderShipmentEntry> orderShipments = orderEntry.getOrderShipments();
						List<DrishtiShipmentEntry> drishtiShipments = new ArrayList<DrishtiShipmentEntry>();

						// get all shipment detail
						drishtiOrderManager.getDrishtiShipment(orderShipments, drishtiShipments);					
						
						singleIvrOrderEntry.setOrderId(orderEntry.getOrderId());
						singleIvrOrderEntry.setDrishtiShipments(drishtiShipments);
						drishtiEntryList.add(singleIvrOrderEntry);
					}
				}
			}
		}

		drishtiResponse.setDrishtiOrderEntryList(drishtiEntryList);
		return drishtiResponse;
	}

	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse create(DrishtiOrderEntry entry) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(DrishtiOrderEntry entry, Long itemId) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}
