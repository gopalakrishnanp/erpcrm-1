package com.myntra.drishti.crm.manager.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.util.StringUtils;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.test.TestHelper;
import com.myntra.commons.utils.Context;
import com.myntra.commons.utils.ServiceURLProperty;
import com.myntra.drishti.crm.client.entry.DrishtiShipmentEntry;
import com.myntra.drishti.crm.client.response.DrishtiOrderResponse;
import com.myntra.drishti.crm.constants.DrishtiContants;
import com.myntra.drishti.crm.manager.DrishtiOrderManager;
import com.myntra.erp.crm.client.CustomerOrderClient;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderShipmentEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderTrackingDetailEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderTripAssignmentEntry;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;
import com.myntra.lms.client.status.TripStatus;
import com.myntra.loyaltyPointsService.client.LoyaltyPointsManagerServiceClient;
import com.myntra.loyaltyPointsService.domain.response.LoyaltyPointsServiceMyMyntraAccountInfoResponse;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.oms.client.response.OrderReleaseResponse;
import com.myntra.portal.crm.client.ProfileClient;
import com.myntra.portal.crm.client.response.CustomerProfileResponse;
import com.myntra.rightnow.crm.client.code.DrishtiErrorCodes;
import com.myntra.rightnow.crm.client.code.DrishtiSuccessCodes;

public class DrishtiOrderManagerImpl extends BaseManagerImpl<DrishtiOrderResponse, DrishtiShipmentEntry> implements
		DrishtiOrderManager {
	
	private static final String[] OPEN_ORDER_STATUSES= new String[] {"Q","WP","RFR","PK","OH","SH"};

	@Override
	public DrishtiOrderResponse getOrderDetailsForDrishti(long orderid) throws ERPServiceException {
		
		String orderidString = Long.valueOf(orderid).toString();
		// Call OMS service to get order response
		CustomerOrderResponse orderResponse = CustomerOrderClient.getCustomerOrderDetails(null, 0, 1, "createdOn", "DESC", "id.eq:"+orderidString,false, false, true, false);
		
		DrishtiOrderResponse drishtiResponse = new DrishtiOrderResponse();
		drishtiResponse.setStatus(orderResponse.getStatus());
		
		if(orderResponse.getStatus().getStatusType().equals("SUCCESS")) {
			List<CustomerOrderEntry> customerOrderEntryList = orderResponse.getCustomerOrderEntryList();
			
			if(customerOrderEntryList.size()>0) {				
				CustomerOrderEntry orderEntry = customerOrderEntryList.get(0);
				List<CustomerOrderShipmentEntry> customerOrderShipments = orderEntry.getOrderShipments();				
				drishtiResponse.setNumberOfShipments(customerOrderShipments.size());				
				List<DrishtiShipmentEntry> drishtiShipmentEntryList = new ArrayList<DrishtiShipmentEntry>();

				// add shipment detail
				getDrishtiShipment(customerOrderShipments, drishtiShipmentEntryList);				
				drishtiResponse.setDrishtiOrderShipments(drishtiShipmentEntryList);
			}
		}
		
		return drishtiResponse;
	}
	
	
	@Override
	public DrishtiOrderResponse getLastNopenOrdersForDrishti(String userContactNo, int fetchSize) throws ERPServiceException {
		DrishtiOrderResponse drishtiResponse = new DrishtiOrderResponse();
		
		if(userContactNo==null || userContactNo.length()!=10) {
			// Throw error response saying wrong contact number.
			StatusResponse error = new StatusResponse(DrishtiErrorCodes.WRONG_INPUT_PARAMETERS,
					StatusResponse.Type.ERROR);
			drishtiResponse.setStatus(error);
			return drishtiResponse;
		}
		
		for(int i=0;i<userContactNo.length();i++) {
			if(!Character.isDigit(userContactNo.charAt(i))) {
				// Throw error response saying wrong contact number.
				StatusResponse error = new StatusResponse(DrishtiErrorCodes.WRONG_INPUT_PARAMETERS,
						StatusResponse.Type.ERROR);
				drishtiResponse.setStatus(error);
				return drishtiResponse;
			}
		}

		drishtiResponse.setCustomerLoyaltyType("UNREGISTERED");
		
		CustomerProfileResponse customerProfile = ProfileClient.findByMobile(null, userContactNo);
		
		if(customerProfile!=null && customerProfile.getStatus().getStatusType().equals("SUCCESS")
				&& customerProfile.getCustomerProfileEntry()!=null) {
			String login = customerProfile.getCustomerProfileEntry().getLogin();
			
			int tier = 0;
			LoyaltyPointsServiceMyMyntraAccountInfoResponse loyaltyResponse 
			= LoyaltyPointsManagerServiceClient.getAccountInfoForMyMyntra(null, login, TestHelper.dummyContextInfo1);
			if(loyaltyResponse!=null && loyaltyResponse.getStatus().getStatusType().equals("SUCCESS")) {
				tier = loyaltyResponse.getTierInfo().getPresentTierNumber();
			}
			
			if(tier==3)
				drishtiResponse.setCustomerLoyaltyType("PLATINUM");
			else if(tier==2)
				drishtiResponse.setCustomerLoyaltyType("GOLD");
			else if(tier==1)
				drishtiResponse.setCustomerLoyaltyType("SILVER");
		}
		

		String searchTerms = "userContactNo.eq:"+userContactNo+"___releaseStatus.in:" + StringUtils.arrayToCommaDelimitedString(OPEN_ORDER_STATUSES);

		// TODO move this to OMS Client codebase.
		BaseWebClient client = new BaseWebClient(null, ServiceURLProperty.OMS_URL, Context.getContextInfo());
		client.path("/oms/orderrelease/search");
		client.query("q", searchTerms);
		client.query("fetchSize", fetchSize);
		client.query("sortBy", "createdOn");
		client.query("sortOrder", "DESC");

		OrderReleaseResponse releaseResponse = client.get(OrderReleaseResponse.class);
		List<OrderReleaseEntry> orderReleaseEntryList = releaseResponse.getData();
		
		List<Long> shipmentIDList= new ArrayList<Long>();
		List<DrishtiShipmentEntry> drishtiShipmentEntryList = new ArrayList<DrishtiShipmentEntry>();

		if(releaseResponse.getStatus().getStatusType().equals("SUCCESS")
				&& releaseResponse.getData().size()>0) {
			
			for (OrderReleaseEntry orderShipment : orderReleaseEntryList) {
				shipmentIDList.add(orderShipment.getId());
			}
			
			for (OrderReleaseEntry orderShipment : orderReleaseEntryList) {
				
				if(!shipmentIDList.contains(orderShipment.getId()))
					continue;
				
				if(orderShipment.getCourierCode().equals("ML") 
						&& orderShipment.getStatus().equals("SH")) {
					
					// For ML we need a lot of details, like logistics details.
					String orderidString = Long.valueOf(orderShipment.getId()).toString();
					
					// Call OMS service to get order response
					CustomerOrderResponse orderResponse = CustomerOrderClient.getCustomerOrderDetails(
							null, 0, 1, "createdOn", "DESC", "id.eq:"+orderidString,false, false, true, false);
					
					if(orderResponse.getStatus().getStatusType().equals("SUCCESS")) {
						List<CustomerOrderEntry> customerOrderEntryList = orderResponse.getCustomerOrderEntryList();
						
						if(customerOrderEntryList.size()>0) {
							CustomerOrderEntry orderEntry = customerOrderEntryList.get(0);
							List<CustomerOrderShipmentEntry> customerOrderShipments = orderEntry.getOrderShipments();
							
							for(Iterator<CustomerOrderShipmentEntry> it = customerOrderShipments.iterator(); it.hasNext();) {
								CustomerOrderShipmentEntry shipmentEntry = it.next();
								if(!shipmentIDList.contains(shipmentEntry.getShipmentId())) {
									// Remove other shipments from the order, that we should not be looking into.
									it.remove();
								}
						
								if(shipmentIDList.contains(shipmentEntry.getShipmentId()))
									shipmentIDList.remove(shipmentEntry.getShipmentId());
							}
							
							// Add Shipment Detail
							getDrishtiShipment(customerOrderShipments, drishtiShipmentEntryList);
						}
					}
					
				} else {
					
					if(shipmentIDList.contains(orderShipment.getId()))
						shipmentIDList.remove(orderShipment.getId());
					
					// Populate customerOrderShipmentEntry object needed for getDrishtiShipment function.
					CustomerOrderShipmentEntry shipmentEntry = new CustomerOrderShipmentEntry();
					shipmentEntry.setShipmentId(orderShipment.getId());
					shipmentEntry.setStatus(orderShipment.getStatus());
					shipmentEntry.setStatusDisplayName(orderShipment.getStatusDisplayName());
					shipmentEntry.setCourierCode(orderShipment.getCourierCode());
					
					// Set all dates.
					shipmentEntry.setCreatedOn(orderShipment.getCreatedOn());
					shipmentEntry.setPromisedDate(orderShipment.getCustomerPromiseTime());
					
					shipmentEntry.setPackedOn(orderShipment.getPackedOn());
					shipmentEntry.setShippedOn(orderShipment.getShippedOn());
					shipmentEntry.setDeliveredOn(orderShipment.getDeliveredOn());
					shipmentEntry.setPromisedDate(orderShipment.getExpectedDeliveryPromise());
					
					List<CustomerOrderShipmentEntry> customerOrderShipments = new ArrayList<CustomerOrderShipmentEntry>();
					customerOrderShipments.add(shipmentEntry);
					
					getDrishtiShipment(customerOrderShipments, drishtiShipmentEntryList);
				}
			}
		}

		drishtiResponse.setDrishtiOrderShipments(drishtiShipmentEntryList);
		drishtiResponse.setNumberOfShipments(drishtiShipmentEntryList.size());
		
		StatusResponse success = new StatusResponse(DrishtiSuccessCodes.DRISHTI_ORDERS_RETRIEVED,
				StatusResponse.Type.SUCCESS, drishtiShipmentEntryList.size());
		drishtiResponse.setStatus(success);
		
		return drishtiResponse;
	}

	/**
	 * @param customerOrderShipments
	 * @param drishtiShipmentEntryList
	 */
	@Override
	public void getDrishtiShipment(List<CustomerOrderShipmentEntry> customerOrderShipments,
			List<DrishtiShipmentEntry> drishtiShipmentEntryList) {
		if(customerOrderShipments != null && drishtiShipmentEntryList != null){
			for (CustomerOrderShipmentEntry orderShipmentEntry : customerOrderShipments) {
				
				DrishtiShipmentEntry drishtiShipmentEntry = new DrishtiShipmentEntry();
				drishtiShipmentEntry.setShipmentId(orderShipmentEntry.getShipmentId());
				drishtiShipmentEntry.setOMSStatus(orderShipmentEntry.getStatus());				
				drishtiShipmentEntry.setOMSStatusDisplayName(orderShipmentEntry.getStatusDisplayName());
				
				// switch based on enum value of order status
				switch (DrishtiContants.ORDER_STATUS.valueOf(orderShipmentEntry.getStatus())) {			
				case Q:
					drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.BEING_PROCESSED);
					drishtiShipmentEntry.setDate(orderShipmentEntry.getPromisedDate());					
					drishtiShipmentEntry.setRemark("Promised Date");
					break;				
				case WP:
					drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.BEING_PROCESSED);
					drishtiShipmentEntry.setDate(orderShipmentEntry.getPromisedDate());
					drishtiShipmentEntry.setRemark("Promised Date");
					break;				
				case RFR:
					drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.BEING_PROCESSED);
					drishtiShipmentEntry.setDate(orderShipmentEntry.getPromisedDate());
					drishtiShipmentEntry.setRemark("Promised Date");
					break;				
				case PK:
					drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.BEING_PROCESSED);
					drishtiShipmentEntry.setDate(orderShipmentEntry.getPromisedDate());
					drishtiShipmentEntry.setRemark("Promised Date");
					break;				
				case OH:
					drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.BEING_PROCESSED);
					drishtiShipmentEntry.setDate(orderShipmentEntry.getPromisedDate());
					drishtiShipmentEntry.setRemark("Promised Date");
					break;			
				case D:
					drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.CANCELLED);
					break;				
				case F:
					drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.CANCELLED);
					break;
				case SH:
					if(!orderShipmentEntry.getCourierCode().equals("ML")) {
						// For non ML couriers, mark status as 'Shipped' and read promised date to customer.
						drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.SHIPPED);
						drishtiShipmentEntry.setDate(orderShipmentEntry.getPromisedDate());
						drishtiShipmentEntry.setRemark("Promised Date");
						
					} else {
						// For ML orders, we need to check 3 cases.
						// 1. Order marked as Delivered in shipment tracker
						// 2. Order marked as Out For delivery in shipment tracker
						// 3. Remaining cases, we need to tell promised date to customer.
						
						List<CustomerOrderTrackingDetailEntry> orderTrackingDetails = orderShipmentEntry.getOrderTrackingDetailEntry();
						
						if(orderTrackingDetails!=null && orderTrackingDetails.size()>0) {
							
							CustomerOrderTrackingDetailEntry orderTrackingDetail = orderTrackingDetails.get(0);
							List<CustomerOrderTripAssignmentEntry> orderTripAssignment = orderShipmentEntry.getOrderTripAssignmentEntry();
							
							boolean statusUpdated = false;
							
							if(statusUpdated || orderTrackingDetail.getActivityType().equals("DL")) {
								drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.DELIVERED);
								drishtiShipmentEntry.setDate(orderTrackingDetails.get(0).getActionDate());
								drishtiShipmentEntry.setRemark("Delivered Date");
								statusUpdated = true;
							} 
							
							if (statusUpdated==false && 
									orderTripAssignment!=null && orderTripAssignment.size()>0
									&& orderTripAssignment.get(0).getCorrespondingTripStatus()!=null
									&& orderTripAssignment.get(0).getCorrespondingTripStatus().equals(TripStatus.OUT_FOR_DELIVERY)) {
								
								drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.OUT_FOR_DELIVERY);
								statusUpdated=true;
							} 

							if (statusUpdated==false && orderTrackingDetails!=null && orderTrackingDetails.size()>0) {
								
								for(int i=0; i<orderTrackingDetails.size(); i++) {
									if(orderTrackingDetails.get(i).getActivityType().equals("RTO")) {
										drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.REDIRECT_CC);
										statusUpdated = true;
										break;
									}
								}
							} 
							
							if(!statusUpdated) {
								drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.SHIPPED);
								drishtiShipmentEntry.setDate(orderShipmentEntry.getPromisedDate());
								drishtiShipmentEntry.setRemark("Promised Date");
							}
							
						} else {
							drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.SHIPPED);
							drishtiShipmentEntry.setDate(orderShipmentEntry.getPromisedDate());
							drishtiShipmentEntry.setRemark("Promised Date");
						}
					}
					break;
					
				case C:
					drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.DELIVERED);
					drishtiShipmentEntry.setDate(orderShipmentEntry.getDeliveredOn());
					drishtiShipmentEntry.setRemark("Delivered Date");
					break;				
				case DL:
					drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.DELIVERED);
					drishtiShipmentEntry.setDate(orderShipmentEntry.getDeliveredOn());
					drishtiShipmentEntry.setRemark("Delivered Date");
					break;				
				default:
					drishtiShipmentEntry.setDrishtiOrderStatus(DrishtiContants.REDIRECT_CC);
					break;
				}
				drishtiShipmentEntryList.add(drishtiShipmentEntry);
			}	
		}
	}		

	@Override
	public AbstractResponse create(DrishtiShipmentEntry entry) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(DrishtiShipmentEntry entry, Long itemId) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}
