package com.myntra.drishti.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.drishti.crm.client.entry.DrishtiShipmentEntry;
import com.myntra.drishti.crm.client.response.DrishtiOrderResponse;

@Path("/drishti/")
public interface DrishtiOrderService extends BaseService<DrishtiOrderResponse, DrishtiShipmentEntry> {
	
	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("/order/")
	AbstractResponse getOrderDetailsForDrishti(@QueryParam("orderid") long orderid)
			throws ERPServiceException;
	
	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("/lastopenshipments/")
	AbstractResponse getLastNopenOrdersForDrishti(@QueryParam("userContactNo") String userContactNo, @DefaultValue("3") @QueryParam("fetchSize") int fetchSize)
			throws ERPServiceException;


}
