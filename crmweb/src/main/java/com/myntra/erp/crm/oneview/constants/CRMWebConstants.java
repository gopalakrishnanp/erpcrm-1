package com.myntra.erp.crm.oneview.constants;

public final class CRMWebConstants {
    
    public static final String CRM_ERP_URL_KEY = "crmErpURL";
    public static final String CRM_PORTAL_URL_KEY = "crmPortalURL";
}
