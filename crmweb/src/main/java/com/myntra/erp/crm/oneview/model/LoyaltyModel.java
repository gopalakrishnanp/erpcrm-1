package com.myntra.erp.crm.oneview.model;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.test.TestHelper;
import com.myntra.loyaltyPointsService.client.LoyaltyPointsManagerServiceClient;
import com.myntra.loyaltyPointsService.domain.response.LoyaltyPointsServiceMyMyntraAccountInfoResponse;
import com.myntra.loyaltyPointsService.domain.response.entry.LoyaltyPointsTierInfo;
import com.myntra.loyaltyPointsService.domain.response.entry.LoyaltyPointsTierInfoEntry;
import com.myntra.loyaltyPointsService.domain.response.entry.LoyaltyPointsTransactionLogEntry;
import com.myntra.loyaltyPointsService.domain.response.entry.LoyaltyPointsUserAccountEntry;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Arun
 */
public class LoyaltyModel implements Serializable {

    private LoyaltyPointsUserAccountEntry account;
    private LoyaltyPointsTierInfoEntry tier;
    private List<LoyaltyPointsTransactionLogEntry> transactionList;
    private Float last6MonthsPurchaseValue;

    public Float getLast6MonthsPurchaseValue() {
        return last6MonthsPurchaseValue;
    }

    public LoyaltyPointsUserAccountEntry getAccount() {
        return account;
    }

    public void setAccount(LoyaltyPointsUserAccountEntry account) {
        this.account = account;
    }

    public LoyaltyPointsTierInfoEntry getTier() {
        return tier;
    }

    public void setTier(LoyaltyPointsTierInfoEntry tier) {
        this.tier = tier;
    }

    public List<LoyaltyPointsTransactionLogEntry> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<LoyaltyPointsTransactionLogEntry> transactionList) {
        this.transactionList = transactionList;
    }

    public void setData(LoyaltyPointsServiceMyMyntraAccountInfoResponse response) {
        this.account = response.getUserAccountInfo();
        this.tier = response.getTierInfo();
        this.transactionList = response.getLoyaltyPointsTransactionHistoryConsolidated();
        setLast6MonthsPurchaseValue();
    }

    public void fetchData(String login) throws ERPServiceException {
        LoyaltyPointsServiceMyMyntraAccountInfoResponse response = LoyaltyPointsManagerServiceClient.getAccountInfoForMyMyntra(null, login, TestHelper.dummyContextInfo1);
        this.setData(response);
    }
    
    public String getTierTitle(Integer tier){
        
        if(tier == 1){
            return "Silver";
        }else if(tier == 2){
            return "Gold (Rs. 3000+)";
        }else if(tier == 3) {
            return "Platinum (Rs. 8000+)";
        }else {
            return "None";
        }
    }
    
    public void setLast6MonthsPurchaseValue(){
        for(LoyaltyPointsTierInfo tierSlabInfo: tier.getTierSlabsInfo()){
            if(tierSlabInfo.getTierNumber() == 1){
                /* tier min purchase value = (tier grant value - total last 6 months purchase value)
                 * in this case it will be (0-last6MonthsPurchaseValue) which is always -ve
                 * to get +ve value we muliply by -2 with last6MonthsPurchaseValue and adds that to tier min purchase value
                 */
                this.last6MonthsPurchaseValue = tierSlabInfo.getMinPurchaseValue()+(tierSlabInfo.getMinPurchaseValue()*-2);
                break;
            }
        }
    }
}
