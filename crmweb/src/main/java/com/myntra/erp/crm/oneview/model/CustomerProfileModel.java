package com.myntra.erp.crm.oneview.model;

import com.myntra.portal.crm.client.entry.CustomerProfileEntry;
import java.io.Serializable;

/**
 *
 * @author pravin
 */
public class CustomerProfileModel implements Serializable {

    private CustomerProfileEntry profileEntry;

    public CustomerProfileEntry getProfileEntry() {
        return profileEntry;
    }

    public void setProfileEntry(CustomerProfileEntry profileEntry) {
        this.profileEntry = profileEntry;
    }
    
    public void setCustomerProfileModel(CustomerProfileEntry profileEntry) {
        setProfileEntry(profileEntry);
    }

    public String getMobile() {
        if(profileEntry!=null)
            return profileEntry.getMobile();
        
        return null;
    }
}
