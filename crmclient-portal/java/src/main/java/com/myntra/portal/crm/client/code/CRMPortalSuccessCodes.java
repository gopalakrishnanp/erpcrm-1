package com.myntra.portal.crm.client.code;

import com.myntra.commons.codes.ERPSuccessCodes;
import com.myntra.commons.codes.StatusCodes;

/**
 * List of success codes
 * 
 * @author Arun Kumar
 * 
 */
public class CRMPortalSuccessCodes extends ERPSuccessCodes {
	
	public static final StatusCodes CUSTOMER_RETURN_RETRIEVED = new CRMPortalSuccessCodes(703, "CUSTOMER_RETURN_RETRIEVED");
	public static final StatusCodes COUPON_RETRIEVED = new CRMPortalSuccessCodes(704, "COUPON_RETRIEVED");	
	public static final StatusCodes CUSTOMER_PROFILE_RETRIEVED = new CRMPortalSuccessCodes(706, "CUSTOMER_PROFILE_RETRIEVED");
	public static final StatusCodes PAYMENT_LOG_RETRIEVED = new CRMPortalSuccessCodes(708, "PAYMENT_LOG_RETRIEVED");
	public static final StatusCodes ORDER_COMMENT_LOG_RETRIEVED = new CRMPortalSuccessCodes(709, "ORDER_COMMENT_LOG_RETRIEVED");
	public static final StatusCodes COD_ORDER_OH_REASON_LOG_RETRIEVED = new CRMPortalSuccessCodes(710, "COD_ORDER_OH_REASON_LOG_RETRIEVED");
	public static final StatusCodes SERVICE_REQUEST_RETRIEVED = new CRMPortalSuccessCodes(711, "SERVICE_REQUEST_RETRIEVED");
	public static final StatusCodes EXCHANGE_DETAIL_RETRIEVED = new CRMPortalSuccessCodes(712, "EXCHANGE_DETAIL_RETRIEVED");
	public static final StatusCodes RTO_RETRIEVED = new CRMPortalSuccessCodes(713, "RTO_RETRIEVED");
	private static final String BUNDLE_NAME = "CRMPortalSuccessCodes";

	public CRMPortalSuccessCodes(int successCode, String successMessage) {
		setAll(successCode, successMessage, BUNDLE_NAME);
	}

}
