package com.myntra.portal.crm.client;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.test.TestHelper;
import com.myntra.commons.utils.ResponseHandler;
import com.myntra.portal.crm.client.constant.CRMPortalClientConstants;
import com.myntra.portal.crm.client.response.CouponResponse;

/**
 * Web client for customer coupon web service which serves customer's active
 * coupon detail
 * 
 * @author Arun Kumar
 */
public class CouponDetailClient extends ResponseHandler {

	public static final String SERVICE_PREFIX = "/crm/coupon/";

	public static CouponResponse getCouponDetail(String serviceURL, String login) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMPortalClientConstants.CRM_PORTAL_URL,
				TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX);
		client.query("login", login);		
		return client.get(CouponResponse.class);
	}

	public static void main(String args[]) throws ERPServiceException {
		CouponResponse test = getCouponDetail(null, "pravin.mehta@myntra.com");		
		System.out.println(test);
	}

}