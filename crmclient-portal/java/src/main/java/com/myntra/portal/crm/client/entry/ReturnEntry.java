package com.myntra.portal.crm.client.entry;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for customer return, return status, comments detail
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "returnEntry")
public class ReturnEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private Long returnId;
	private Long orderId;
	private Long itemId;
	private String skuCode;

	private Integer quantity;
	private String size;
	private String returnStatus;
	private String login;
	private String customerName;
	private String returnAddress;
	private String city;
	private String state;
	private String country;
	private String zipCode;
	private String mobile;
	private String returnMode;
	private String courierService;
	private String trackingNumber;
	private String returnReason;
	private String returnDescription;
	private Double pickupCharge;
	private Double refundAmount;
	private Date returnCreatedDate;
	private String DCCode;
	private Boolean isRefunded;
	private Date refundDate;
	private Double refundModeCredit;
	private String returnStatusCode;
	private String returnStatusName;
	private String returnStatusDescription;
	private String itemBarCode;
	private Integer wareHouseId;
	
	// return comments
	private List<ReturnCommentEntry> returnCommentEntry;

	public ReturnEntry() {
	}
	
	public ReturnEntry(Long returnId, Long orderId, Long itemId, String skuCode, Integer quantity, String size, String returnStatus,
			String login, String customerName, String returnAddress, String city, String state, String country,
			String zipCode, String mobile, String returnMode, String courierService, String trackingNumber,
			String returnReason, String returnDescription, Double pickupCharge, Double refundAmount,
			Date returnCreatedDate, String dCCode, Boolean isRefunded, Date refundDate, Double refundModeCredit,
			String returnStatusCode, String returnStatusName, String returnStatusDescription, String itemBarCode,
			Integer wareHouseId) {
		
		this.returnId = returnId;
		this.orderId = orderId;
		this.itemId = itemId;
		this.skuCode = skuCode;
		this.quantity = quantity;
		this.size = size;
		this.returnStatus = returnStatus;
		this.login = login;
		this.customerName = customerName;
		this.returnAddress = returnAddress;
		this.city = city;
		this.state = state;
		this.country = country;
		this.zipCode = zipCode;
		this.mobile = mobile;
		this.returnMode = returnMode;
		this.courierService = courierService;
		this.trackingNumber = trackingNumber;
		this.returnReason = returnReason;
		this.returnDescription = returnDescription;
		this.pickupCharge = pickupCharge;
		this.refundAmount = refundAmount;
		this.returnCreatedDate = returnCreatedDate;
		this.DCCode = dCCode;
		this.isRefunded = isRefunded;
		this.refundDate = refundDate;
		this.refundModeCredit = refundModeCredit;
		this.returnStatusCode = returnStatusCode;
		this.returnStatusName = returnStatusName;
		this.returnStatusDescription = returnStatusDescription;
		this.itemBarCode = itemBarCode;
		this.wareHouseId = wareHouseId;	
	}


	public Long getReturnId() {
		return returnId;
	}

	public void setReturnId(Long returnId) {
		this.returnId = returnId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}
	
	public String getSkuCode() {
		return skuCode;
	}
	
	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getReturnStatus() {
		return returnStatus;
	}

	public void setReturnStatus(String returnStatus) {
		this.returnStatus = returnStatus;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getReturnAddress() {
		return returnAddress;
	}

	public void setReturnAddress(String returnAddress) {
		this.returnAddress = returnAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getReturnMode() {
		return returnMode;
	}

	public void setReturnMode(String returnMode) {
		this.returnMode = returnMode;
	}

	public String getCourierService() {
		return courierService;
	}

	public void setCourierService(String courierService) {
		this.courierService = courierService;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getReturnReason() {
		return returnReason;
	}

	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}

	public String getReturnDescription() {
		return returnDescription;
	}

	public void setReturnDescription(String returnDescription) {
		this.returnDescription = returnDescription;
	}

	public Double getPickupCharge() {
		return pickupCharge;
	}

	public void setPickupCharge(Double pickupCharge) {
		this.pickupCharge = pickupCharge;
	}

	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public Date getReturnCreatedDate() {
		return returnCreatedDate;
	}

	public void setReturnCreatedDate(Date returnCreatedDate) {
		this.returnCreatedDate = returnCreatedDate;
	}

	public String getDCCode() {
		return DCCode;
	}

	public void setDCCode(String dCCode) {
		DCCode = dCCode;
	}

	public Boolean getIsRefunded() {
		return isRefunded;
	}

	public void setIsRefunded(Boolean isRefunded) {
		this.isRefunded = isRefunded;
	}

	public Date getRefundDate() {
		return refundDate;
	}

	public void setRefundDate(Date refundDate) {
		this.refundDate = refundDate;
	}

	public Double getRefundModeCredit() {
		return refundModeCredit;
	}

	public void setRefundModeCredit(Double refundModeCredit) {
		this.refundModeCredit = refundModeCredit;
	}

	public String getReturnStatusCode() {
		return returnStatusCode;
	}

	public void setReturnStatusCode(String returnStatusCode) {
		this.returnStatusCode = returnStatusCode;
	}

	public String getReturnStatusName() {
		return returnStatusName;
	}

	public void setReturnStatusName(String returnStatusName) {
		this.returnStatusName = returnStatusName;
	}

	public String getReturnStatusDescription() {
		return returnStatusDescription;
	}

	public void setReturnStatusDescription(String returnStatusDescription) {
		this.returnStatusDescription = returnStatusDescription;
	}

	public String getItemBarCode() {
		return itemBarCode;
	}

	public void setItemBarCode(String itemBarCode) {
		this.itemBarCode = itemBarCode;
	}

	public Integer getWareHouseId() {
		return wareHouseId;
	}

	public void setWareHouseId(Integer wareHouseId) {
		this.wareHouseId = wareHouseId;
	}
	
	@XmlElementWrapper(name = "returnCommentEntryList")
	@XmlElement(name = "returnCommentEntry")
	public List<ReturnCommentEntry> getReturnCommentEntry() {
		return returnCommentEntry;
	}

	public void setReturnCommentEntry(List<ReturnCommentEntry> returnCommentEntry) {
		this.returnCommentEntry = returnCommentEntry;
	}

	@Override
	public String toString() {
		return "ReturnEntry [returnId=" + returnId + ", orderId=" + orderId + ", itemId=" + itemId + ", skuCode=" + skuCode + ", quantity="
				+ quantity + ", size=" + size + ", returnStatus=" + returnStatus + ", login=" + login
				+ ", customerName=" + customerName + ", returnAddress=" + returnAddress + ", city=" + city + ", state="
				+ state + ", country=" + country + ", zipCode=" + zipCode + ", mobile=" + mobile + ", returnMode="
				+ returnMode + ", courierService=" + courierService + ", trackingNumber=" + trackingNumber
				+ ", returnReason=" + returnReason + ", returnDescription=" + returnDescription + ", pickupCharge="
				+ pickupCharge + ", refundAmount=" + refundAmount + ", returnCreatedDate=" + returnCreatedDate
				+ ", DCCode=" + DCCode + ", isRefunded=" + isRefunded + ", refundDate=" + refundDate
				+ ", refundModeCredit=" + refundModeCredit + ", returnStatusCode=" + returnStatusCode
				+ ", returnStatusName=" + returnStatusName + ", returnStatusDescription=" + returnStatusDescription
				+ ", itemBarCode=" + itemBarCode + ", wareHouseId=" + wareHouseId +"]";
	}
	
}