package com.myntra.portal.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.entry.CouponEntry;
/**
 * @author Pravin Mehta
 */
@XmlRootElement(name = "couponResponse")
public class CouponResponse extends AbstractResponse {

	private static final long serialVersionUID = 3688398701763484215L;
	private List<CouponEntry> couponEntryList;

	public CouponResponse() {
	}

	@XmlElementWrapper(name = "couponEntryList")
	@XmlElement(name = "couponEntry")
	public List<CouponEntry> getCouponEntryList() {
		return couponEntryList;
	}

	public void setCouponEntryList(List<CouponEntry> couponEntryList) {
		this.couponEntryList = couponEntryList;
	}

	@Override
	public String toString() {
		return "CouponResponse [couponEntryList=" + couponEntryList + "]";
	}
}