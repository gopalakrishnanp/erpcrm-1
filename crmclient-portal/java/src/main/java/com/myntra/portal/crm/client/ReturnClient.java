package com.myntra.portal.crm.client;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.test.TestHelper;
import com.myntra.commons.utils.ResponseHandler;
import com.myntra.portal.crm.client.constant.CRMPortalClientConstants;
import com.myntra.portal.crm.client.response.ReturnResponse;

/**
 * Web client for customer return web service which integrates detail of return,
 * status, comments.
 * 
 * @author Arun Kumar
 */
public class ReturnClient extends ResponseHandler {

	public static final String SERVICE_PREFIX = "/crm/return/";

	public static ReturnResponse getCustomerReturn(String serviceURL, Long returnId, Long orderId,
			String login) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMPortalClientConstants.CRM_PORTAL_URL,
				TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX);
		
		if(returnId!=null) {
			client.query("returnId", returnId);
		}
		if(orderId!=null) {
			client.query("orderId", orderId);
		}
		if(login!=null && !login.trim().equals("")){
			client.query("login", login);	
		}		

		return client.get(ReturnResponse.class);
	}

	public static void main(String args[]) throws ERPServiceException {
		// 56932&orderId=2611060
		ReturnResponse test = getCustomerReturn("http://localhost:7092/crmservice-portal", 56932L,
				2611060L, "pravin.mehta@myntra.com");
		System.out.println(test);
	}

}