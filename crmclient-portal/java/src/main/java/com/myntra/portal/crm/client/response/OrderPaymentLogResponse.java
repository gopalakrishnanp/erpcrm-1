package com.myntra.portal.crm.client.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.entry.OrderPaymentLogEntry;

/**
 * Response for order payment log web service
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "orderPaymentLogResponse")
public class OrderPaymentLogResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;
	private OrderPaymentLogEntry orderPaymentLogEntry;

	public OrderPaymentLogResponse() {
	
	}	
	
	@XmlElement(name = "orderPaymentLogEntry")
	public OrderPaymentLogEntry getOrderPaymentLogEntry() {
		return orderPaymentLogEntry;
	}	

	public void setOrderPaymentLogEntry(OrderPaymentLogEntry orderPaymentLogEntry) {
		this.orderPaymentLogEntry = orderPaymentLogEntry;
	}

	@Override
	public String toString() {
		return "OrderPaymentLogResponse [orderPaymentLogEntry=" + orderPaymentLogEntry + "]";
	}
}