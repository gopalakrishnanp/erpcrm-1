package com.myntra.portal.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.portal.crm.client.entry.OrderPaymentLogEntry;
import com.myntra.portal.crm.client.response.OrderPaymentLogResponse;

/**
 * Order payment log service interface to retrieve the detail of order payment
 * and gateway
 * 
 * @author Arun Kumar
 */
@Path("/order/payment/log/{orderId}")
public interface OrderPaymentLogService extends BaseService<OrderPaymentLogResponse, OrderPaymentLogEntry> {

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	AbstractResponse getOrderPaymentLog(@PathParam("orderId") Long orderId) throws ERPServiceException;
}
