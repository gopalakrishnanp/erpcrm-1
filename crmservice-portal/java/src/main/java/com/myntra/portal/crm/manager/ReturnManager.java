package com.myntra.portal.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.portal.crm.client.entry.ReturnEntry;
import com.myntra.portal.crm.client.response.ReturnResponse;

/**
 * Manager interface(abstract) for customer return web service which integrates
 * detail of return comments.
 * 
 * @author Arun Kumar
 */
public interface ReturnManager extends BaseManager<ReturnResponse, ReturnEntry> {

	public ReturnResponse getReturnDetail(Long returnId, Long orderId, String login) throws ERPServiceException;
}