package com.myntra.portal.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.portal.crm.client.entry.CustomerProfileEntry;
import com.myntra.portal.crm.client.response.CustomerProfileResponse;
import com.myntra.portal.crm.manager.CustomerProfileManager;
import com.myntra.portal.crm.service.CustomerProfileService;

public class CustomerProfileServiceImpl extends BaseServiceImpl<CustomerProfileResponse, CustomerProfileEntry> implements CustomerProfileService {

    @Override
    public AbstractResponse getCustomerProfileByLogin(String login) throws ERPServiceException {
        try {
            return ((CustomerProfileManager) getManager()).getCustomerProfileByLogin(login);
        } catch (ERPServiceException e) {
            return e.getResponse();
        } catch (TransactionSystemException ex) {
            // Spring encapsulates Application Exception into TransactionSystemException
            return ((ERPServiceException) ex.getApplicationException()).getResponse();
        }
    }
    
    @Override
    public AbstractResponse getCustomerProfileByMobile(String mobile) throws ERPServiceException {
        try {
            return ((CustomerProfileManager) getManager()).getCustomerProfileByMobile(mobile);
        } catch (ERPServiceException e) {
            return e.getResponse();
        } catch (TransactionSystemException ex) {
            // Spring encapsulates Application Exception into TransactionSystemException
            return ((ERPServiceException) ex.getApplicationException()).getResponse();
        }
    }

    @Override
    public AbstractResponse createCustomerProfile() throws ERPServiceException {
        return ((CustomerProfileManager) getManager()).createCustomerProfile();
    }
}
