package com.myntra.portal.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.portal.crm.client.entry.CustomerOrderCommentEntry;
import com.myntra.portal.crm.client.response.CustomerOrderCommentResponse;

/**
 * Manager interface(abstract) for customer order comment log service
 * 
 * @author Arun Kumar
 */
public interface OrderCommentLogManager extends BaseManager<CustomerOrderCommentResponse, CustomerOrderCommentEntry> {

	public CustomerOrderCommentResponse getOrderCommentLog(Long orderId) throws ERPServiceException;
}