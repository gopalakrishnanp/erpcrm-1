/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.portal.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.portal.crm.client.entry.CouponEntry;
import com.myntra.portal.crm.client.response.CouponResponse;

/**
 *
 * @author pravin
 */
public interface CouponManager extends BaseManager<CouponResponse, CouponEntry>{
    public CouponResponse getCouponForLogin(String login) throws ERPServiceException;
}
