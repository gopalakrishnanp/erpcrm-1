package com.myntra.portal.crm.manager.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.cache.annotation.Cacheable;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.code.CRMPortalSuccessCodes;
import com.myntra.portal.crm.client.entry.OrderPaymentLogEntry;
import com.myntra.portal.crm.client.response.OrderPaymentLogResponse;
import com.myntra.portal.crm.dao.CustomerOrderDAO;
import com.myntra.portal.crm.entity.OrderPaymentLogEntity;
import com.myntra.portal.crm.manager.OrderPaymentLogManager;
import com.myntra.portal.crm.util.WebserviceUtil;

/**
 * Manager implementation for order payment log.
 * 
 * @author Arun Kumar
 */
public class OrderPaymentLogManagerImpl extends BaseManagerImpl<OrderPaymentLogResponse, OrderPaymentLogEntry>
		implements OrderPaymentLogManager {

	private static final Logger LOGGER = Logger.getLogger(OrderPaymentLogManagerImpl.class);

	private CustomerOrderDAO customerOrderDAO;

	public CustomerOrderDAO getCustomerOrderDAO() {
		return customerOrderDAO;
	}

	public void setCustomerOrderDAO(CustomerOrderDAO customerOrderDAO) {
		this.customerOrderDAO = customerOrderDAO;
	}

	@Override
	@Cacheable(value = "orderPaymentLogCache")
	public OrderPaymentLogResponse getOrderPaymentLog(Long orderId) throws ERPServiceException {

		// Initialise order payment response and entry
		OrderPaymentLogResponse response = new OrderPaymentLogResponse();
		OrderPaymentLogEntry orderPaymentLogEntry = new OrderPaymentLogEntry();
		OrderPaymentLogEntity orderPaymentLogEntity = null;

		try {
			LOGGER.debug("\n\nPayment log from Service in paymentLogURL\n\n");
			String paymentLogURL = WebserviceUtil.getServiceUrlForKey("paymentLogURL");

			LOGGER.debug("Trying to get payment info from link : " + paymentLogURL + orderId);

			HttpGet getRequest = new HttpGet(paymentLogURL + orderId);
			getRequest.addHeader("accept", "application/json");

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse httpResponse = httpClient.execute(getRequest);

			if (httpResponse.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + httpResponse.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((httpResponse.getEntity().getContent())));

			String data;
			StringBuilder outputBuilder = new StringBuilder(2048);
			while ((data = br.readLine()) != null) {
				outputBuilder.append(data);
			}

			JSONObject paymentLogObject = new JSONObject(outputBuilder.toString());
			if (paymentLogObject != null) {
				orderPaymentLogEntry.setLogId((Long) paymentLogObject.getLong("id"));
				orderPaymentLogEntry.setOrderId((Long) paymentLogObject.getLong("orderId"));
				orderPaymentLogEntry.setLogin(paymentLogObject.getString("login"));
				orderPaymentLogEntry.setUserAgent(paymentLogObject.getString("userAgent"));
				orderPaymentLogEntry.setIPAddress(paymentLogObject.getString("ipAddress"));
				orderPaymentLogEntry.setGateway(paymentLogObject.getString("gatewayName"));
				orderPaymentLogEntry.setPaymentOption(paymentLogObject.getString("paymentOption"));
				orderPaymentLogEntry.setPaymentIssuer(paymentLogObject.getString("paymentIssuer"));
				orderPaymentLogEntry.setBankTransactionId(paymentLogObject.getString("bankTransactionId"));
				orderPaymentLogEntry.setGatewayPaymentId(paymentLogObject.getString("gatewayPaymentId"));
				orderPaymentLogEntry.setCompletedVia(paymentLogObject.getString("completedVia"));
				orderPaymentLogEntry.setCardBankName(paymentLogObject.getString("cardBankName"));
				orderPaymentLogEntry.setBinNumber((Long) paymentLogObject.getLong("binNumber"));

				if (!JSONObject.NULL.equals(paymentLogObject.get("amount"))) {
					orderPaymentLogEntry.setAmount((Double) paymentLogObject.getDouble("amount"));
				}

				if (!JSONObject.NULL.equals(paymentLogObject.get("amountPaid"))) {
					orderPaymentLogEntry.setAmountPaid((Double) paymentLogObject.getDouble("amountPaid"));
				}

				if (!JSONObject.NULL.equals(paymentLogObject.get("amountToBePaid"))) {
					orderPaymentLogEntry.setAmountToBePaid((Double) paymentLogObject.getDouble("amountToBePaid"));
				}

				orderPaymentLogEntry.setIsInline(paymentLogObject.getBoolean("isInline"));
				orderPaymentLogEntry.setIsTampered(paymentLogObject.getBoolean("isTampered"));
				orderPaymentLogEntry.setIsComplete(paymentLogObject.getBoolean("isComplete"));
				orderPaymentLogEntry.setIsFlagged(paymentLogObject.getBoolean("isFlagged"));
				orderPaymentLogEntry.setResponseCode(paymentLogObject.getString("responseCode"));
				orderPaymentLogEntry.setResponseMessage(paymentLogObject.getString("responseMessage"));
				orderPaymentLogEntry.setLogInsertTime(new Date(paymentLogObject.getLong("insertTime")));
				orderPaymentLogEntry.setReturnTime(new Date(paymentLogObject.getLong("returnTime")));
				orderPaymentLogEntry.setTransactionTime(new Date(paymentLogObject.getLong("transactionTime")));
			}

			response.setOrderPaymentLogEntry(orderPaymentLogEntry);

		} catch (ClientProtocolException e) {
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			LOGGER.debug("PaymentLogURL Error: " + e.getMessage());
			// throw new ERPServiceException(response);
			orderPaymentLogEntity = null;
		} catch (IllegalStateException e) {
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			LOGGER.debug("PaymentLogURL Error: " + e.getMessage());
			// throw new ERPServiceException(response);
			orderPaymentLogEntity = null;
		} catch (IOException e) {
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			LOGGER.debug("PaymentLogURL Error: " + e.getMessage());
			// throw new ERPServiceException(response);
			orderPaymentLogEntity = null;
		} catch (JSONException e) {
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			LOGGER.debug("PaymentLogURL Error: " + e.getMessage());
			// throw new ERPServiceException(response);
			orderPaymentLogEntity = null;
		} catch (RuntimeException e) {
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			LOGGER.debug("PaymentLogURL Error: " + e.getMessage());
			// throw new ERPServiceException(response);
			orderPaymentLogEntity = null;
		}

		if (orderPaymentLogEntity == null) {

			LOGGER.debug("\n\nPayment log from portal native query\n\n");

			try {
				orderPaymentLogEntity = (OrderPaymentLogEntity) ((CustomerOrderDAO) getCustomerOrderDAO())
						.getOrderPaymentLog(orderId);

				if (orderPaymentLogEntity != null) {

					orderPaymentLogEntry.setLogId(orderPaymentLogEntity.getLogId());
					orderPaymentLogEntry.setOrderId(orderPaymentLogEntity.getOrderId());
					orderPaymentLogEntry.setLogin(orderPaymentLogEntity.getLogin());
					orderPaymentLogEntry.setUserAgent(orderPaymentLogEntity.getUserAgent());
					orderPaymentLogEntry.setIPAddress(orderPaymentLogEntity.getIPAddress());
					orderPaymentLogEntry.setGateway(orderPaymentLogEntity.getGateway());
					orderPaymentLogEntry.setPaymentOption(orderPaymentLogEntity.getPaymentOption());
					orderPaymentLogEntry.setPaymentIssuer(orderPaymentLogEntity.getPaymentIssuer());
					orderPaymentLogEntry.setBankTransactionId(orderPaymentLogEntity.getBankTransactionId());
					orderPaymentLogEntry.setGatewayPaymentId(orderPaymentLogEntity.getGatewayPaymentId());
					orderPaymentLogEntry.setCompletedVia(orderPaymentLogEntity.getCompletedVia());
					orderPaymentLogEntry.setCardBankName(orderPaymentLogEntity.getCardBankName());
					orderPaymentLogEntry.setBinNumber(orderPaymentLogEntity.getBinNumber());
					orderPaymentLogEntry.setAmount(orderPaymentLogEntity.getAmount());
					orderPaymentLogEntry.setAmountPaid(orderPaymentLogEntity.getAmountPaid());
					orderPaymentLogEntry.setAmountToBePaid(orderPaymentLogEntity.getAmountToBePaid());
					orderPaymentLogEntry.setIsInline(orderPaymentLogEntity.getIsInline());
					orderPaymentLogEntry.setIsTampered(orderPaymentLogEntity.getIsTampered());
					orderPaymentLogEntry.setIsComplete(orderPaymentLogEntity.getIsComplete());
					orderPaymentLogEntry.setIsFlagged(orderPaymentLogEntity.getIsFlagged());
					orderPaymentLogEntry.setResponseCode(orderPaymentLogEntity.getResponseCode());
					orderPaymentLogEntry.setResponseMessage(orderPaymentLogEntity.getResponseMessage());
					orderPaymentLogEntry.setLogInsertTime(orderPaymentLogEntity.getLogInsertTime());
					orderPaymentLogEntry.setReturnTime(orderPaymentLogEntity.getReturnTime());
					orderPaymentLogEntry.setTransactionTime(orderPaymentLogEntity.getTransactionTime());
				}
				response.setOrderPaymentLogEntry(orderPaymentLogEntry);

			} catch (ERPServiceException e) {
				LOGGER.error("Error validating/retreiving data", e);
				StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
				response.setStatus(error);
				throw new ERPServiceException(response);

			} catch (Exception e) {
				LOGGER.error("Error", e);
				StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
				response.setStatus(error);
				throw new ERPServiceException(response);
			}
		}

		// response with status
		if (orderPaymentLogEntity == null) {
			StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.PAYMENT_LOG_RETRIEVED,
					StatusResponse.Type.SUCCESS, 0);
			response.setStatus(success);
			
		} else {
			StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.PAYMENT_LOG_RETRIEVED,
					StatusResponse.Type.SUCCESS, 1);
			response.setStatus(success);
		}
		return response;
	}

	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse create(OrderPaymentLogEntry entry) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(OrderPaymentLogEntry entry, Long itemId) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}