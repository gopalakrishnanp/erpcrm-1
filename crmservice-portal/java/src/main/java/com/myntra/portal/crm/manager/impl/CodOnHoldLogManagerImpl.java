package com.myntra.portal.crm.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.code.CRMPortalSuccessCodes;
import com.myntra.portal.crm.client.entry.CodOnHoldLogEntry;
import com.myntra.portal.crm.client.response.CodOnHoldLogResponse;
import com.myntra.portal.crm.dao.CustomerOrderDAO;
import com.myntra.portal.crm.entity.CodOnHoldLogEntity;
import com.myntra.portal.crm.manager.CodOnHoldLogManager;

/**
 * Manager implementation for COD order on hold reason log.
 * 
 * @author Arun Kumar
 */
public class CodOnHoldLogManagerImpl extends BaseManagerImpl<CodOnHoldLogResponse, CodOnHoldLogEntry> implements
		CodOnHoldLogManager {

	private static final Logger LOGGER = Logger.getLogger(CodOnHoldLogManagerImpl.class);

	private CustomerOrderDAO customerOrderDAO;

	public CustomerOrderDAO getCustomerOrderDAO() {
		return customerOrderDAO;
	}

	public void setCustomerOrderDAO(CustomerOrderDAO customerOrderDAO) {
		this.customerOrderDAO = customerOrderDAO;
	}

	@Override
	public CodOnHoldLogResponse getCodOnHoldLog(Long orderId) throws ERPServiceException {

		// Initialise COD order onhold response and entry
		CodOnHoldLogResponse response = new CodOnHoldLogResponse();
		List<CodOnHoldLogEntry> codOnHoldLogEntryList = new ArrayList<CodOnHoldLogEntry>();

		try {

			List<CodOnHoldLogEntity> codOnHoldLogEntityList = (List<CodOnHoldLogEntity>) ((CustomerOrderDAO) getCustomerOrderDAO())
					.getCodOnHoldLog(orderId);

			if (codOnHoldLogEntityList != null) {
				for (CodOnHoldLogEntity singleOnHoldOrderLogEntity : codOnHoldLogEntityList) {
					CodOnHoldLogEntry singleOnHoldOrderLogEntry = toCodOnHoldLogEntry(singleOnHoldOrderLogEntity);
					codOnHoldLogEntryList.add(singleOnHoldOrderLogEntry);
				}
			}
			response.setCodOnHoldLogEntryList(codOnHoldLogEntryList);

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);

		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.COD_ORDER_OH_REASON_LOG_RETRIEVED,
				StatusResponse.Type.SUCCESS, response.getCodOnHoldLogEntryList().size());
		response.setStatus(success);
		return response;
	}

	private CodOnHoldLogEntry toCodOnHoldLogEntry(CodOnHoldLogEntity ohe) {

		if (ohe == null) {
			return null;
		}

		return new CodOnHoldLogEntry(ohe.getOrderId(), ohe.getDisposition(), ohe.getReason(),
				ohe.getReasonDisplayName(), ohe.getTrialNumber(), ohe.getCreatedBy(), ohe.getComment(),
				ohe.getCreatedDate());
	}

	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse create(CodOnHoldLogEntry arg0) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(CodOnHoldLogEntry arg0, Long arg1) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}