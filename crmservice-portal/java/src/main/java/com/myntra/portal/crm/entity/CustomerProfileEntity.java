package com.myntra.portal.crm.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;

import com.myntra.commons.entities.BaseEntity;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;

@Entity
@Table(name = "xcart_customers")
@NamedNativeQueries({
    @NamedNativeQuery(
        name = CRMPortalServiceConstants.NAMED_QUERIES.CUSTOMER_PROFILE_BY_LOGIN,
        query = "SELECT login, " +
        		" if(gender='',null,gender) as gender, " +
        		" DOB, firstname, lastname, email, phone, referer, " +
    			" mobile, first_login, last_login, status from xcart_customers where login=:login and usertype='C'",
        resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_CUSTOMER_PROFILE_BY_LOGIN
    ),
    @NamedNativeQuery(
            name = CRMPortalServiceConstants.NAMED_QUERIES.CUSTOMER_PROFILE_BY_MOBILE,
            query = "SELECT login, " +
            		" if(gender='',null,gender) as gender, " +
            		" DOB, firstname, lastname, email, phone, referer, " +
            		" mobile, first_login, last_login, status from xcart_customers where mobile=:mobile and usertype='C' " +
            		" order by last_login desc limit 1",
            resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_CUSTOMER_PROFILE_BY_MOBILE
        )
})

@SqlResultSetMappings({
    @SqlResultSetMapping(name=CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_CUSTOMER_PROFILE_BY_LOGIN,
        columns = {
    		@ColumnResult(name="login"),
            @ColumnResult(name="gender"),
            @ColumnResult(name="DOB"),
            @ColumnResult(name="firstname"),
            @ColumnResult(name="lastname"),
            @ColumnResult(name="email"),
            @ColumnResult(name="phone"),
            @ColumnResult(name="referer"),
            @ColumnResult(name="mobile"),
            @ColumnResult(name="first_login"),
            @ColumnResult(name="last_login"),
            @ColumnResult(name="status")
        }),
        @SqlResultSetMapping(name=CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_CUSTOMER_PROFILE_BY_MOBILE,
        columns = {
    		@ColumnResult(name="login"),
            @ColumnResult(name="gender"),
            @ColumnResult(name="DOB"),
            @ColumnResult(name="firstname"),
            @ColumnResult(name="lastname"),
            @ColumnResult(name="email"),
            @ColumnResult(name="phone"),
            @ColumnResult(name="referer"),
            @ColumnResult(name="mobile"),
            @ColumnResult(name="first_login"),
            @ColumnResult(name="last_login"),
            @ColumnResult(name="status")
        }),
        
})

public class CustomerProfileEntity extends BaseEntity {

    private static final long serialVersionUID = 3688398701763484215L;
    
    @Id
    @Column(name = "login")
    private String login;
        
    @Column(name = "gender")
    private String gender;
    
    @Column(name = "DOB")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dob;
    
    @Column(name = "firstname")
    private String firstName;
    
    @Column(name = "lastname")
    private String lastName;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "phone")
    private String phone;
    
    @Column(name = "referer")
    private String referer;
    
    @Column(name = "mobile")
    private String mobile;
        
    @Column(name = "first_login")
    private Integer firstLogin;
    
    @Column(name = "last_login")
    private Integer lastLogin;
    
    @Column(name = "status")
    private String status;
    
    public CustomerProfileEntity() {
    }

    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(Integer firstLogin) {
        this.firstLogin = firstLogin;
    }
    
    public Integer getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Integer lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public String getReferer() {
		return referer;
	}
    
	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    
    public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public CustomerProfileEntity(String login, String gender, Date dob, String firstName, 
    		String lastName, String email, String phone, String referer, String mobile, 
    		Integer firstLogin, Integer lastLogin, String status) {
    	
        this.login = login;
        this.gender = gender;
        this.dob = dob;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.referer = referer;
        this.mobile = mobile;
        this.firstLogin = firstLogin;
        this.lastLogin = lastLogin;
        this.status = status;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CustomerProfileEntity [")
                .append("login=").append(login)
                .append(", gender=").append(gender)
                .append(", dob=").append(dob)
                .append(", firstName=").append(firstName)
                .append(", lastName=").append(lastName)
                .append(", email=").append(email)
                .append(", phone=").append(phone)
                .append(", referer=").append(referer)
                .append(", mobile=").append(mobile)
                .append(", firstLogin=").append(firstLogin)
                .append(", lastLogin=").append(lastLogin)
                .append(", status=").append(status)
                .append("]");
        return builder.toString();
    }
}