package com.myntra.portal.crm.dao;

import java.util.List;

import com.myntra.commons.dao.BaseDAO;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.portal.crm.entity.SREntity;

/**
 * DAO for SR
 * 
 * @author Arun Kumar
 */
public interface SRDAO extends BaseDAO<SREntity> {

	public List<SREntity> getSRDetail(Long id, String loginOrMobile, String filterType) throws ERPServiceException;

}
