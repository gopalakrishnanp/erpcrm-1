package com.myntra.portal.crm.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import com.myntra.commons.entities.BaseEntity;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;

/**
 * Entity for return and its status detail
 * 
 * @author Arun Kumar
 */
@Entity
@Table(name = "xcart_returns")
@NamedNativeQueries({
		@NamedNativeQuery(
				name = CRMPortalServiceConstants.NAMED_QUERIES.CUSTOMER_RETURN_BY_LOGIN, 
				query = "select"
						+ " r.returnid,r.orderid,r.itemid,r.skucode,r.quantity,r.sizeoption as size,r.status as returnStatus"
						+ " ,r.login,r.name as customerName,r.address as returnAddress,r.city,r.state,r.country"
						+ " ,r.zipcode,r.mobile,r.return_mode as returnMode,r.courier_service as courierService"
						+ " ,r.tracking_no as trackingNumber,r.reason as returnReason,r.description as returnDescription"
						+ " ,r.pickup_charges as pickupCharge,r.refundamount as refundAmount,from_unixtime(r.createddate) as returnCreatedDate"
						+ " ,r.dc_code as DCCode,r.is_refunded as isRefunded,from_unixtime(r.refundeddate) as refundDate,r.delivery_credit as returnModeCredit"
						+ " ,rs.code as returnStatusCode,rs.display_name as returnStatusName,rs.mymyntra_msg as returnStatusDescription"
						+ " ,r.itembarcode as itemBarCode, r.warehouseid as wareHouseId"
						+ " from" + " xcart_returns r" 
						+ " inner join mk_return_status_details rs on rs.code=r.status"
						+ " where" + " r.login=:login" 
						+ " order by r.createddate desc", 
				resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_CUSTOMER_RETURN),

		@NamedNativeQuery(
				name = CRMPortalServiceConstants.NAMED_QUERIES.CUSTOMER_RETURN_BY_ORDERID, 
				query = "select"
						+ " r.returnid,r.orderid,r.itemid,r.skucode,r.quantity,r.sizeoption as size,r.status as returnStatus"
						+ " ,r.login,r.name as customerName,r.address as returnAddress,r.city,r.state,r.country"
						+ " ,r.zipcode,r.mobile,r.return_mode as returnMode,r.courier_service as courierService"
						+ " ,r.tracking_no as trackingNumber,r.reason as returnReason,r.description as returnDescription"
						+ " ,r.pickup_charges as pickupCharge,r.refundamount as refundAmount,from_unixtime(r.createddate) as returnCreatedDate"
						+ " ,r.dc_code as DCCode,r.is_refunded as isRefunded,from_unixtime(r.refundeddate) as refundDate,r.delivery_credit as returnModeCredit"
						+ " ,rs.code as returnStatusCode,rs.display_name as returnStatusName,rs.mymyntra_msg as returnStatusDescription"
						+ " ,r.itembarcode as itemBarCode, r.warehouseid as wareHouseId"
						+ " from" + " xcart_returns r" + " inner join mk_return_status_details rs on rs.code=r.status"
						+ " where" + " r.orderid=:orderid"
						+ " order by r.createddate desc", 
				resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_CUSTOMER_RETURN),

		@NamedNativeQuery(
				name = CRMPortalServiceConstants.NAMED_QUERIES.CUSTOMER_RETURN_BY_RETURNID, 
				query = "select"
						+ " r.returnid,r.orderid,r.itemid,r.skucode,r.quantity,r.sizeoption as size,r.status as returnStatus"
						+ " ,r.login,r.name as customerName,r.address as returnAddress,r.city,r.state,r.country"
						+ " ,r.zipcode,r.mobile,r.return_mode as returnMode,r.courier_service as courierService"
						+ " ,r.tracking_no as trackingNumber,r.reason as returnReason,r.description as returnDescription"
						+ " ,r.pickup_charges as pickupCharge,r.refundamount as refundAmount,from_unixtime(r.createddate) as returnCreatedDate"
						+ " ,r.dc_code as DCCode,r.is_refunded as isRefunded,from_unixtime(r.refundeddate) as refundDate,r.delivery_credit as returnModeCredit"
						+ " ,rs.code as returnStatusCode,rs.display_name as returnStatusName,rs.mymyntra_msg as returnStatusDescription"
						+ " ,r.itembarcode as itemBarCode, r.warehouseid as wareHouseId"
						+ " from" + " xcart_returns r" + " inner join mk_return_status_details rs on rs.code=r.status"
						+ " where" + " r.returnid=:returnid"
						+ " order by r.createddate desc",
				resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_CUSTOMER_RETURN) 
		})

@SqlResultSetMapping(
	name = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_CUSTOMER_RETURN, 
	columns = {
			@ColumnResult(name = "returnId"), @ColumnResult(name = "orderId"), @ColumnResult(name = "itemId"), @ColumnResult(name = "skuCode"),
			@ColumnResult(name = "quantity"), @ColumnResult(name = "size"), @ColumnResult(name = "returnStatus"),
			@ColumnResult(name = "login"), @ColumnResult(name = "customerName"),
			@ColumnResult(name = "returnAddress"), @ColumnResult(name = "city"), @ColumnResult(name = "state"),
			@ColumnResult(name = "country"), @ColumnResult(name = "zipCode"), @ColumnResult(name = "mobile"),
			@ColumnResult(name = "returnMode"), @ColumnResult(name = "courierService"),
			@ColumnResult(name = "trackingNumber"), @ColumnResult(name = "returnReason"),
			@ColumnResult(name = "returnDescription"), @ColumnResult(name = "pickupCharge"),
			@ColumnResult(name = "refundAmount"), @ColumnResult(name = "returnCreatedDate"),
			@ColumnResult(name = "DCCode"), @ColumnResult(name = "isRefunded"), @ColumnResult(name = "refundDate"),
			@ColumnResult(name = "returnModeCredit"), @ColumnResult(name = "returnStatusCode"),
			@ColumnResult(name = "returnStatusName"), @ColumnResult(name = "returnStatusDescription"),
			@ColumnResult(name = "itemBarCode"), @ColumnResult(name = "wareHouseId")
	})

public class CustomerReturnEntity extends BaseEntity {

	private static final long serialVersionUID = 3688398701763484215L;

	@Id
	@Column(name = "returnId")
	private Long returnId;

	@Id
	@Column(name = "orderId")
	private Long orderId;
		
	@Column(name = "itemId")
	private Long itemId;

	@Column(name = "skuCode")
	private String skuCode;

	@Column(name = "quantity")
	private Integer quantity;

	@Column(name = "size")
	private String size;

	@Column(name = "returnStatus")
	private String returnStatus;

	@Id
	@Column(name = "login")
	private String login;

	@Column(name = "customerName")
	private String customerName;

	@Column(name = "returnAddress")
	private String returnAddress;

	@Column(name = "city")
	private String city;

	@Column(name = "state")
	private String state;

	@Column(name = "country")
	private String country;

	@Column(name = "zipCode")
	private String zipCode;

	@Column(name = "mobile")
	private String mobile;

	@Column(name = "returnMode")
	private String returnMode;

	@Column(name = "courierService")
	private String courierService;

	@Column(name = "trackingNumber")
	private String trackingNumber;

	@Column(name = "returnReason")
	private String returnReason;

	@Column(name = "returnDescription")
	private String returnDescription;

	@Column(name = "pickupCharge")
	private Double pickupCharge;

	@Column(name = "refundAmount")
	private Double refundAmount;

	@Column(name = "returnCreatedDate")
	// @Temporal(javax.persistence.TemporalType.DATE)
	private Date returnCreatedDate;

	@Column(name = "DCCode")
	private String DCCode;

	@Column(name = "isRefunded")
	private boolean isRefunded;

	@Column(name = "refundDate")
	// @Temporal(javax.persistence.TemporalType.DATE)
	private Date refundDate;

	@Column(name = "returnModeCredit")
	private Double refundModeCredit;

	@Column(name = "returnStatusCode")
	private String returnStatusCode;

	@Column(name = "returnStatusName")
	private String returnStatusName;

	@Column(name = "returnStatusDescription")
	private String returnStatusDescription;
	
	@Column(name = "itemBarCode")
	private String itemBarCode;

	@Column(name = "wareHouseId")
	private Integer wareHouseId;

	public CustomerReturnEntity() {

	}
	
	public CustomerReturnEntity(Long returnId, Long orderId, Long itemId, String skuCode, Integer quantity, String size,
			String returnStatus, String login, String customerName, String returnAddress, String city, String state,
			String country, String zipCode, String mobile, String returnMode, String courierService,
			String trackingNumber, String returnReason, String returnDescription, Double pickupCharge,
			Double refundAmount, Date returnCreatedDate, String dCCode, boolean isRefunded, Date refundDate,
			Double refundModeCredit, String returnStatusCode, String returnStatusName, String returnStatusDescription,
			String itemBarCode, Integer wareHouseId) {
	
		this.returnId = returnId;
		this.orderId = orderId;
		this.itemId = itemId;
		this.skuCode = skuCode;
		this.quantity = quantity;
		this.size = size;
		this.returnStatus = returnStatus;
		this.login = login;
		this.customerName = customerName;
		this.returnAddress = returnAddress;
		this.city = city;
		this.state = state;
		this.country = country;
		this.zipCode = zipCode;
		this.mobile = mobile;
		this.returnMode = returnMode;
		this.courierService = courierService;
		this.trackingNumber = trackingNumber;
		this.returnReason = returnReason;
		this.returnDescription = returnDescription;
		this.pickupCharge = pickupCharge;
		this.refundAmount = refundAmount;
		this.returnCreatedDate = returnCreatedDate;
		this.DCCode = dCCode;
		this.isRefunded = isRefunded;
		this.refundDate = refundDate;
		this.refundModeCredit = refundModeCredit;
		this.returnStatusCode = returnStatusCode;
		this.returnStatusName = returnStatusName;
		this.returnStatusDescription = returnStatusDescription;
		this.itemBarCode = itemBarCode;
		this.wareHouseId = wareHouseId;
	}


	public Long getReturnId() {
		return returnId;
	}

	public void setReturnId(Long returnId) {
		this.returnId = returnId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	
	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public String getSkuCode() {
		return skuCode;
	}

	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getReturnStatus() {
		return returnStatus;
	}

	public void setReturnStatus(String returnStatus) {
		this.returnStatus = returnStatus;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getReturnAddress() {
		return returnAddress;
	}

	public void setReturnAddress(String returnAddress) {
		this.returnAddress = returnAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getReturnMode() {
		return returnMode;
	}

	public void setReturnMode(String returnMode) {
		this.returnMode = returnMode;
	}

	public String getCourierService() {
		return courierService;
	}

	public void setCourierService(String courierService) {
		this.courierService = courierService;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getReturnReason() {
		return returnReason;
	}

	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}

	public String getReturnDescription() {
		return returnDescription;
	}

	public void setReturnDescription(String returnDescription) {
		this.returnDescription = returnDescription;
	}

	public Double getPickupCharge() {
		return pickupCharge;
	}

	public void setPickupCharge(Double pickupCharge) {
		this.pickupCharge = pickupCharge;
	}

	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public Date getReturnCreatedDate() {
		return returnCreatedDate;
	}

	public void setReturnCreatedDate(Date returnCreatedDate) {
		this.returnCreatedDate = returnCreatedDate;
	}

	public String getDCCode() {
		return DCCode;
	}

	public void setDCCode(String dCCode) {
		DCCode = dCCode;
	}

	public boolean getIsRefunded() {
		return isRefunded;
	}

	public void setIsRefunded(Boolean isRefunded) {
		this.isRefunded = isRefunded;
	}

	public Date getRefundDate() {
		return refundDate;
	}

	public void setRefundDate(Date refundDate) {
		this.refundDate = refundDate;
	}

	public Double getRefundModeCredit() {
		return refundModeCredit;
	}

	public void setRefundModeCredit(Double refundModeCredit) {
		this.refundModeCredit = refundModeCredit;
	}

	public String getReturnStatusCode() {
		return returnStatusCode;
	}

	public void setReturnStatusCode(String returnStatusCode) {
		this.returnStatusCode = returnStatusCode;
	}

	public String getReturnStatusName() {
		return returnStatusName;
	}

	public void setReturnStatusName(String returnStatusName) {
		this.returnStatusName = returnStatusName;
	}

	public String getReturnStatusDescription() {
		return returnStatusDescription;
	}

	public void setReturnStatusDescription(String returnStatusDescription) {
		this.returnStatusDescription = returnStatusDescription;
	}


	public String getItemBarCode() {
		return itemBarCode;
	}


	public void setItemBarCode(String itemBarCode) {
		this.itemBarCode = itemBarCode;
	}


	public Integer getWareHouseId() {
		return wareHouseId;
	}


	public void setWareHouseId(Integer wareHouseId) {
		this.wareHouseId = wareHouseId;
	}

	@Override
	public String toString() {
		return "CustomerReturnEntity [returnId=" + returnId + ", orderId=" + orderId + ", itemId=" + itemId + ", skuCode=" + skuCode
				+ ", quantity=" + quantity + ", size=" + size + ", returnStatus=" + returnStatus + ", login=" + login
				+ ", customerName=" + customerName + ", returnAddress=" + returnAddress + ", city=" + city + ", state="
				+ state + ", country=" + country + ", zipCode=" + zipCode + ", mobile=" + mobile + ", returnMode="
				+ returnMode + ", courierService=" + courierService + ", trackingNumber=" + trackingNumber
				+ ", returnReason=" + returnReason + ", returnDescription=" + returnDescription + ", pickupCharge="
				+ pickupCharge + ", refundAmount=" + refundAmount + ", returnCreatedDate=" + returnCreatedDate
				+ ", DCCode=" + DCCode + ", isRefunded=" + isRefunded + ", refundDate=" + refundDate
				+ ", refundModeCredit=" + refundModeCredit + ", returnStatusCode=" + returnStatusCode
				+ ", returnStatusName=" + returnStatusName + ", returnStatusDescription=" + returnStatusDescription
				+ ", itemBarCode=" + itemBarCode + ", wareHouseId=" + wareHouseId + "]";
	}	
	
}