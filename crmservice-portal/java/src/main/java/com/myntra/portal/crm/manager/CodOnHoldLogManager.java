package com.myntra.portal.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.entry.CodOnHoldLogEntry;
import com.myntra.portal.crm.client.response.CodOnHoldLogResponse;

/**
 * Manager interface(abstract) for customer COD order on hold reason log service
 * 
 * @author Arun Kumar
 */
public interface CodOnHoldLogManager extends BaseManager<CodOnHoldLogResponse, CodOnHoldLogEntry> {

	public AbstractResponse getCodOnHoldLog(Long orderId) throws ERPServiceException;
}