package com.myntra.portal.crm.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import com.myntra.commons.entities.BaseEntity;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;

/**
 * Entity for order payment detail
 * 
 * @author Arun Kumar
 */
@Entity
@Table(name = "mk_payments_log")
@NamedNativeQuery(
	name = CRMPortalServiceConstants.NAMED_QUERIES.ORDER_PAYMENT_LOG_BY_ORDERID, 
	query = "select"
		+ " id as logId,login,orderid as orderId,user_agent as userAgent,ip_address as IPAddress"
		+ " ,payment_gateway_name as gateway,payment_option as paymentOption,payment_issuer as paymentIssuer"
		+ " ,bank_transaction_id as bankTransactionId,gateway_payment_id as gatewayPaymentId"
		+ " ,completed_via as completedVia,card_bank_name as cardBankName,bin_number as binNumber"
		+ " ,amount,amountPaid,amountToBePaid,is_inline as isInLine,is_tampered as isTampered"
		+ " ,is_complete as isComplete,is_flagged as isFlagged,response_code as responseCode"
		+ " ,response_message as responseMessage,time_insert as logInsertTime,time_return as returnTime"
		+ " ,time_transaction as transactionTime"
		+ " from mk_payments_log where orderid=:orderId",
	resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_ORDER_PAYMENT_LOG_BY_ORDERID
)

@SqlResultSetMapping(
	name = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_ORDER_PAYMENT_LOG_BY_ORDERID, 
	columns = {
		@ColumnResult(name = "logId"), @ColumnResult(name = "login"),
		@ColumnResult(name = "orderId"), @ColumnResult(name = "userAgent"),
		@ColumnResult(name = "IPAddress"), @ColumnResult(name = "gateway"),
		@ColumnResult(name = "paymentOption"), @ColumnResult(name = "paymentIssuer"),
		@ColumnResult(name = "bankTransactionId"), @ColumnResult(name = "gatewayPaymentId"),
		@ColumnResult(name = "completedVia"), @ColumnResult(name = "cardBankName"),
		@ColumnResult(name = "binNumber"), @ColumnResult(name = "amount"),
		@ColumnResult(name = "amountPaid"), @ColumnResult(name = "amountToBePaid"),
		@ColumnResult(name = "isInLine"), @ColumnResult(name = "isTampered"),
		@ColumnResult(name = "isComplete"), @ColumnResult(name = "isFlagged"),
		@ColumnResult(name = "responseCode"), @ColumnResult(name = "responseMessage"),
		@ColumnResult(name = "logInsertTime"), @ColumnResult(name = "returnTime"),
		@ColumnResult(name = "transactionTime")
		}
)

public class OrderPaymentLogEntity extends BaseEntity {

	private static final long serialVersionUID = 3688398701763484215L;

	@Column(name = "logId")
	private Long logId;
	
	@Column(name = "login")
	private String login;
	
	@Column(name = "orderId")
	private Long orderId;

	@Column(name = "userAgent")
	private String userAgent;
	
	@Column(name = "IPAddress")
	private String IPAddress;
	
	@Column(name = "gateway")
	private String gateway;
	
	@Column(name = "paymentOption")
	private String paymentOption;
	
	@Column(name = "paymentIssuer")
	private String paymentIssuer;
	
	@Column(name = "bankTransactionId")
	private String bankTransactionId;
	
	@Column(name = "gatewayPaymentId")
	private String gatewayPaymentId;
	
	@Column(name = "completedVia")
	private String completedVia;
	
	@Column(name = "cardBankName")
	private String cardBankName;
	
	@Column(name = "binNumber")
	private Long binNumber;

	// amount
	@Column(name = "amount")
	private Double amount;
	
	@Column(name = "amountPaid")
	private Double amountPaid;
	
	@Column(name = "amountToBePaid")
	private Double amountToBePaid;

	@Column(name = "isInline")
	private Boolean isInline;
	
	@Column(name = "isTampered")
	private Boolean isTampered;
	
	@Column(name = "isComplete")
	private Boolean isComplete;
	
	@Column(name = "isFlagged")
	private Boolean isFlagged;

	@Column(name = "responseCode")
	private String responseCode;
	
	@Column(name = "responseMessage")
	private String responseMessage;

	// date
	@Column(name = "logInsertTime")
	private Date logInsertTime;
	
	@Column(name = "returnTime")
	private Date returnTime;
	
	@Column(name = "transactionTime")
	private Date transactionTime;

	public OrderPaymentLogEntity() {

	}

	public OrderPaymentLogEntity(Long logId, String login, Long orderId, String userAgent, String iPAddress,
			String gateway, String paymentOption, String paymentIssuer, String bankTransactionId,
			String gatewayPaymentId, String completedVia, String cardBankName, Long binNumber, Double amount,
			Double amountPaid, Double amountToBePaid, Boolean isInline, Boolean isTampered, Boolean isComplete,
			Boolean isFlagged, String responseCode, String responseMessage, Date logInsertTime, Date returnTime,
			Date transactionTime) {
		
		this.logId = logId;
		this.login = login;
		this.orderId = orderId;
		this.userAgent = userAgent;
		this.IPAddress = iPAddress;
		this.gateway = gateway;
		this.paymentOption = paymentOption;
		this.paymentIssuer = paymentIssuer;
		this.bankTransactionId = bankTransactionId;
		this.gatewayPaymentId = gatewayPaymentId;
		this.completedVia = completedVia;
		this.cardBankName = cardBankName;
		this.binNumber = binNumber;
		this.amount = amount;
		this.amountPaid = amountPaid;
		this.amountToBePaid = amountToBePaid;
		this.isInline = isInline;
		this.isTampered = isTampered;
		this.isComplete = isComplete;
		this.isFlagged = isFlagged;
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
		this.logInsertTime = logInsertTime;
		this.returnTime = returnTime;
		this.transactionTime = transactionTime;
	}

	public Long getLogId() {
		return logId;
	}

	public String getLogin() {
		return login;
	}

	public Long getOrderId() {
		return orderId;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public String getIPAddress() {
		return IPAddress;
	}

	public String getGateway() {
		return gateway;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public String getPaymentIssuer() {
		return paymentIssuer;
	}

	public String getBankTransactionId() {
		return bankTransactionId;
	}

	public String getGatewayPaymentId() {
		return gatewayPaymentId;
	}

	public String getCompletedVia() {
		return completedVia;
	}

	public String getCardBankName() {
		return cardBankName;
	}

	public Long getBinNumber() {
		return binNumber;
	}

	public Double getAmount() {
		return amount;
	}

	public Double getAmountPaid() {
		return amountPaid;
	}

	public Double getAmountToBePaid() {
		return amountToBePaid;
	}

	public Boolean getIsInline() {
		return isInline;
	}

	public Boolean getIsTampered() {
		return isTampered;
	}

	public Boolean getIsComplete() {
		return isComplete;
	}

	public Boolean getIsFlagged() {
		return isFlagged;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public Date getLogInsertTime() {
		return logInsertTime;
	}

	public Date getReturnTime() {
		return returnTime;
	}

	public Date getTransactionTime() {
		return transactionTime;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public void setIPAddress(String iPAddress) {
		IPAddress = iPAddress;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}

	public void setPaymentIssuer(String paymentIssuer) {
		this.paymentIssuer = paymentIssuer;
	}

	public void setBankTransactionId(String bankTransactionId) {
		this.bankTransactionId = bankTransactionId;
	}

	public void setGatewayPaymentId(String gatewayPaymentId) {
		this.gatewayPaymentId = gatewayPaymentId;
	}

	public void setCompletedVia(String completedVia) {
		this.completedVia = completedVia;
	}

	public void setCardBankName(String cardBankName) {
		this.cardBankName = cardBankName;
	}

	public void setBinNumber(Long binNumber) {
		this.binNumber = binNumber;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setAmountPaid(Double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public void setAmountToBePaid(Double amountToBePaid) {
		this.amountToBePaid = amountToBePaid;
	}

	public void setIsInline(Boolean isInline) {
		this.isInline = isInline;
	}

	public void setIsTampered(Boolean isTampered) {
		this.isTampered = isTampered;
	}

	public void setIsComplete(Boolean isComplete) {
		this.isComplete = isComplete;
	}

	public void setIsFlagged(Boolean isFlagged) {
		this.isFlagged = isFlagged;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public void setLogInsertTime(Date logInsertTime) {
		this.logInsertTime = logInsertTime;
	}

	public void setReturnTime(Date returnTime) {
		this.returnTime = returnTime;
	}

	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}

	@Override
	public String toString() {
		return "OrderPaymentLogEntity [logId=" + logId + ", login=" + login + ", orderId=" + orderId + ", userAgent="
				+ userAgent + ", IPAddress=" + IPAddress + ", gateway=" + gateway + ", paymentOption=" + paymentOption
				+ ", paymentIssuer=" + paymentIssuer + ", bankTransactionId=" + bankTransactionId
				+ ", gatewayPaymentId=" + gatewayPaymentId + ", completedVia=" + completedVia + ", cardBankName="
				+ cardBankName + ", binNumber=" + binNumber + ", amount=" + amount + ", amountPaid=" + amountPaid
				+ ", amountToBePaid=" + amountToBePaid + ", isInline=" + isInline + ", isTampered=" + isTampered
				+ ", isComplete=" + isComplete + ", isFlagged=" + isFlagged + ", responseCode=" + responseCode
				+ ", responseMessage=" + responseMessage + ", logInsertTime=" + logInsertTime + ", returnTime="
				+ returnTime + ", transactionTime=" + transactionTime + "]";
	}	
}