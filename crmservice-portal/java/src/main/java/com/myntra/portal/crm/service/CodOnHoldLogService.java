package com.myntra.portal.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.portal.crm.client.entry.CodOnHoldLogEntry;
import com.myntra.portal.crm.client.response.CodOnHoldLogResponse;

/**
 * Order comment log service interface to retrieve the detail of order comment
 * 
 * @author Arun Kumar
 */
@Path("/order/cod/onhold/log/{orderId}")
public interface CodOnHoldLogService extends BaseService<CodOnHoldLogResponse, CodOnHoldLogEntry> {

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	AbstractResponse getCodOnHoldLog(@PathParam("orderId") Long orderId) throws ERPServiceException;
}
