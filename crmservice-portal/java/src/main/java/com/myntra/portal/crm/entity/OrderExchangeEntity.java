package com.myntra.portal.crm.entity;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import com.myntra.commons.entities.BaseEntity;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;

/**
 * Entity for order exchange details
 * 
 * @author Pravin Mehta
 */
@Entity
@Table(name = "exchange_order_mappings")
@NamedNativeQueries({
	@NamedNativeQuery(
			name = CRMPortalServiceConstants.NAMED_QUERIES.EXCHANGE_DETAIL_BY_ORDERID,
			 query = " SELECT"
					+ " ex.exchange_orderid, ex.releaseid, ex.itemid, ex.returnid,elt(ort.returntype,'RT','RTO') as returntype"
					+ " from"
					+ " exchange_order_mappings ex"
					+ " join ( select max(exchange_orderid) as max_ex_orderid"
					+ " from exchange_order_mappings ex_sub"
					+ " join xcart_orders o_sub on ex_sub.exchange_orderid=o_sub.orderid"
					+ " where"
					+ " o_sub.status not in ('F','D','L')"
					+ " and ex_sub.releaseid = :orderId"
					+ " group by ex_sub.releaseid, ex_sub.itemid"
					+ " ) a on a.max_ex_orderid = ex.exchange_orderid"
					+ " left join mk_old_returns_tracking ort on ort.orderid=ex.exchange_orderid",
			
			resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_EXCHANGE_DETAIL_BY_ORDERID)

	})

@SqlResultSetMapping(
	name = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_EXCHANGE_DETAIL_BY_ORDERID, 
	columns = {
			@ColumnResult(name = "exchange_orderid"), 
			@ColumnResult(name = "releaseid"), 
			@ColumnResult(name = "itemid"),
			@ColumnResult(name = "returnid"),
			@ColumnResult(name = "returntype")
	})


public class OrderExchangeEntity extends BaseEntity {

	private static final long serialVersionUID = 3688398701763484215L;

	@Id
	@Column(name = "exchange_orderid")
	private Long exchangeOrderId;
	
	@Column(name = "releaseid")
	private Long shipmentId;
	
	@Column(name = "itemid")
	private Long itemId;
	
	@Column(name = "returnid")
	private Long returnId;
	
	@Column(name = "returntype")
	private String returnType;
	
	public OrderExchangeEntity() {
	}

	public OrderExchangeEntity(Long exchangeOrderId, Long shipmentId,
			Long itemId, Long returnId, String returnType) {
		this.exchangeOrderId = exchangeOrderId;
		this.shipmentId = shipmentId;
		this.itemId = itemId;
		this.returnId = returnId;
		this.returnType = returnType;
	}
	
	public Long getExchangeOrderId() {
		return exchangeOrderId;
	}

	public void setExchangeOrderId(Long exchangeOrderId) {
		this.exchangeOrderId = exchangeOrderId;
	}

	public Long getShipmentId() {
		return shipmentId;
	}

	public void setShipmentId(Long shipmentId) {
		this.shipmentId = shipmentId;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public Long getReturnId() {
		return returnId;
	}

	public void setReturnId(Long returnId) {
		this.returnId = returnId;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	@Override
	public String toString() {
		return "OrderExchangeEntity [exchangeOrderId=" + exchangeOrderId
				+ ", shipmentId=" + shipmentId + ", itemId=" + itemId
				+ ", returnId=" + returnId + ", returnType=" + returnType + "]";
	}
}