package com.myntra.portal.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.portal.crm.client.entry.SREntry;
import com.myntra.portal.crm.client.response.SRResponse;

/**
 * Manager interface(abstract) for SR detail
 * 
 * @author Arun Kumar
 */
public interface SRManager extends BaseManager<SRResponse, SREntry> {

	public SRResponse getSRDetail(Long id, String loginOrMobile, String filterType) throws ERPServiceException;
	
}