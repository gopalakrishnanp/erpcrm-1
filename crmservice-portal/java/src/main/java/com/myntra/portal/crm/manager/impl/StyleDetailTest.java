package com.myntra.portal.crm.manager.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class StyleDetailTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// style image and title detail
		
		try {
			//String styleDetailURL = WebserviceUtil.getServiceUrlForKey("styleDetailURL");
			HttpGet getRequest = new HttpGet("http://54.251.99.27:7003/myntra-absolut-service/absolut/style/" + "19031");						
			getRequest.addHeader("accept", "application/json");

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse httpResponse = httpClient.execute(getRequest);

			if (httpResponse.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + httpResponse.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((httpResponse.getEntity().getContent())));

			String data;
			StringBuilder outputBuilder = new StringBuilder(2048);
			while ((data = br.readLine()) != null) {
				outputBuilder.append(data);
			}
			
			JSONObject styleObject = new JSONObject(outputBuilder.toString());						
			System.out.println(styleObject.toString());
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}/*} catch (ERPServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/ catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}