package com.myntra.portal.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.portal.crm.client.entry.OrderRtoEntry;
import com.myntra.portal.crm.client.response.OrderRtoResponse;

/**
 * Manager interface(abstract) for order RTO details
 * 
 * @author Arun Kumar
 */
public interface OrderRtoManager extends BaseManager<OrderRtoResponse, OrderRtoEntry> {

	public OrderRtoResponse getOrderRtoDetail(Long orderId) throws ERPServiceException;
}