package com.myntra.portal.crm.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import com.myntra.commons.entities.BaseEntity;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;

/**
 * Entity for return comments
 * 
 * @author Arun Kumar
 */
@Entity
@Table(name = "mk_ordercommentslog")
@NamedNativeQuery(
	name = CRMPortalServiceConstants.NAMED_QUERIES.CUSTOMER_ORDER_COMMENT_BY_ORDERID, 
	query = "select"
		+ " commentid as commentId, orderid as orderId, commenttype as type"
		+ " ,commenttitle as title, commentaddedby as commentBy, description, commentdate as commentDate"
		+ " ,newaddress as newAddress, giftwrapmessage as giftWrapMessage, attachmentname"		
		+ " from mk_ordercommentslog where orderid=:orderId order by commentdate desc", 
	resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_CUSTOMER_ORDER_COMMENT_BY_ORDERID
)

@SqlResultSetMapping(
	name = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_CUSTOMER_ORDER_COMMENT_BY_ORDERID, 
	columns = {
		@ColumnResult(name = "commentId"), @ColumnResult(name = "orderId"),
		@ColumnResult(name = "type"), @ColumnResult(name = "title"),
		@ColumnResult(name = "commentBy"), @ColumnResult(name = "description"),
		@ColumnResult(name = "commentDate"), @ColumnResult(name = "newAddress"),
		@ColumnResult(name = "giftWrapMessage"), @ColumnResult(name = "attachmentname")
	}
)

public class CustomerOrderCommentEntity extends BaseEntity {

	private static final long serialVersionUID = 3688398701763484215L;

	@Column(name = "commentId")
	private Long commentId;

	@Column(name = "orderId")
	private Long orderId;

	@Column(name = "type")
	private String type;

	@Column(name = "title")
	private String title;

	@Column(name = "commentBy")
	private String commentBy;

	@Column(name = "description")
	private String description;

	@Column(name = "newAddress")
	private String newAddress;

	@Column(name = "giftWrapMessage")
	private String giftWrapMessage;

	@Column(name = "commentDate")
	private Date commentDate;
	
	@Column(name = "attachmentname")
	private String attachmentName;

	public CustomerOrderCommentEntity() {

	}

	public CustomerOrderCommentEntity(Long commentId, Long orderId, String type, String title, String commentBy,
			String description, String newAddress, String giftWrapMessage, Date commentDate, String attachmentName) {
		this.commentId = commentId;
		this.orderId = orderId;
		this.type = type;
		this.title = title;
		this.commentBy = commentBy;
		this.description = description;
		this.newAddress = newAddress;
		this.giftWrapMessage = giftWrapMessage;
		this.commentDate = commentDate;
		this.attachmentName = attachmentName;
	}

	public Long getCommentId() {
		return commentId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public String getType() {
		return type;
	}

	public String getTitle() {
		return title;
	}

	public String getCommentBy() {
		return commentBy;
	}

	public String getDescription() {
		return description;
	}

	public String getNewAddress() {
		return newAddress;
	}

	public String getGiftWrapMessage() {
		return giftWrapMessage;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setCommentBy(String commentBy) {
		this.commentBy = commentBy;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setNewAddress(String newAddress) {
		this.newAddress = newAddress;
	}

	public void setGiftWrapMessage(String giftWrapMessage) {
		this.giftWrapMessage = giftWrapMessage;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}
	
	public String getAttachmentName() {
		return attachmentName;
	}

	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}

	@Override
	public String toString() {
		return "CustomerOrderCommentEntity [commentId=" + commentId
				+ ", orderId=" + orderId + ", type=" + type + ", title="
				+ title + ", commentBy=" + commentBy + ", description="
				+ description + ", newAddress=" + newAddress
				+ ", giftWrapMessage=" + giftWrapMessage + ", commentDate="
				+ commentDate + ", attachmentName=" + attachmentName + "]";
	}
}