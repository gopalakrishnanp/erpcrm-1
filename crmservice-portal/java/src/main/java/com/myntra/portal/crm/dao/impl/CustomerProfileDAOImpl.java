package com.myntra.portal.crm.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import com.myntra.commons.dao.impl.BaseDAOImpl;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;
import com.myntra.portal.crm.dao.CustomerProfileDAO;
import com.myntra.portal.crm.entity.CustomerProfileEntity;

public class CustomerProfileDAOImpl extends BaseDAOImpl<CustomerProfileEntity>
		implements CustomerProfileDAO {

	@Override
	public CustomerProfileEntity findById(Long id) {
		return null;
	}

	@Override
	public List<CustomerProfileEntity> getCustomerProfileByLogin(String login)
			throws ERPServiceException {
		Query q = getEntityManager().createNamedQuery(
				CRMPortalServiceConstants.NAMED_QUERIES.CUSTOMER_PROFILE_BY_LOGIN);
		q.setParameter("login", login);
		// Query q = entityManager.createNamedQuery("CustomerProfileEntity.q1",
		// CustomerProfileEntity.class);

		List<Object[]> list = q.getResultList();
		List<CustomerProfileEntity> listCustomerProfiles = new ArrayList<CustomerProfileEntity>();

		for (Object[] objArray : list) {
			int index = 0;
			String loginField = (String) objArray[index++];

			// Need to typecast Character into String due to cxf issues which
			// map Character to Integer type in xml response.
			Character genderChar = (Character)objArray[index++];
			
			String gender = null;
			if(genderChar!=null)
				gender = genderChar.toString();

			Date dob = (Date) objArray[index++];
			String firstName = (String) objArray[index++];
			String lastName = (String) objArray[index++];
			String email = (String) objArray[index++];
			String phone = (String) objArray[index++];
			String referer = (String) objArray[index++];
			String mobile = (String) objArray[index++];
			Integer firstLogin = (Integer) objArray[index++];
			Integer lastLogin = (Integer) objArray[index++];
			
			Character statusChar = (Character)objArray[index++];
			String status = null;
			
			if(statusChar!=null)
				status = statusChar.toString();

			CustomerProfileEntity entity = new CustomerProfileEntity(
					loginField, gender, dob, firstName, lastName, email, phone, referer,
					mobile, firstLogin, lastLogin, status);

			listCustomerProfiles.add(entity);
		}

		return listCustomerProfiles;
	}
	
	@Override
	public List<CustomerProfileEntity> getCustomerProfileByMobile(String mobileInput)
			throws ERPServiceException {
		Query q = getEntityManager().createNamedQuery(
				CRMPortalServiceConstants.NAMED_QUERIES.CUSTOMER_PROFILE_BY_MOBILE);
		q.setParameter("mobile", mobileInput);
		// Query q = entityManager.createNamedQuery("CustomerProfileEntity.q1",
		// CustomerProfileEntity.class);

		List<Object[]> list = q.getResultList();
		
		List<CustomerProfileEntity> listCustomerProfiles = new ArrayList<CustomerProfileEntity>();

		for (Object[] objArray : list) {
			int index = 0;
			String loginField = (String) objArray[index++];

			// Need to typecast Character into String due to cxf issues which
			// map Character to Integer type in xml response.
			Character genderChar = (Character)objArray[index++];
			
			String gender = null;
			if(genderChar!=null)
				gender = genderChar.toString();

			Date dob = (Date) objArray[index++];
			String firstName = (String) objArray[index++];
			String lastName = (String) objArray[index++];
			String email = (String) objArray[index++];
			String phone = (String) objArray[index++];
			String referer = (String) objArray[index++];
			String mobile = (String) objArray[index++];
			Integer firstLogin = (Integer) objArray[index++];
			Integer lastLogin = (Integer) objArray[index++];
			
			Character statusChar = (Character)objArray[index++];
			String status = null;
			
			if(statusChar!=null)
				status = statusChar.toString();

			CustomerProfileEntity entity = new CustomerProfileEntity(
					loginField, gender, dob, firstName, lastName, email, phone, referer,
					mobile, firstLogin, lastLogin, status);

			listCustomerProfiles.add(entity);
		}

		return listCustomerProfiles;
	}


	@Override
	public boolean createCustomerProfile() throws ERPServiceException {
		Query q = getEntityManager().createNativeQuery("insert into xcart_customers values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

		int index = 1;
		q.setParameter(index++, "athena@myntra.com"); // login
		q.setParameter(index++, "M"); // gender
		q.setParameter(index++, new Date(2013, 2, 13)); // DOB
		q.setParameter(index++, "athena"); // firstname
		q.setParameter(index++, "othena"); // lastname
		q.setParameter(index++, "athena@myntra.com"); // email
		q.setParameter(index++, "0801990099"); // phone
		q.setParameter(index++, "9900990099"); // mobile
		q.setParameter(index++, "1010101001"); // first_login

		int p = 0;
		// int p = q.executeUpdate();
		if (p > 0)
			return true;
		return false;
	}
}
