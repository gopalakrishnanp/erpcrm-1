package com.myntra.portal.crm.dao;

import java.util.List;

import com.myntra.commons.dao.BaseDAO;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.portal.crm.entity.CodOnHoldLogEntity;
import com.myntra.portal.crm.entity.CustomerOrderCommentEntity;
import com.myntra.portal.crm.entity.OrderExchangeEntity;
import com.myntra.portal.crm.entity.OrderPaymentLogEntity;
import com.myntra.portal.crm.entity.OrderRtoEntity;

/**
 * DAO for order related detail
 * 
 * @author Arun Kumar
 */
public interface CustomerOrderDAO extends BaseDAO<OrderPaymentLogEntity> {

	// fetches order payment log
	public OrderPaymentLogEntity getOrderPaymentLog(Long orderId) throws ERPServiceException;

	// fetches order comments log
	public List<CustomerOrderCommentEntity> getCustomerOrderCommentLog(Long orderId) throws ERPServiceException;

	// fetches COD order on hold reason log
	public List<CodOnHoldLogEntity> getCodOnHoldLog(Long orderId) throws ERPServiceException;

	public List<OrderExchangeEntity> getExchangeDetail(Long orderId) throws ERPServiceException;

	// order RTO detail
	public List<OrderRtoEntity> getOrderRtoDetail(Long orderId) throws ERPServiceException;
}
