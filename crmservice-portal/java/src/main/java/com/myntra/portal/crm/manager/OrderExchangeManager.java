package com.myntra.portal.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.portal.crm.client.entry.OrderExchangeEntry;
import com.myntra.portal.crm.client.response.OrderExchangeResponse;

/**
 * Manager interface(abstract) for customer order exchange details.
 * 
 * @author Pravin Mehta
 */
public interface OrderExchangeManager extends BaseManager<OrderExchangeResponse, OrderExchangeEntry> {

	public OrderExchangeResponse getExchangeDetail(Long orderId) throws ERPServiceException;
}