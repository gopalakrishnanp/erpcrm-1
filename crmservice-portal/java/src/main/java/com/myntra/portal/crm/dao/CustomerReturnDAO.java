package com.myntra.portal.crm.dao;

import java.util.List;

import com.myntra.commons.dao.BaseDAO;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.portal.crm.entity.CustomerReturnCommentEntity;
import com.myntra.portal.crm.entity.CustomerReturnEntity;

/**
 * DAO for customer return detail
 * 
 * @author Arun Kumar
 */
public interface CustomerReturnDAO extends BaseDAO<CustomerReturnEntity> {

	public List<CustomerReturnEntity> getCustomerReturnData(Long returnId, Long orderId, String login)
			throws ERPServiceException;

	public List<CustomerReturnCommentEntity> getCustomerReturnCommentData(Long returnId) throws ERPServiceException;
}
