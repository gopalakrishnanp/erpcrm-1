package com.myntra.crm.entry;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;
import com.myntra.commons.tranformer.TransformIgnoreAttribute;
import com.myntra.lms.client.status.DeliveryStatus;

/**
 * Entry for customer order search response with relevant shipment fields
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "orderShipmentEntry")
public class CustomerOrderShipmentEntry extends BaseEntry {

	private static final long serialVersionUID = -5498279636900777852L;

	// unique Id
	private Long shipmentId;

	// customer detail
	private String login;
	private String receiverName;
	private String address;
	private String city;
	private String locality;
	private String state;
	private String zipcode;
	private String country;
	private String mobile;
	private String email;

	// status/reason
	private String status;
	private String statusDisplayName;
	private String codPaymentStatus;
	private String cancellationReason;
	private Long cancellationReasonId;
	private boolean refunded;
	private String comment;

	// item/sku detail
	private List<CustomerOrderItemEntry> orderItems;

	// payment detail
	private String paymentMethod;
	private String paymentMethodDisplay;

	// qty
	private Integer quantity;

	// amount detail
	private Double mrpTotal;
	private Double discount;
	private Double couponDiscount;
	private Double cartDiscount;
	private Double cashRedeemed;
	private Double pgDiscount;
	private Double shippingCharge;
	private Double giftCharge;
	private Double codCharge;
	private Double finalAmount;
	private Double cashbackOffered;
	private Double taxAmount;
	private Double emiCharge;

	// dates
	private Date packedOn;
	private Date shippedOn;
	private Date deliveredOn;
	private Date completedOn;
	private Date cancelledOn;

	// logistic detail
	private String courierCode;
	private String courierDisplayName;
	private String trackingNo;

	// tracking detail
	private List<CustomerOrderTrackingDetailEntry> orderTrackingDetailEntry;
	private DeliveryStatus deliveryStatus;
	private int failedAttempts;

	// warehouse detail
	private Integer warehouseId;
	private String warehouseName;
	private String barcode;
	private String warehouseAddress;
	private String warehouseCity;
	private String warehouseState;
	private String warehousePostalCode;
	private String warehouseCountry;
	private String warehouseTinNumber;

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getWarehouseAddress() {
		return warehouseAddress;
	}

	public void setWarehouseAddress(String warehouseAddress) {
		this.warehouseAddress = warehouseAddress;
	}

	public String getWarehouseCity() {
		return warehouseCity;
	}

	public void setWarehouseCity(String warehouseCity) {
		this.warehouseCity = warehouseCity;
	}

	public String getWarehouseState() {
		return warehouseState;
	}

	public void setWarehouseState(String warehouseState) {
		this.warehouseState = warehouseState;
	}

	public String getWarehousePostalCode() {
		return warehousePostalCode;
	}

	public void setWarehousePostalCode(String warehousePostalCode) {
		this.warehousePostalCode = warehousePostalCode;
	}

	public String getWarehouseCountry() {
		return warehouseCountry;
	}

	public void setWarehouseCountry(String warehouseCountry) {
		this.warehouseCountry = warehouseCountry;
	}

	public String getWarehouseTinNumber() {
		return warehouseTinNumber;
	}

	public void setWarehouseTinNumber(String warehouseTinNumber) {
		this.warehouseTinNumber = warehouseTinNumber;
	}
	
	@TransformIgnoreAttribute
	public Long getCancellationReasonId() {
		return cancellationReasonId;
	}

	public void setCancellationReasonId(Long cancellationReasonId) {
		this.cancellationReasonId = cancellationReasonId;
	}

	@TransformIgnoreAttribute
	public Double getEmiCharge() {
		return emiCharge;
	}

	public void setEmiCharge(Double emiCharge) {
		this.emiCharge = emiCharge;
	}

	@TransformIgnoreAttribute
	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Long getShipmentId() {
		return shipmentId;
	}

	public void setShipmentId(Long shipmentId) {
		this.shipmentId = shipmentId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@TransformIgnoreAttribute
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDisplayName() {
		return statusDisplayName;
	}

	public void setStatusDisplayName(String statusDisplay) {
		this.statusDisplayName = statusDisplay;
	}

	@XmlElementWrapper(name = "orderItems")
	@XmlElement(name = "orderItemEntry")
	@TransformIgnoreAttribute
	public List<CustomerOrderItemEntry> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<CustomerOrderItemEntry> orderItems) {
		this.orderItems = orderItems;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public void setPaymentMethodDisplay(String paymentMethodDisplay) {
		this.paymentMethodDisplay = paymentMethodDisplay;
	}

	public String getPaymentMethodDisplay() {
		return paymentMethodDisplay;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getQuantity() {
		return quantity;
	}

	@TransformIgnoreAttribute
	public Double getMrpTotal() {
		return mrpTotal;
	}

	public void setMrpTotal(Double mrpTotal) {
		this.mrpTotal = mrpTotal;
	}

	@TransformIgnoreAttribute
	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	@TransformIgnoreAttribute
	public Double getCouponDiscount() {
		return couponDiscount;
	}

	public void setCouponDiscount(Double couponDiscount) {
		this.couponDiscount = couponDiscount;
	}

	@TransformIgnoreAttribute
	public Double getCartDiscount() {
		return cartDiscount;
	}

	public void setCartDiscount(Double cartDiscount) {
		this.cartDiscount = cartDiscount;
	}

	@TransformIgnoreAttribute
	public Double getCashRedeemed() {
		return cashRedeemed;
	}

	public void setCashRedeemed(Double cashRedeemed) {
		this.cashRedeemed = cashRedeemed;
	}

	@TransformIgnoreAttribute
	public Double getPgDiscount() {
		return pgDiscount;
	}

	public void setPgDiscount(Double pgDiscount) {
		this.pgDiscount = pgDiscount;
	}

	@TransformIgnoreAttribute
	public Double getShippingCharge() {
		return shippingCharge;
	}

	public void setShippingCharge(Double shippingCharge) {
		this.shippingCharge = shippingCharge;
	}

	@TransformIgnoreAttribute
	public Double getGiftCharge() {
		return giftCharge;
	}

	public void setGiftCharge(Double giftCharge) {
		this.giftCharge = giftCharge;
	}

	@TransformIgnoreAttribute
	public Double getCodCharge() {
		return codCharge;
	}

	public void setCodCharge(Double codCharge) {
		this.codCharge = codCharge;
	}

	@TransformIgnoreAttribute
	public Double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(Double finalAmount) {
		this.finalAmount = finalAmount;
	}

	@TransformIgnoreAttribute
	public Double getCashbackOffered() {
		return cashbackOffered;
	}

	public void setCashbackOffered(Double cashbackOffered) {
		this.cashbackOffered = cashbackOffered;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCourierCode() {
		return courierCode;
	}

	public void setCourierCode(String courierCode) {
		this.courierCode = courierCode;
	}

	public String getCourierDisplayName() {
		return courierDisplayName;
	}

	public void setCourierDisplayName(String courierDisplayName) {
		this.courierDisplayName = courierDisplayName;
	}

	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

	public Integer getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Integer warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getCodPaymentStatus() {
		return codPaymentStatus;
	}

	public void setCodPaymentStatus(String codPaymentStatus) {
		this.codPaymentStatus = codPaymentStatus;
	}

	@TransformIgnoreAttribute
	public String getCancellationReason() {
		return cancellationReason;
	}

	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}

	public Date getPackedOn() {
		return packedOn;
	}

	public void setPackedOn(Date packedOn) {
		this.packedOn = packedOn;
	}

	public Date getShippedOn() {
		return shippedOn;
	}

	public void setShippedOn(Date shippedOn) {
		this.shippedOn = shippedOn;
	}

	public Date getDeliveredOn() {
		return deliveredOn;
	}

	public void setDeliveredOn(Date deliveredOn) {
		this.deliveredOn = deliveredOn;
	}

	public Date getCompletedOn() {
		return completedOn;
	}

	public void setCompletedOn(Date completedOn) {
		this.completedOn = completedOn;
	}

	public Date getCancelledOn() {
		return cancelledOn;
	}

	public void setCancelledOn(Date cancelledOn) {
		this.cancelledOn = cancelledOn;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean getRefunded() {
		return refunded;
	}

	public void setRefunded(boolean refunded) {
		this.refunded = refunded;
	}
	
	@XmlElementWrapper(name = "orderTrackingDetails")
	@XmlElement(name = "orderTrackingDetail")
	public List<CustomerOrderTrackingDetailEntry> getOrderTrackingDetailEntry() {
		return orderTrackingDetailEntry;
	}

	public void setOrderTrackingDetailEntry(
			List<CustomerOrderTrackingDetailEntry> orderTrackingDetailEntry) {
		this.orderTrackingDetailEntry = orderTrackingDetailEntry;
	}

	public DeliveryStatus getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public int getFailedAttempts() {
		return failedAttempts;
	}

	public void setFailedAttempts(int failedAttempts) {
		this.failedAttempts = failedAttempts;
	}

	// tracking details
	@Override
	public String toString() {
		return "OrderReleaseEntry [address=" + address
				+ ", cancellationReason=" + cancellationReason
				+ ", cancelledOn=" + cancelledOn + ", cartDiscount="
				+ cartDiscount + ", cashRedeemed=" + cashRedeemed
				+ ", cashbackOffered=" + cashbackOffered + ", city=" + city
				+ ", codCharge=" + codCharge + ", codPaymentStatus="
				+ codPaymentStatus + ", completedOn=" + completedOn
				+ ", country=" + country + ", couponDiscount=" + couponDiscount
				+ ", courierService=" + courierDisplayName + ", deliveredOn="
				+ deliveredOn + ", discount=" + discount + ", email=" + email
				+ ", finalAmount=" + finalAmount + ", giftCharge=" + giftCharge
				+ ", locality=" + locality + ", login=" + login + ", mobile="
				+ mobile + ", mrpTotal=" + mrpTotal + ", orderId=" + shipmentId
				+ ", orderLines=" + orderItems + ", packedOn=" + packedOn
				+ ", paymentMethod=" + paymentMethod + ", pgDiscount="
				+ pgDiscount + ", quantity=" + quantity + ", receiverName="
				+ receiverName + ", shippedOn=" + shippedOn
				+ ", shippingCharge=" + shippingCharge + ", state=" + state
				+ ", status=" + status + ", statusDisplayName="
				+ statusDisplayName + ", trackingNo=" + trackingNo
				+ ", warehouseId=" + warehouseId + ", warehouseName="
				+ warehouseName + ", zipcode=" + zipcode + ", refunded="
				+ refunded + "]";
	}

}
