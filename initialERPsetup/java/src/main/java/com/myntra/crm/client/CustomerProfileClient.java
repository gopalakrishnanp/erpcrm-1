package com.myntra.crm.client;

import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.utils.ResponseHandler;
import com.myntra.crm.client.response.CustomerProfileResponse;

/**
 * Web client for all the contact web services
 */
public class CustomerProfileClient extends ResponseHandler {

    public static final String SERVICE_PREFIX_PERSON = "crm/customerprofile/";

    //public static final String CRMURL = "http://localhost:9090/myntra-crm-service/";
    public static CustomerProfileResponse findByLoginId(String serviceURL,
            String loginID, ContextInfo info) throws ERPServiceException {

        BaseWebClient client = new BaseWebClient(serviceURL, "crmURL", info);
        client.path(SERVICE_PREFIX_PERSON);
        client.query("login", loginID);
        return client.get(CustomerProfileResponse.class);
    }
}
