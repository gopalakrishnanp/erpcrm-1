package com.myntra.crm.client.response;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Person details used outside DAO
 *
 */
@XmlRootElement(name = "customerProfileEntry")
public class CustomerProfileEntry {

    private static final long serialVersionUID = 1L;
    private String login;
    private Character gender;
    private Date dob;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String mobile;
    private Integer firstLogin;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }


    public Character getGender() {
        return gender;
    }

    public void setGender(Character gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(Integer firstLogin) {
        this.firstLogin = firstLogin;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("CustomerProfileEntity [")
                .append("login=").append(login)
                .append(", gender=").append(gender)
                .append(", dob=").append(dob)
                .append(", firstName=").append(firstName)
                .append(", lastName=").append(lastName)
                .append(", email=").append(email)
                .append(", phone=").append(phone)
                .append(", mobile=").append(mobile)
                .append(", firstLogin=").append(firstLogin)
                .append("]");
        return builder.toString();
    }

    public CustomerProfileEntry(String login,Character gender, Date dob, String firstName, String lastName, String email, String phone, String mobile, Integer firstLogin) {
        this.login = login;
        this.gender = gender;
        this.dob = dob;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.mobile = mobile;
        this.firstLogin = firstLogin;
    }

    public CustomerProfileEntry() {
        super();
    }
}
