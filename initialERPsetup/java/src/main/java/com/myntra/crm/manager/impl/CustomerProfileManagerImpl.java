/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.crm.manager.impl;

import com.myntra.crm.client.codes.CRMErrorCodes;
import com.myntra.crm.client.codes.CRMSuccessCodes;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.crm.client.response.CustomerProfileEntry;
import com.myntra.crm.client.response.CustomerProfileResponse;
import com.myntra.crm.dao.CustomerProfileDAO;
import com.myntra.crm.entity.CustomerProfileEntity;
import com.myntra.crm.manager.CustomerProfileManager;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author pravin
 */
public class CustomerProfileManagerImpl extends BaseManagerImpl<CustomerProfileResponse, CustomerProfileEntry> implements CustomerProfileManager {

    private static final Logger LOGGER = Logger
            .getLogger(CustomerProfileManagerImpl.class);
    private CustomerProfileDAO customerProfileDAO;

    public CustomerProfileDAO getCustomerProfileDAO() {
        return customerProfileDAO;
    }

    public void setCustomerProfileDAO(CustomerProfileDAO customerProfileDAO) {
        this.customerProfileDAO = customerProfileDAO;
    }

    @Override
    public AbstractResponse findById(Long l) throws ERPServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AbstractResponse create(CustomerProfileEntry e) throws ERPServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AbstractResponse update(CustomerProfileEntry e, Long l) throws ERPServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AbstractResponse search(int i, int i1, String string, String string1, String string2) throws ERPServiceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CustomerProfileResponse getCustomerProfileByLogin(String login) throws ERPServiceException {
        LOGGER.debug("Entering getCustomerProfileByLogin");
        CustomerProfileResponse response = new CustomerProfileResponse();
        List<CustomerProfileEntity> list;
        try {
            list = (List<CustomerProfileEntity>) ((CustomerProfileDAO) getCustomerProfileDAO()).getCustomerProfileByLogin(login);
            List<CustomerProfileEntry> retList;

            LOGGER.debug("Entering getCustomerProfileByLogin - success in list");
            if (list != null && list.size() > 0) {
                retList = new ArrayList<CustomerProfileEntry>();
                for (CustomerProfileEntity entity : list) {
                    retList.add(toCustomerProfileEntry(entity));
                }
                response.setData(retList);
            }

        } catch (ERPServiceException e) {
            LOGGER.error("Error validating/retreiving data", e);
            StatusResponse error = new StatusResponse(e.getCode(),
                    e.getMessage(), StatusResponse.Type.ERROR);
            response.setStatus(error);
            throw new ERPServiceException(response);
        } catch (Exception e) {
            LOGGER.error("error", e);
            StatusResponse error = new StatusResponse(CRMErrorCodes.ERR_RETRIEVING,
                    StatusResponse.Type.ERROR);
            response.setStatus(error);
            throw new ERPServiceException(response);
        }
        StatusResponse success = new StatusResponse(CRMSuccessCodes.CONTACT_RETRIEVED,
                StatusResponse.Type.SUCCESS, 1);
        response.setStatus(success);
        return response;
    }

    private CustomerProfileEntry toCustomerProfileEntry(CustomerProfileEntity cpe) {
        if (cpe == null) {
            return null;
        }

        return new CustomerProfileEntry(cpe.getLogin(),
                cpe.getGender(), cpe.getDob(), cpe.getFirstName(),
                cpe.getLastName(), cpe.getEmail(), cpe.getPhone(), cpe.getMobile(), 
                cpe.getFirstLogin()
                );
    }

    private CustomerProfileEntity toCustomerProfile(CustomerProfileEntry e, CustomerProfileEntity pd) {
        CustomerProfileEntity pe = pd;
        if (e == null) {
            return null;
        } else {
            if (pe == null) {
                pe = new CustomerProfileEntity();
            }
            if (e.getLogin() != null) {
                pe.setLogin(e.getLogin());
            }
            if (e.getGender()!=null) {
                pe.setGender(e.getGender());
            }            
            if (e.getDob() != null) {
                pe.setDob(e.getDob());
            }
            if (e.getFirstName() != null) {
                pe.setFirstName(e.getFirstName());
            }
            if (e.getLastName() != null) {
                pe.setLastName(e.getLastName());
            }
            if (e.getEmail() != null) {
                pe.setEmail(e.getEmail());
            }
            if (e.getPhone() != null) {
                pe.setPhone(e.getPhone());
            }
            if (e.getMobile() != null) {
                pe.setMobile(e.getMobile());
            }
            if (e.getFirstLogin() != null) {
                pe.setFirstLogin(e.getFirstLogin());
            }
            
            return pe;
        }
    }

    @Override
    public CustomerProfileResponse createCustomerProfile() throws ERPServiceException {
        LOGGER.debug("Entering createCustomerProfile");
        CustomerProfileResponse response = new CustomerProfileResponse();
        try{
            boolean test = ((CustomerProfileDAO) getCustomerProfileDAO()).createCustomerProfile();
            if(test) {
                LOGGER.debug("CheckCustomer created");
            }
        } catch (ERPServiceException e) {
            LOGGER.error("Error validating/retreiving data", e);
            StatusResponse error = new StatusResponse(e.getCode(),
                    e.getMessage(), StatusResponse.Type.ERROR);
            response.setStatus(error);
            throw new ERPServiceException(response);
        } catch (Exception e) {
            LOGGER.error("error", e);
            StatusResponse error = new StatusResponse(CRMErrorCodes.ERR_RETRIEVING,
                    StatusResponse.Type.ERROR);
            response.setStatus(error);
            throw new ERPServiceException(response);
        }
        StatusResponse success = new StatusResponse(CRMSuccessCodes.CONTACT_RETRIEVED,
                StatusResponse.Type.SUCCESS, 1);
        response.setStatus(success);
        return response;
    }
}
