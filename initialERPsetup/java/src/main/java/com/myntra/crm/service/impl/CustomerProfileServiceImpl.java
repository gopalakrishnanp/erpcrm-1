package com.myntra.crm.service.impl;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.crm.client.response.CustomerProfileEntry;
import com.myntra.crm.client.response.CustomerProfileResponse;
import com.myntra.crm.manager.CustomerProfileManager;
import com.myntra.crm.service.CustomerProfileService;
import org.springframework.transaction.TransactionSystemException;

public class CustomerProfileServiceImpl extends BaseServiceImpl<CustomerProfileResponse, CustomerProfileEntry> implements CustomerProfileService {

    @Override
    public AbstractResponse getCustomerProfileByLogin(String login) throws ERPServiceException {
        try {
            return ((CustomerProfileManager) getManager()).getCustomerProfileByLogin(login);
        } catch (ERPServiceException e) {
            return e.getResponse();
        } catch (TransactionSystemException ex) {
            // Spring encapsulates Application Exception into TransactionSystemException
            return ((ERPServiceException) ex.getApplicationException()).getResponse();
        }
    }

    @Override
    public AbstractResponse createCustomerProfile() throws ERPServiceException {
        return ((CustomerProfileManager) getManager()).createCustomerProfile();
    }
}
