package com.myntra.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.crm.entry.CustomerOrderItemEntry;

/**
 * Response for customer order search web service with detail of order
 * items(skus)
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "orderItemResponse")
public class CustomerOrderItemResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;

	private List<CustomerOrderItemEntry> orderItemEntry;

	@XmlElementWrapper(name = "data")
	@XmlElement(name = "orderItem")
	public List<CustomerOrderItemEntry> getData() {
		return orderItemEntry;
	}

	public CustomerOrderItemResponse() {
	}

	public CustomerOrderItemResponse(List<CustomerOrderItemEntry> orderItemEntry) {
		this.orderItemEntry = orderItemEntry;
	}

	public void setData(List<CustomerOrderItemEntry> orderItemEntry) {
		this.orderItemEntry = orderItemEntry;
	}

	@Override
	public String toString() {
		return "OrderLineResponse [data=" + orderItemEntry + ", getStatus()="
				+ getStatus() + "]";
	}

}
