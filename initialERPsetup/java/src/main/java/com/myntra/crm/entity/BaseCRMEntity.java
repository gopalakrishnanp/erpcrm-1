/**
 * Copyright (c) 2011, Myntra and/or its affiliates. All rights reserved. MYNTRA
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.myntra.crm.entity;

import java.io.Serializable;
import javax.persistence.MappedSuperclass;

/**
 * @author ramu
 *
 * Base class for an entity to be persisted.
 *
 * It has a few default member variables needed to be present across all
 * entities
 */
@MappedSuperclass
public abstract class BaseCRMEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public BaseCRMEntity() {
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("BaseCRMEntity []");
        return builder.toString();
    }
}
